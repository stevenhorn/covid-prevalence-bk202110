1 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2373.99    29.05
p_loo       25.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      441   89.6%
 (0.5, 0.7]   (ok)         40    8.1%
   (0.7, 1]   (bad)        10    2.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.053   0.167    0.341  ...    89.0      74.0      38.0   0.99
pu        0.858  0.024   0.819    0.892  ...    27.0      32.0      67.0   1.03
mu        0.110  0.020   0.077    0.139  ...    19.0      18.0      56.0   1.09
mus       0.160  0.026   0.114    0.208  ...    56.0      58.0      48.0   1.00
gamma     0.170  0.034   0.119    0.240  ...   152.0     152.0      76.0   1.00
Is_begin  3.329  2.352   0.087    8.339  ...   152.0     146.0      59.0   1.04
Ia_begin  5.218  4.632   0.006   13.494  ...    99.0     103.0      54.0   1.01
E_begin   3.652  3.420   0.163   11.121  ...   116.0     101.0      80.0   1.01

[8 rows x 11 columns]