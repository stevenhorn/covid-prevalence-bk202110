0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo  -368.78    65.10
p_loo       63.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.5%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)        11    2.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.052   0.169    0.337  ...   120.0     108.0      26.0   1.08
pu        0.808  0.040   0.741    0.881  ...    20.0      20.0      42.0   1.05
mu        0.186  0.031   0.135    0.242  ...    31.0      41.0      58.0   1.03
mus       0.248  0.042   0.165    0.315  ...    42.0      53.0      18.0   1.08
gamma     0.314  0.059   0.202    0.403  ...    82.0     100.0      54.0   1.00
Is_begin  1.694  1.294   0.078    3.980  ...   127.0     131.0      86.0   0.98
Ia_begin  0.711  0.525   0.008    1.597  ...   130.0     110.0      80.0   0.98
E_begin   0.982  0.957   0.001    2.895  ...    86.0      71.0      60.0   1.03

[8 rows x 11 columns]