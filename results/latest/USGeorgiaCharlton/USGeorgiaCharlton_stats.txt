0 Divergences 
Passed validation 
2021-07-22 date of last case count
Computed from 80 by 495 log-likelihood matrix

         Estimate       SE
elpd_loo -1356.46    60.94
p_loo       55.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   92.7%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.199  0.033   0.151    0.259  ...   146.0     152.0      59.0   1.04
pu        0.716  0.012   0.700    0.740  ...   152.0     151.0      54.0   1.03
mu        0.172  0.026   0.127    0.213  ...    24.0      25.0      59.0   1.07
mus       0.263  0.052   0.189    0.364  ...    98.0     112.0      39.0   1.12
gamma     0.446  0.072   0.341    0.588  ...   152.0     152.0      87.0   1.04
Is_begin  0.560  0.680   0.001    1.672  ...    94.0      84.0      59.0   1.03
Ia_begin  1.060  1.138   0.071    3.981  ...    74.0      74.0      93.0   1.02
E_begin   0.499  0.701   0.004    1.600  ...    62.0      20.0      32.0   1.10

[8 rows x 11 columns]