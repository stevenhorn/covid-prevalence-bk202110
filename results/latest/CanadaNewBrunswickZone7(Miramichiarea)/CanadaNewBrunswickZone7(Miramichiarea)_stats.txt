0 Divergences 
Passed validation 
2021-08-08 date of last case count
Computed from 80 by 513 log-likelihood matrix

         Estimate       SE
elpd_loo   -99.79    55.75
p_loo       52.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      443   86.4%
 (0.5, 0.7]   (ok)         62   12.1%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.053   0.176    0.350  ...   152.0     152.0      56.0   1.04
pu        0.812  0.022   0.771    0.849  ...   152.0     152.0      93.0   0.98
mu        0.147  0.032   0.102    0.215  ...    86.0      76.0      37.0   1.01
mus       0.291  0.045   0.231    0.383  ...    84.0     152.0      41.0   1.01
gamma     0.305  0.033   0.251    0.372  ...   152.0     152.0      88.0   0.99
Is_begin  0.453  0.468   0.000    1.316  ...   126.0     152.0      40.0   1.02
Ia_begin  0.495  0.509   0.007    1.505  ...    99.0      77.0     100.0   0.99
E_begin   0.235  0.267   0.000    0.726  ...    29.0      22.0      60.0   1.08

[8 rows x 11 columns]