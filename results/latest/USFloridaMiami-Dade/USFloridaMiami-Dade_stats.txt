0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -3714.52    43.93
p_loo       36.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.9%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.157   0.008   0.150    0.175  ...   152.0     124.0      40.0   1.00
pu          0.702   0.002   0.700    0.707  ...   152.0     130.0      59.0   1.01
mu          0.071   0.011   0.050    0.087  ...    11.0      10.0      57.0   1.16
mus         0.146   0.025   0.103    0.192  ...    53.0      56.0      67.0   0.99
gamma       0.306   0.047   0.208    0.382  ...    97.0      97.0      19.0   0.99
Is_begin   49.328  24.290   5.907   89.158  ...    88.0      86.0      96.0   1.02
Ia_begin  106.015  68.906   0.222  214.379  ...    34.0      23.0      24.0   1.09
E_begin    97.431  74.891   0.927  269.039  ...   111.0      90.0     100.0   0.99

[8 rows x 11 columns]