0 Divergences 
Passed validation 
2021-08-08 date of last case count
Computed from 80 by 513 log-likelihood matrix

         Estimate       SE
elpd_loo  -433.23    56.79
p_loo       66.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      479   93.4%
 (0.5, 0.7]   (ok)         30    5.8%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.182  0.028   0.150    0.238  ...   152.0     152.0      38.0   1.07
pu        0.711  0.009   0.700    0.728  ...   152.0     152.0      75.0   0.99
mu        0.269  0.037   0.195    0.331  ...     8.0       9.0      65.0   1.20
mus       0.411  0.055   0.298    0.500  ...    99.0     108.0      99.0   1.01
gamma     0.559  0.089   0.423    0.750  ...   152.0     152.0      77.0   1.01
Is_begin  1.288  1.471   0.002    4.585  ...   134.0     110.0      30.0   1.03
Ia_begin  1.574  1.401   0.067    4.513  ...   101.0      79.0      68.0   1.04
E_begin   0.684  0.645   0.001    1.710  ...    93.0      39.0      52.0   1.04

[8 rows x 11 columns]