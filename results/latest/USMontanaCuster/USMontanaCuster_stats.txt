0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1106.34    38.04
p_loo       34.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      464   94.3%
 (0.5, 0.7]   (ok)         21    4.3%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.049   0.164    0.328  ...    87.0      92.0      93.0   0.99
pu        0.773  0.021   0.739    0.812  ...    12.0      14.0      56.0   1.17
mu        0.114  0.022   0.081    0.158  ...    49.0      50.0      97.0   1.03
mus       0.172  0.033   0.114    0.221  ...    84.0      74.0      31.0   1.05
gamma     0.196  0.030   0.131    0.243  ...   108.0      99.0      60.0   1.03
Is_begin  0.210  0.234   0.004    0.619  ...   114.0      73.0      95.0   1.02
Ia_begin  0.364  0.440   0.006    1.099  ...    96.0      77.0      18.0   1.01
E_begin   0.168  0.240   0.001    0.581  ...    88.0      70.0      74.0   1.02

[8 rows x 11 columns]