0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -229.50    35.48
p_loo       38.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      465   94.5%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.061   0.156    0.340  ...   152.0     152.0      81.0   1.01
pu        0.838  0.040   0.756    0.886  ...    18.0      19.0      59.0   1.09
mu        0.140  0.026   0.088    0.182  ...    52.0      52.0      60.0   1.02
mus       0.173  0.034   0.121    0.250  ...   104.0     139.0      59.0   1.01
gamma     0.187  0.044   0.113    0.273  ...   152.0     152.0      80.0   1.01
Is_begin  0.697  0.735   0.004    2.150  ...   111.0     152.0      69.0   1.02
Ia_begin  1.643  1.660   0.051    4.639  ...   108.0     125.0      93.0   0.99
E_begin   0.561  0.518   0.002    1.540  ...   122.0     101.0      93.0   0.99

[8 rows x 11 columns]