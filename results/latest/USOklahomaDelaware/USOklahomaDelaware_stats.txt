0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1717.80    37.01
p_loo       40.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      454   92.3%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.206  0.039   0.152    0.275  ...    99.0      83.0      24.0   1.07
pu        0.716  0.015   0.700    0.745  ...    56.0      38.0      60.0   1.00
mu        0.122  0.020   0.087    0.159  ...    15.0      15.0      40.0   1.10
mus       0.184  0.030   0.134    0.236  ...    44.0      47.0      62.0   1.02
gamma     0.260  0.042   0.171    0.324  ...    51.0      64.0      26.0   1.10
Is_begin  1.002  0.968   0.003    2.767  ...    28.0      31.0      22.0   1.05
Ia_begin  2.402  2.111   0.053    6.359  ...   102.0      89.0      74.0   1.01
E_begin   1.023  1.149   0.007    3.223  ...   114.0      63.0      60.0   0.99

[8 rows x 11 columns]