0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -1037.97    46.79
p_loo       61.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      454   92.1%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    4    0.8%

             mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.247   0.061   0.157    0.344  ...    72.0      62.0      40.0   1.01
pu          0.817   0.049   0.727    0.890  ...    26.0      25.0      31.0   1.00
mu          0.267   0.029   0.217    0.313  ...    42.0      26.0      25.0   1.08
mus         0.308   0.037   0.263    0.399  ...    71.0      80.0      32.0   0.99
gamma       0.534   0.092   0.377    0.666  ...   152.0     152.0      83.0   1.01
Is_begin   35.005  17.249   4.405   64.578  ...    98.0     112.0      41.0   1.00
Ia_begin   84.608  33.743  27.042  148.502  ...    70.0      67.0      40.0   1.00
E_begin   136.868  61.716  23.888  231.629  ...    43.0      40.0      32.0   1.03

[8 rows x 11 columns]