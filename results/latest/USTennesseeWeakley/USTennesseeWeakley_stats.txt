0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1527.69    35.19
p_loo       36.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      462   93.9%
 (0.5, 0.7]   (ok)         26    5.3%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.053   0.151    0.321  ...    70.0      67.0      60.0   1.00
pu        0.762  0.028   0.705    0.802  ...    45.0      44.0      40.0   1.01
mu        0.133  0.026   0.079    0.171  ...    27.0      28.0      17.0   1.02
mus       0.199  0.028   0.146    0.239  ...    70.0      83.0      60.0   1.01
gamma     0.241  0.046   0.168    0.333  ...    83.0      98.0      65.0   1.08
Is_begin  0.761  0.740   0.003    2.080  ...    94.0      62.0      22.0   1.00
Ia_begin  1.381  1.388   0.008    4.083  ...   112.0      70.0      93.0   1.00
E_begin   0.631  0.719   0.005    2.457  ...    88.0      87.0      88.0   1.01

[8 rows x 11 columns]