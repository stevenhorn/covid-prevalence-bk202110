7 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2004.94    32.69
p_loo       58.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      474   96.3%
 (0.5, 0.7]   (ok)         13    2.6%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.057   0.162    0.338  ...    55.0      52.0      40.0   1.02
pu        0.784  0.044   0.709    0.840  ...     3.0       4.0      33.0   1.81
mu        0.109  0.018   0.079    0.142  ...    10.0      11.0      48.0   1.15
mus       0.103  0.091   0.015    0.225  ...     3.0       3.0      24.0   1.99
gamma     0.205  0.037   0.134    0.275  ...    72.0      70.0      49.0   1.03
Is_begin  0.916  0.928   0.019    2.196  ...    12.0       6.0      39.0   1.28
Ia_begin  0.589  0.525   0.030    1.555  ...    18.0      10.0      33.0   1.19
E_begin   0.662  0.974   0.003    2.068  ...    25.0      24.0      38.0   1.07

[8 rows x 11 columns]