0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -664.68    42.64
p_loo       56.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      449   91.3%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)        11    2.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.056   0.153    0.338  ...    50.0      64.0      19.0   1.14
pu        0.755  0.042   0.701    0.826  ...    11.0       8.0      29.0   1.20
mu        0.139  0.027   0.087    0.187  ...    59.0      63.0      83.0   1.03
mus       0.243  0.061   0.153    0.374  ...    20.0      25.0      74.0   1.07
gamma     0.269  0.064   0.180    0.397  ...    14.0      15.0      59.0   1.11
Is_begin  0.752  0.712   0.001    2.075  ...   138.0      96.0      40.0   0.99
Ia_begin  1.869  1.661   0.004    4.668  ...    84.0     106.0      60.0   1.05
E_begin   0.861  0.854   0.003    2.351  ...    77.0     102.0      74.0   1.00

[8 rows x 11 columns]