0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1067.84    34.93
p_loo       32.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      461   93.7%
 (0.5, 0.7]   (ok)         22    4.5%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.300  0.039   0.222    0.349  ...    28.0      54.0      40.0   1.16
pu        0.893  0.007   0.883    0.900  ...   126.0     126.0      51.0   1.01
mu        0.157  0.017   0.132    0.187  ...    24.0      28.0      39.0   1.05
mus       0.175  0.028   0.124    0.225  ...   152.0     152.0     100.0   1.03
gamma     0.236  0.037   0.178    0.309  ...   152.0     152.0     100.0   1.00
Is_begin  0.962  0.840   0.023    2.372  ...    87.0      60.0      59.0   1.03
Ia_begin  2.521  2.222   0.071    5.646  ...    59.0     104.0      49.0   0.99
E_begin   1.129  1.042   0.096    3.109  ...    74.0      77.0      59.0   1.00

[8 rows x 11 columns]