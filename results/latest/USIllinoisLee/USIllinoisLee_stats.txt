0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1425.91    34.67
p_loo       37.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      443   90.0%
 (0.5, 0.7]   (ok)         38    7.7%
   (0.7, 1]   (bad)        10    2.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.050   0.165    0.340  ...    95.0     110.0      96.0   0.99
pu        0.738  0.019   0.705    0.767  ...    46.0      48.0      32.0   1.02
mu        0.128  0.023   0.090    0.166  ...    77.0      74.0      80.0   0.99
mus       0.173  0.028   0.131    0.244  ...   152.0     146.0      88.0   1.02
gamma     0.235  0.041   0.155    0.293  ...   135.0     131.0      93.0   0.99
Is_begin  0.645  0.554   0.005    1.790  ...    93.0      78.0      93.0   0.98
Ia_begin  1.280  1.296   0.013    3.299  ...   105.0     152.0      59.0   0.99
E_begin   0.614  0.714   0.001    2.045  ...    87.0     101.0      60.0   1.00

[8 rows x 11 columns]