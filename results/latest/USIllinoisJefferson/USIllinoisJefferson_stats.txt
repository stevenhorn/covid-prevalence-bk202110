0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1549.22    52.97
p_loo       51.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      448   91.1%
 (0.5, 0.7]   (ok)         34    6.9%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.185  0.028   0.150    0.239  ...   129.0     152.0      46.0   0.99
pu        0.714  0.012   0.700    0.738  ...   118.0     105.0      59.0   1.05
mu        0.148  0.020   0.111    0.184  ...    22.0      21.0      34.0   1.08
mus       0.248  0.034   0.193    0.309  ...   107.0     114.0      54.0   1.01
gamma     0.377  0.049   0.306    0.470  ...   104.0     109.0      65.0   0.99
Is_begin  0.573  0.631   0.004    1.841  ...   123.0      80.0      57.0   1.02
Ia_begin  0.931  0.862   0.013    2.479  ...    70.0      65.0      59.0   0.99
E_begin   0.518  0.571   0.007    1.616  ...    95.0      75.0      86.0   1.01

[8 rows x 11 columns]