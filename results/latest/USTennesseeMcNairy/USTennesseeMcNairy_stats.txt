0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1319.18    29.57
p_loo       26.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      469   95.3%
 (0.5, 0.7]   (ok)         19    3.9%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.264  0.054   0.171    0.344  ...    85.0      83.0      59.0   1.02
pu        0.786  0.022   0.753    0.827  ...    46.0      46.0      87.0   1.03
mu        0.114  0.015   0.091    0.146  ...    54.0      49.0     100.0   1.05
mus       0.167  0.029   0.102    0.214  ...   152.0     152.0     100.0   1.00
gamma     0.200  0.036   0.144    0.271  ...   117.0     125.0      69.0   1.02
Is_begin  0.687  0.647   0.014    2.180  ...    79.0      83.0      88.0   1.08
Ia_begin  1.392  1.612   0.016    5.136  ...    96.0      97.0     100.0   1.00
E_begin   0.377  0.410   0.002    0.979  ...    99.0      65.0      60.0   1.05

[8 rows x 11 columns]