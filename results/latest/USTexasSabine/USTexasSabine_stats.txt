0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1196.67    87.04
p_loo       55.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      470   95.5%
 (0.5, 0.7]   (ok)         18    3.7%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.217  0.044   0.153    0.293  ...    83.0      88.0      93.0   0.99
pu        0.726  0.015   0.703    0.755  ...    86.0      80.0      97.0   0.99
mu        0.159  0.019   0.129    0.197  ...    13.0      13.0      40.0   1.14
mus       0.329  0.049   0.232    0.405  ...   102.0     104.0     100.0   1.00
gamma     0.594  0.090   0.426    0.754  ...    68.0      68.0      57.0   1.02
Is_begin  0.738  0.703   0.012    2.167  ...    17.0      10.0      38.0   1.17
Ia_begin  1.236  1.097   0.011    3.052  ...    21.0      23.0      60.0   1.09
E_begin   0.591  0.740   0.008    2.036  ...    14.0      19.0      35.0   1.17

[8 rows x 11 columns]