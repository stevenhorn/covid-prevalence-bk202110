0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1468.44    66.13
p_loo       47.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.054   0.168    0.348  ...    80.0      96.0      60.0   0.98
pu        0.742  0.026   0.702    0.787  ...    20.0      17.0      56.0   1.10
mu        0.138  0.020   0.102    0.168  ...     7.0       7.0      24.0   1.24
mus       0.289  0.035   0.237    0.349  ...    34.0      31.0      59.0   1.05
gamma     0.494  0.088   0.341    0.635  ...   152.0     152.0      19.0   0.98
Is_begin  1.333  0.911   0.035    3.143  ...    48.0      38.0      40.0   1.01
Ia_begin  0.786  0.485   0.086    1.758  ...   102.0      93.0      75.0   0.99
E_begin   1.141  1.011   0.021    3.093  ...    37.0      43.0      39.0   1.03

[8 rows x 11 columns]