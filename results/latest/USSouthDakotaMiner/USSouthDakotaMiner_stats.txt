0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -628.93    51.60
p_loo       52.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      447   90.9%
 (0.5, 0.7]   (ok)         36    7.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.057   0.159    0.334  ...    78.0      76.0      60.0   1.04
pu        0.775  0.019   0.748    0.811  ...    88.0      94.0      87.0   0.99
mu        0.147  0.026   0.102    0.192  ...    49.0      46.0      38.0   1.01
mus       0.225  0.030   0.172    0.285  ...    93.0      94.0      54.0   1.00
gamma     0.294  0.059   0.205    0.416  ...   141.0     129.0      59.0   1.04
Is_begin  0.514  0.430   0.044    1.248  ...   133.0     125.0      91.0   0.98
Ia_begin  0.895  0.692   0.016    2.189  ...    86.0     107.0      93.0   0.99
E_begin   0.523  0.502   0.003    1.661  ...    86.0      61.0      32.0   1.01

[8 rows x 11 columns]