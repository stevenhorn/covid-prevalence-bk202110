0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1106.97    64.96
p_loo       53.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      444   90.2%
 (0.5, 0.7]   (ok)         38    7.7%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.048   0.178    0.345  ...    63.0      68.0      42.0   1.04
pu        0.750  0.022   0.704    0.787  ...    16.0      20.0      59.0   1.10
mu        0.131  0.022   0.084    0.168  ...     6.0       6.0      22.0   1.34
mus       0.228  0.033   0.181    0.300  ...    74.0      77.0      59.0   1.05
gamma     0.301  0.042   0.232    0.378  ...    97.0     106.0      57.0   1.00
Is_begin  0.474  0.512   0.001    1.500  ...    54.0      38.0      53.0   1.00
Ia_begin  0.851  1.045   0.007    2.785  ...    46.0      24.0      43.0   1.06
E_begin   0.368  0.426   0.004    1.077  ...    64.0      37.0      22.0   1.00

[8 rows x 11 columns]