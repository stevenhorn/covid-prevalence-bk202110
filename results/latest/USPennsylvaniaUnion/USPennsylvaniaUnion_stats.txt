0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1830.08    78.98
p_loo       78.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      463   94.1%
 (0.5, 0.7]   (ok)         17    3.5%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    4    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.170  0.018   0.150    0.200  ...    33.0      37.0      39.0   1.05
pu        0.706  0.005   0.700    0.717  ...    51.0      46.0      56.0   1.08
mu        0.089  0.015   0.065    0.116  ...    18.0      17.0      74.0   1.11
mus       0.266  0.044   0.192    0.332  ...    23.0      20.0      54.0   1.08
gamma     0.359  0.084   0.235    0.489  ...     9.0       8.0      60.0   1.23
Is_begin  0.768  0.641   0.014    1.978  ...   128.0      92.0      40.0   0.99
Ia_begin  1.337  1.354   0.045    3.311  ...    82.0     102.0      80.0   1.07
E_begin   0.486  0.482   0.003    1.363  ...    95.0      63.0      15.0   1.02

[8 rows x 11 columns]