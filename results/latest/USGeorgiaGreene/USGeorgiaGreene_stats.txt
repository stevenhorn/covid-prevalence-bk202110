0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1184.30    61.52
p_loo       47.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      451   91.7%
 (0.5, 0.7]   (ok)         35    7.1%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.054   0.166    0.339  ...    69.0      73.0      38.0   1.07
pu        0.784  0.025   0.732    0.815  ...    37.0      56.0      57.0   1.04
mu        0.145  0.020   0.115    0.179  ...    39.0      37.0      60.0   1.02
mus       0.233  0.043   0.162    0.312  ...    64.0      67.0      40.0   1.00
gamma     0.302  0.050   0.214    0.379  ...    81.0      71.0      60.0   1.01
Is_begin  0.945  0.802   0.044    2.303  ...    93.0      77.0     102.0   0.99
Ia_begin  2.464  1.692   0.183    5.372  ...   115.0     102.0      86.0   1.01
E_begin   1.300  1.247   0.019    3.699  ...    28.0      20.0      60.0   1.09

[8 rows x 11 columns]