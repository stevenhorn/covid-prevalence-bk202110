0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -831.08    32.00
p_loo       29.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      454   92.3%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.051   0.168    0.338  ...    75.0      75.0      59.0   1.08
pu        0.844  0.014   0.818    0.866  ...    86.0      93.0      95.0   1.10
mu        0.146  0.022   0.109    0.193  ...    24.0      25.0      22.0   1.08
mus       0.205  0.037   0.141    0.264  ...    52.0      53.0      40.0   1.00
gamma     0.254  0.042   0.167    0.311  ...    56.0      54.0      56.0   1.03
Is_begin  1.064  1.075   0.004    2.936  ...    98.0      25.0      38.0   1.06
Ia_begin  2.425  2.044   0.000    6.889  ...    74.0      49.0      30.0   1.02
E_begin   1.164  1.356   0.005    3.269  ...   102.0      88.0      30.0   1.00

[8 rows x 11 columns]