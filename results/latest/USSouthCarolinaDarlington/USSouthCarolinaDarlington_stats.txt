0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1696.55    31.93
p_loo       33.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.9%
 (0.5, 0.7]   (ok)         34    6.9%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.049   0.166    0.336  ...    19.0      22.0      59.0   1.09
pu        0.732  0.021   0.700    0.765  ...    18.0      15.0      20.0   1.12
mu        0.111  0.021   0.075    0.147  ...    11.0      11.0      42.0   1.15
mus       0.166  0.027   0.119    0.218  ...    68.0      65.0     100.0   1.04
gamma     0.213  0.036   0.161    0.276  ...    77.0      78.0      60.0   1.02
Is_begin  1.019  0.932   0.011    2.854  ...   132.0     104.0      40.0   1.01
Ia_begin  0.645  0.481   0.001    1.544  ...    37.0      20.0      29.0   1.08
E_begin   0.650  0.787   0.013    1.568  ...   102.0      44.0      60.0   1.03

[8 rows x 11 columns]