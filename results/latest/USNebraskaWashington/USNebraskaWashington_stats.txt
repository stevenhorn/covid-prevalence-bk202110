0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1205.37    45.33
p_loo       45.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.057   0.166    0.344  ...    54.0      59.0      43.0   1.08
pu        0.730  0.020   0.702    0.765  ...    52.0      48.0      42.0   1.00
mu        0.147  0.024   0.101    0.187  ...    43.0      42.0      57.0   0.99
mus       0.211  0.033   0.154    0.272  ...    76.0      81.0      60.0   1.00
gamma     0.285  0.050   0.209    0.380  ...   118.0     113.0      63.0   0.99
Is_begin  1.333  0.877   0.010    2.777  ...   101.0      80.0      58.0   1.00
Ia_begin  2.978  2.257   0.017    6.930  ...   113.0     104.0      60.0   0.99
E_begin   1.498  1.745   0.026    5.331  ...   121.0      81.0      60.0   1.00

[8 rows x 11 columns]