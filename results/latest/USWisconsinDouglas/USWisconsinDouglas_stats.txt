0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1433.91    36.09
p_loo       36.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      445   90.4%
 (0.5, 0.7]   (ok)         41    8.3%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.050   0.162    0.339  ...    54.0      54.0      38.0   1.03
pu        0.742  0.024   0.701    0.779  ...    20.0      19.0      24.0   1.12
mu        0.128  0.022   0.087    0.158  ...     7.0       7.0      53.0   1.27
mus       0.191  0.027   0.149    0.237  ...   152.0     152.0      81.0   1.00
gamma     0.263  0.047   0.171    0.346  ...   113.0     107.0      91.0   0.99
Is_begin  0.775  0.834   0.009    2.722  ...   101.0      90.0      57.0   1.01
Ia_begin  1.655  1.442   0.012    4.124  ...   106.0     101.0      40.0   0.99
E_begin   0.716  0.847   0.015    2.559  ...   129.0     152.0     100.0   1.04

[8 rows x 11 columns]