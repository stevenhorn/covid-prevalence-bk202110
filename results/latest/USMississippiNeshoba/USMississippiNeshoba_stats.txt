0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1528.08    32.39
p_loo       36.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      440   89.4%
 (0.5, 0.7]   (ok)         38    7.7%
   (0.7, 1]   (bad)        13    2.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.209  0.038   0.150    0.270  ...    77.0      73.0      56.0   1.07
pu        0.720  0.017   0.701    0.754  ...    44.0      36.0      61.0   1.05
mu        0.102  0.016   0.079    0.128  ...    19.0      20.0      66.0   1.06
mus       0.154  0.031   0.109    0.210  ...    64.0      55.0      88.0   1.04
gamma     0.203  0.037   0.144    0.270  ...    86.0      81.0      60.0   1.04
Is_begin  0.775  0.752   0.015    2.473  ...   105.0      64.0      24.0   1.01
Ia_begin  1.181  1.184   0.085    4.146  ...    92.0      97.0      93.0   0.99
E_begin   0.640  0.826   0.007    2.080  ...    46.0      26.0      40.0   1.12

[8 rows x 11 columns]