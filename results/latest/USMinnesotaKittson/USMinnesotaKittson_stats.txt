0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -662.37    36.72
p_loo       43.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.229  0.050   0.157    0.318  ...   104.0      97.0     100.0   1.01
pu        0.741  0.024   0.703    0.780  ...    25.0      24.0      60.0   1.07
mu        0.174  0.035   0.116    0.231  ...     7.0       8.0      59.0   1.22
mus       0.276  0.045   0.192    0.347  ...   127.0     125.0      60.0   1.01
gamma     0.344  0.051   0.260    0.440  ...    99.0     110.0      83.0   0.99
Is_begin  0.478  0.649   0.001    1.864  ...   116.0      69.0      53.0   1.01
Ia_begin  0.761  0.810   0.007    2.315  ...    27.0      50.0      96.0   1.09
E_begin   0.357  0.542   0.002    1.342  ...    94.0      53.0      59.0   1.02

[8 rows x 11 columns]