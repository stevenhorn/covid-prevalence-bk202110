0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2152.79    31.30
p_loo       39.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      434   88.2%
 (0.5, 0.7]   (ok)         47    9.6%
   (0.7, 1]   (bad)        11    2.2%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.210   0.048   0.150    0.291  ...   152.0     111.0      40.0   0.98
pu         0.724   0.018   0.702    0.753  ...    63.0      67.0      60.0   1.02
mu         0.110   0.015   0.084    0.138  ...    26.0      28.0      44.0   1.02
mus        0.210   0.027   0.164    0.258  ...   152.0     152.0      52.0   1.02
gamma      0.413   0.052   0.315    0.489  ...    61.0      52.0      58.0   1.02
Is_begin   3.617   2.230   0.201    7.617  ...   121.0      85.0      60.0   1.01
Ia_begin  11.335   4.616   3.790   19.604  ...   128.0     108.0      60.0   1.01
E_begin   17.325  10.157   0.260   34.376  ...    69.0      60.0      38.0   1.01

[8 rows x 11 columns]