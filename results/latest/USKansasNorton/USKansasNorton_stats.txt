0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1304.84    76.12
p_loo       40.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      471   95.7%
 (0.5, 0.7]   (ok)         12    2.4%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.010   0.150    0.179  ...   152.0     141.0      60.0   1.01
pu        0.703  0.002   0.700    0.709  ...   114.0      94.0      37.0   1.04
mu        0.113  0.019   0.084    0.149  ...    60.0      69.0      60.0   1.00
mus       0.154  0.034   0.096    0.219  ...   115.0     103.0      49.0   0.98
gamma     0.186  0.033   0.137    0.239  ...    29.0      37.0      93.0   1.03
Is_begin  0.285  0.357   0.003    1.009  ...    53.0      70.0      83.0   1.01
Ia_begin  0.410  0.651   0.001    1.062  ...    69.0      32.0      43.0   1.03
E_begin   0.169  0.201   0.003    0.581  ...    42.0      40.0      59.0   1.04

[8 rows x 11 columns]