0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1266.08    41.49
p_loo       42.44        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      448   91.1%
 (0.5, 0.7]   (ok)         34    6.9%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    4    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.014   0.150    0.196  ...   127.0     111.0      97.0   1.01
pu        0.705  0.005   0.700    0.718  ...    71.0      22.0      40.0   1.08
mu        0.123  0.018   0.093    0.156  ...    41.0      44.0      46.0   1.01
mus       0.213  0.036   0.149    0.273  ...    51.0      97.0      88.0   1.06
gamma     0.348  0.054   0.232    0.437  ...   131.0      90.0      58.0   1.02
Is_begin  0.589  0.531   0.010    1.718  ...    55.0      66.0      59.0   1.01
Ia_begin  0.929  1.090   0.009    3.230  ...    66.0      69.0      72.0   1.02
E_begin   0.380  0.401   0.007    0.831  ...    75.0      27.0      32.0   1.08

[8 rows x 11 columns]