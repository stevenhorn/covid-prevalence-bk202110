0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1651.88    32.59
p_loo       30.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      463   94.1%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.053   0.166    0.339  ...   152.0     152.0      58.0   0.99
pu        0.762  0.026   0.709    0.802  ...    68.0      73.0      88.0   1.00
mu        0.134  0.021   0.085    0.166  ...    26.0      26.0      53.0   1.05
mus       0.182  0.035   0.125    0.247  ...   145.0     147.0      80.0   0.99
gamma     0.232  0.040   0.169    0.298  ...    74.0      80.0      72.0   1.06
Is_begin  0.771  0.750   0.015    2.062  ...   113.0      81.0      85.0   1.01
Ia_begin  1.200  1.132   0.018    3.024  ...   111.0     101.0      80.0   0.99
E_begin   0.650  0.724   0.014    1.480  ...    72.0      95.0      80.0   0.98

[8 rows x 11 columns]