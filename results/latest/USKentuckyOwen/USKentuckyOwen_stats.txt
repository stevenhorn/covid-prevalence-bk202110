0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -980.77    46.94
p_loo       39.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.054   0.155    0.332  ...   152.0     152.0      80.0   1.01
pu        0.813  0.026   0.767    0.861  ...    11.0      11.0      59.0   1.15
mu        0.145  0.024   0.105    0.198  ...    32.0      32.0      40.0   1.06
mus       0.187  0.029   0.133    0.231  ...   144.0     152.0      95.0   1.02
gamma     0.237  0.042   0.160    0.312  ...   132.0     152.0      88.0   1.03
Is_begin  0.589  0.538   0.005    1.718  ...    88.0      61.0      88.0   1.02
Ia_begin  1.069  1.185   0.017    2.993  ...    99.0     101.0      81.0   0.99
E_begin   0.472  0.771   0.003    1.446  ...   100.0      56.0      60.0   1.02

[8 rows x 11 columns]