0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -844.73    27.41
p_loo       27.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      450   91.5%
 (0.5, 0.7]   (ok)         35    7.1%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.052   0.166    0.338  ...    19.0      24.0      40.0   1.11
pu        0.867  0.018   0.836    0.898  ...     6.0       6.0      46.0   1.30
mu        0.121  0.020   0.086    0.159  ...    34.0      38.0     100.0   1.04
mus       0.178  0.036   0.103    0.234  ...   152.0     152.0      40.0   1.10
gamma     0.228  0.043   0.159    0.307  ...   152.0     152.0      93.0   0.99
Is_begin  0.774  0.826   0.005    2.490  ...   110.0      71.0      40.0   1.04
Ia_begin  1.662  2.040   0.001    5.336  ...    70.0      30.0      22.0   1.04
E_begin   0.702  0.960   0.000    2.321  ...    95.0      46.0      40.0   1.00

[8 rows x 11 columns]