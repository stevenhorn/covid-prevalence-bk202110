0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2803.74    78.14
p_loo       53.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.7%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.189  0.030   0.150    0.240  ...   152.0     138.0      24.0   1.02
pu        0.710  0.008   0.700    0.725  ...    88.0      86.0      83.0   0.98
mu        0.105  0.013   0.084    0.130  ...    55.0      55.0      60.0   1.02
mus       0.299  0.036   0.246    0.375  ...    48.0      46.0      60.0   1.04
gamma     0.506  0.079   0.399    0.655  ...    84.0      84.0      59.0   1.01
Is_begin  1.524  1.202   0.044    3.446  ...    40.0      57.0      60.0   1.04
Ia_begin  2.123  1.743   0.122    6.283  ...   100.0      66.0      37.0   1.04
E_begin   1.037  0.979   0.001    2.854  ...    45.0      38.0      29.0   1.01

[8 rows x 11 columns]