0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1371.13    47.46
p_loo       45.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         23    4.7%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.052   0.168    0.335  ...   100.0      97.0      57.0   1.01
pu        0.766  0.033   0.709    0.823  ...     6.0       7.0      15.0   1.25
mu        0.125  0.024   0.079    0.162  ...    17.0      17.0      60.0   1.09
mus       0.229  0.040   0.152    0.301  ...    24.0      28.0      59.0   1.05
gamma     0.263  0.049   0.158    0.341  ...    76.0      80.0      56.0   1.03
Is_begin  0.686  0.738   0.007    1.944  ...   120.0     102.0      95.0   0.99
Ia_begin  1.291  1.243   0.011    3.872  ...    37.0      62.0      76.0   1.03
E_begin   0.496  0.524   0.003    1.662  ...    66.0      63.0      79.0   0.99

[8 rows x 11 columns]