0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1351.09    51.73
p_loo       76.16        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      468   95.1%
 (0.5, 0.7]   (ok)         18    3.7%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.215  0.042   0.159    0.299  ...    11.0      13.0      80.0   1.12
pu        0.738  0.022   0.711    0.778  ...     5.0       6.0      72.0   1.34
mu        0.153  0.039   0.091    0.214  ...     3.0       3.0      19.0   2.28
mus       0.170  0.026   0.125    0.214  ...     8.0       9.0      58.0   1.21
gamma     0.126  0.053   0.066    0.211  ...     3.0       3.0      20.0   2.13
Is_begin  0.478  0.366   0.017    1.204  ...    43.0      33.0      22.0   1.12
Ia_begin  1.513  1.653   0.023    4.420  ...     6.0       6.0      22.0   1.35
E_begin   0.762  1.019   0.006    3.198  ...     6.0       5.0      25.0   1.44

[8 rows x 11 columns]