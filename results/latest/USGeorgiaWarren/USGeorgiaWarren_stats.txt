0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -846.02    58.99
p_loo       42.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      467   94.9%
 (0.5, 0.7]   (ok)         21    4.3%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.050   0.153    0.329  ...    73.0      69.0      39.0   1.02
pu        0.846  0.020   0.815    0.878  ...    27.0      32.0      16.0   1.03
mu        0.117  0.021   0.088    0.162  ...    12.0      13.0      19.0   1.11
mus       0.215  0.041   0.156    0.301  ...    45.0      52.0      62.0   1.18
gamma     0.303  0.050   0.223    0.392  ...   101.0     106.0      60.0   1.01
Is_begin  0.849  0.866   0.012    2.557  ...   129.0      90.0      59.0   1.04
Ia_begin  1.558  1.628   0.008    4.455  ...    88.0      86.0      23.0   0.99
E_begin   0.775  0.805   0.003    2.676  ...    92.0      81.0      57.0   1.00

[8 rows x 11 columns]