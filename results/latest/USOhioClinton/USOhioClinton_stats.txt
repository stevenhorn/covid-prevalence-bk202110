0 Divergences 
Failed validation 
2021-07-22 date of last case count
Computed from 80 by 495 log-likelihood matrix

         Estimate       SE
elpd_loo -1381.78    70.09
p_loo       44.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      454   91.7%
 (0.5, 0.7]   (ok)         33    6.7%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.051   0.163    0.323  ...    58.0      56.0      60.0   1.01
pu        0.807  0.020   0.771    0.844  ...    49.0      48.0      54.0   0.99
mu        0.130  0.023   0.095    0.181  ...    26.0      25.0      40.0   1.05
mus       0.214  0.040   0.136    0.280  ...    50.0      47.0      56.0   1.01
gamma     0.258  0.046   0.182    0.336  ...    96.0      96.0      57.0   0.98
Is_begin  0.920  0.883   0.030    2.979  ...    90.0      57.0      22.0   1.02
Ia_begin  1.629  1.649   0.023    5.475  ...   121.0      71.0      60.0   1.00
E_begin   0.686  0.644   0.007    2.316  ...    62.0      34.0      43.0   1.04

[8 rows x 11 columns]