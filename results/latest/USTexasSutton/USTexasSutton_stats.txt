0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -862.76    42.71
p_loo       44.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.051   0.154    0.330  ...    66.0      68.0      67.0   1.02
pu        0.758  0.022   0.715    0.792  ...    43.0      39.0      93.0   1.03
mu        0.124  0.024   0.090    0.177  ...    31.0      34.0      58.0   1.04
mus       0.181  0.031   0.142    0.249  ...    39.0      46.0      34.0   1.04
gamma     0.231  0.042   0.159    0.296  ...    73.0      89.0      60.0   0.99
Is_begin  0.213  0.284   0.001    0.768  ...    28.0      42.0      28.0   1.07
Ia_begin  0.433  0.591   0.006    1.945  ...    68.0      60.0      57.0   1.04
E_begin   0.155  0.172   0.003    0.543  ...    21.0      16.0      42.0   1.13

[8 rows x 11 columns]