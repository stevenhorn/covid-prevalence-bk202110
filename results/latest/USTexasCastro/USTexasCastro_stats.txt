0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1248.01    58.07
p_loo       43.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      462   93.9%
 (0.5, 0.7]   (ok)         20    4.1%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.190  0.034   0.152    0.257  ...   102.0      96.0      61.0   1.02
pu        0.711  0.009   0.700    0.725  ...    71.0      71.0      58.0   1.01
mu        0.167  0.033   0.112    0.223  ...     6.0       6.0      85.0   1.32
mus       0.224  0.039   0.147    0.292  ...    10.0      10.0      34.0   1.16
gamma     0.277  0.057   0.182    0.367  ...    19.0      15.0      97.0   1.10
Is_begin  0.859  0.972   0.004    2.642  ...    68.0      26.0      37.0   1.04
Ia_begin  1.627  1.755   0.020    4.575  ...    84.0      56.0      59.0   1.02
E_begin   0.665  1.021   0.011    2.502  ...    53.0      66.0      60.0   1.01

[8 rows x 11 columns]