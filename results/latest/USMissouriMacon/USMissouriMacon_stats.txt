0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1356.49    91.29
p_loo       50.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      463   94.1%
 (0.5, 0.7]   (ok)         19    3.9%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.015   0.150    0.193  ...    79.0      63.0      34.0   1.03
pu        0.706  0.007   0.700    0.717  ...    71.0     152.0      59.0   1.04
mu        0.110  0.013   0.088    0.131  ...    52.0      56.0      93.0   1.00
mus       0.271  0.034   0.226    0.337  ...    66.0      67.0      59.0   1.02
gamma     0.588  0.079   0.477    0.741  ...    94.0     112.0      65.0   1.00
Is_begin  0.698  0.636   0.013    2.005  ...    78.0     126.0      93.0   1.04
Ia_begin  0.926  0.981   0.017    2.723  ...    89.0      88.0      57.0   1.02
E_begin   0.422  0.409   0.008    1.238  ...    86.0      70.0      59.0   0.99

[8 rows x 11 columns]