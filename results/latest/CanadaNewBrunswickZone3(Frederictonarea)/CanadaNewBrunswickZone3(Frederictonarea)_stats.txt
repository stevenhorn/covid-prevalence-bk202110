0 Divergences 
Passed validation 
2021-08-08 date of last case count
Computed from 80 by 513 log-likelihood matrix

         Estimate       SE
elpd_loo  -684.66    28.77
p_loo       34.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      465   90.6%
 (0.5, 0.7]   (ok)         43    8.4%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.296   0.045   0.189    0.349  ...   152.0     145.0      81.0   1.00
pu         0.893   0.007   0.880    0.900  ...    45.0      39.0      36.0   1.02
mu         0.228   0.032   0.177    0.282  ...    21.0      22.0      34.0   1.19
mus        0.236   0.032   0.177    0.282  ...    96.0      96.0     100.0   1.07
gamma      0.355   0.056   0.269    0.469  ...    97.0      97.0      96.0   1.01
Is_begin   8.416   5.189   0.897   19.155  ...    84.0      91.0      95.0   1.04
Ia_begin  34.774  21.153   7.740   88.650  ...   117.0     121.0      97.0   1.03
E_begin   16.961  13.170   0.036   40.156  ...    85.0      78.0      56.0   1.02

[8 rows x 11 columns]