0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1452.16    44.49
p_loo       39.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      444   90.2%
 (0.5, 0.7]   (ok)         44    8.9%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.238  0.045   0.169    0.339  ...    32.0      46.0      45.0   1.06
pu        0.730  0.018   0.702    0.756  ...    21.0      18.0      38.0   1.10
mu        0.129  0.024   0.090    0.169  ...     5.0       5.0      40.0   1.41
mus       0.197  0.031   0.137    0.246  ...    66.0      52.0      59.0   1.05
gamma     0.252  0.038   0.198    0.330  ...   131.0     152.0      80.0   1.03
Is_begin  0.501  0.415   0.034    1.234  ...    84.0      50.0      88.0   1.02
Ia_begin  0.740  0.793   0.023    2.502  ...    79.0      52.0      60.0   0.99
E_begin   0.393  0.438   0.015    1.096  ...   103.0      82.0      60.0   1.01

[8 rows x 11 columns]