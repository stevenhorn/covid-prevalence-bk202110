4 Divergences 
Passed validation 
2021-08-08 date of last case count
Computed from 80 by 513 log-likelihood matrix

         Estimate       SE
elpd_loo -2036.15    39.73
p_loo       39.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      448   87.3%
 (0.5, 0.7]   (ok)         54   10.5%
   (0.7, 1]   (bad)        10    1.9%
   (1, Inf)   (very bad)    1    0.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.280   0.044   0.217    0.346  ...    89.0      56.0      37.0   1.04
pu         0.887   0.012   0.861    0.900  ...    55.0      73.0      60.0   1.02
mu         0.177   0.022   0.139    0.216  ...    12.0      10.0      40.0   1.19
mus        0.192   0.031   0.142    0.247  ...   104.0     106.0     102.0   0.98
gamma      0.277   0.040   0.205    0.345  ...    90.0      93.0      93.0   1.02
Is_begin   7.387   6.226   0.251   19.438  ...    64.0      47.0      49.0   1.04
Ia_begin  77.560  33.433  26.018  140.337  ...    86.0      81.0      56.0   0.99
E_begin   54.211  29.990   8.125  110.126  ...    99.0     109.0      72.0   1.00

[8 rows x 11 columns]