1 Divergences 
Passed validation 
2021-08-08 date of last case count
Computed from 80 by 513 log-likelihood matrix

         Estimate       SE
elpd_loo -1994.41    32.71
p_loo       33.49        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      477   93.0%
 (0.5, 0.7]   (ok)         27    5.3%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.247   0.053   0.167    0.335  ...   152.0     152.0      75.0   1.05
pu         0.803   0.061   0.710    0.900  ...    19.0      28.0      69.0   1.06
mu         0.131   0.023   0.091    0.171  ...     7.0       7.0      29.0   1.25
mus        0.195   0.030   0.149    0.265  ...    84.0      76.0      60.0   0.99
gamma      0.261   0.041   0.193    0.335  ...    97.0      88.0      88.0   1.01
Is_begin   6.679   4.894   0.500   15.548  ...    79.0      57.0      93.0   1.02
Ia_begin  34.828  24.414   3.666   80.710  ...    57.0      42.0      59.0   1.06
E_begin   19.366  16.300   0.067   50.461  ...    13.0       8.0      17.0   1.23

[8 rows x 11 columns]