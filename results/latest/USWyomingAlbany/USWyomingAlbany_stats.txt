0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1506.49    35.74
p_loo       42.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      461   93.7%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.052   0.177    0.343  ...    13.0      13.0      17.0   1.13
pu        0.752  0.026   0.713    0.799  ...    10.0      10.0      32.0   1.17
mu        0.130  0.021   0.096    0.165  ...    34.0      30.0      60.0   1.06
mus       0.197  0.028   0.145    0.244  ...   152.0     152.0      38.0   1.07
gamma     0.279  0.056   0.198    0.387  ...    77.0      76.0      60.0   1.04
Is_begin  0.683  0.615   0.007    1.874  ...    91.0     111.0      60.0   1.04
Ia_begin  1.525  1.736   0.001    4.468  ...    96.0      50.0      40.0   1.05
E_begin   0.622  0.615   0.021    1.690  ...   101.0      37.0      54.0   1.04

[8 rows x 11 columns]