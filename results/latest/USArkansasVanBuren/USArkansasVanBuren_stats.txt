0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1164.21    54.12
p_loo       50.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         26    5.3%
   (0.7, 1]   (bad)        10    2.0%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.042   0.155    0.295  ...    76.0      73.0      60.0   1.02
pu        0.724  0.020   0.700    0.764  ...    78.0      58.0      39.0   1.03
mu        0.129  0.024   0.089    0.176  ...    46.0      41.0      40.0   1.00
mus       0.212  0.037   0.161    0.297  ...    30.0      36.0      59.0   1.04
gamma     0.277  0.047   0.201    0.381  ...    69.0      67.0      60.0   1.04
Is_begin  1.284  0.902   0.068    3.092  ...   152.0     152.0      93.0   0.98
Ia_begin  2.708  2.219   0.088    7.565  ...    87.0      81.0      60.0   1.00
E_begin   1.284  1.235   0.070    3.796  ...    66.0      15.0      25.0   1.11

[8 rows x 11 columns]