0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1640.36    76.27
p_loo       60.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      468   95.1%
 (0.5, 0.7]   (ok)         19    3.9%
   (0.7, 1]   (bad)         1    0.2%
   (1, Inf)   (very bad)    4    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.201  0.033   0.154    0.263  ...    92.0     103.0      60.0   1.02
pu        0.719  0.015   0.700    0.746  ...    37.0      27.0      57.0   1.09
mu        0.137  0.021   0.104    0.175  ...     7.0       8.0      81.0   1.25
mus       0.262  0.035   0.205    0.328  ...    69.0      67.0      83.0   1.02
gamma     0.474  0.067   0.360    0.606  ...    88.0      93.0      95.0   1.01
Is_begin  0.796  0.838   0.007    2.557  ...    95.0      73.0      60.0   1.01
Ia_begin  1.270  1.336   0.004    4.173  ...    18.0       8.0      15.0   1.22
E_begin   0.580  0.584   0.037    1.636  ...    93.0      70.0      56.0   1.05

[8 rows x 11 columns]