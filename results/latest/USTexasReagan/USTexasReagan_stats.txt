0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -964.96    76.00
p_loo       57.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      473   96.1%
 (0.5, 0.7]   (ok)         14    2.8%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.231  0.047   0.161    0.315  ...    62.0      80.0      56.0   1.05
pu        0.734  0.020   0.701    0.763  ...   124.0     100.0      59.0   1.00
mu        0.129  0.021   0.095    0.171  ...    40.0      50.0      60.0   1.04
mus       0.204  0.034   0.152    0.266  ...    56.0      56.0      43.0   1.00
gamma     0.248  0.048   0.166    0.336  ...    48.0      45.0      91.0   1.03
Is_begin  0.403  0.422   0.000    1.263  ...    25.0      25.0      81.0   1.07
Ia_begin  0.512  0.740   0.003    2.126  ...    36.0      31.0     100.0   1.03
E_begin   0.268  0.421   0.003    0.897  ...    70.0      28.0      40.0   1.06

[8 rows x 11 columns]