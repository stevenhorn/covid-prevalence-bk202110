0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1684.47    38.96
p_loo       33.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.7%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.053   0.168    0.336  ...   103.0     103.0      99.0   1.06
pu        0.828  0.017   0.785    0.849  ...    49.0      58.0      22.0   1.03
mu        0.132  0.023   0.091    0.165  ...     7.0       7.0      20.0   1.28
mus       0.184  0.032   0.134    0.245  ...    67.0      42.0      45.0   1.04
gamma     0.198  0.040   0.130    0.262  ...    18.0      14.0      59.0   1.12
Is_begin  1.057  0.802   0.006    2.688  ...    41.0      43.0      57.0   1.04
Ia_begin  0.736  0.521   0.008    1.670  ...    38.0      33.0      31.0   1.08
E_begin   0.663  0.685   0.013    2.049  ...    24.0      24.0      24.0   1.10

[8 rows x 11 columns]