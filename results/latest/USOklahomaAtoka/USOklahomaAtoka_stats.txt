0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1363.41    56.14
p_loo       53.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      471   95.7%
 (0.5, 0.7]   (ok)         15    3.0%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.203  0.040   0.152    0.280  ...    59.0      78.0      58.0   1.00
pu        0.714  0.011   0.701    0.738  ...   115.0      93.0      60.0   1.01
mu        0.130  0.019   0.091    0.160  ...    35.0      35.0      43.0   1.01
mus       0.207  0.035   0.142    0.277  ...   152.0     152.0      59.0   0.99
gamma     0.245  0.042   0.161    0.303  ...   152.0     152.0      74.0   1.09
Is_begin  0.571  0.602   0.007    1.880  ...   103.0     101.0      88.0   1.01
Ia_begin  0.943  0.844   0.014    2.935  ...    77.0     101.0      88.0   1.03
E_begin   0.496  0.598   0.008    1.378  ...   105.0      51.0      40.0   1.04

[8 rows x 11 columns]