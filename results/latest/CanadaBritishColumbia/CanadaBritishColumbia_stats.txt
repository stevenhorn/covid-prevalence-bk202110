0 Divergences 
Passed validation 
2021-08-07 date of last case count
Computed from 80 by 512 log-likelihood matrix

         Estimate       SE
elpd_loo -3254.10    58.00
p_loo       23.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      505   98.6%
 (0.5, 0.7]   (ok)          5    1.0%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    0    0.0%

             mean      sd   hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.342   0.002    0.340    0.345  ...     3.0       3.0      40.0   2.05
pu          0.895   0.001    0.895    0.896  ...     3.0       3.0      16.0   1.96
mu          0.272   0.005    0.268    0.277  ...     3.0       4.0      42.0   1.87
mus         0.192   0.002    0.190    0.194  ...     3.0       3.0      14.0   2.27
gamma       0.102   0.006    0.096    0.109  ...     3.0       3.0      17.0   2.16
Is_begin  121.863  13.742  107.834  135.993  ...     3.0       3.0      30.0   1.89
Ia_begin  154.890  37.209  117.624  192.686  ...     3.0       3.0      32.0   2.13
E_begin    61.780  18.503   42.853   80.693  ...     3.0       3.0      14.0   2.24

[8 rows x 11 columns]