0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1422.72    64.39
p_loo       65.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.052   0.157    0.326  ...    88.0      81.0      40.0   1.00
pu        0.812  0.025   0.768    0.856  ...    16.0      15.0      59.0   1.10
mu        0.124  0.025   0.073    0.169  ...    27.0      35.0      18.0   1.02
mus       0.178  0.031   0.116    0.227  ...    76.0      76.0     100.0   1.03
gamma     0.214  0.044   0.132    0.296  ...    27.0      29.0      69.0   1.06
Is_begin  0.820  0.701   0.021    2.041  ...   116.0      64.0      40.0   1.05
Ia_begin  1.309  1.430   0.056    3.581  ...    86.0      71.0      60.0   0.98
E_begin   0.699  0.719   0.018    2.324  ...    25.0      23.0      59.0   1.07

[8 rows x 11 columns]