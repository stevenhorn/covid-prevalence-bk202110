0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1037.21    37.21
p_loo       37.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         26    5.3%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.059   0.158    0.346  ...    55.0      58.0      50.0   1.03
pu        0.802  0.034   0.731    0.849  ...     6.0       7.0      19.0   1.26
mu        0.141  0.025   0.096    0.185  ...    33.0      31.0      20.0   1.04
mus       0.186  0.026   0.140    0.237  ...   142.0     145.0      75.0   1.01
gamma     0.246  0.040   0.166    0.305  ...    92.0      93.0      93.0   0.99
Is_begin  1.093  0.833   0.055    2.914  ...   120.0     127.0      83.0   0.99
Ia_begin  2.269  1.933   0.030    5.906  ...    73.0      49.0      84.0   1.03
E_begin   1.425  1.392   0.047    3.950  ...    30.0      31.0      69.0   1.06

[8 rows x 11 columns]