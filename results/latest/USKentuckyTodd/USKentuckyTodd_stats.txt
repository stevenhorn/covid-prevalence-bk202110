0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1147.62    43.06
p_loo       47.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      462   93.9%
 (0.5, 0.7]   (ok)         21    4.3%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.051   0.168    0.331  ...    91.0      98.0      96.0   1.03
pu        0.732  0.017   0.701    0.764  ...    67.0      61.0      60.0   1.02
mu        0.152  0.027   0.105    0.194  ...    33.0      32.0      39.0   1.04
mus       0.237  0.037   0.180    0.312  ...   109.0     119.0      87.0   1.14
gamma     0.325  0.056   0.220    0.425  ...   152.0     143.0      60.0   0.98
Is_begin  0.824  0.623   0.047    2.186  ...    91.0      83.0      60.0   0.98
Ia_begin  1.484  1.333   0.024    4.144  ...    71.0      63.0      58.0   1.03
E_begin   0.603  0.509   0.003    1.615  ...    91.0      76.0      61.0   1.02

[8 rows x 11 columns]