0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -855.27    38.18
p_loo       41.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      450   91.5%
 (0.5, 0.7]   (ok)         36    7.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.054   0.161    0.337  ...    18.0      26.0      60.0   1.08
pu        0.753  0.030   0.704    0.801  ...     7.0       8.0      48.0   1.25
mu        0.123  0.024   0.062    0.154  ...   151.0     152.0      95.0   1.01
mus       0.189  0.035   0.130    0.259  ...   135.0     141.0     100.0   1.01
gamma     0.214  0.040   0.138    0.279  ...    92.0     101.0      61.0   1.02
Is_begin  0.398  0.461   0.003    1.234  ...    95.0      82.0      60.0   0.99
Ia_begin  0.549  0.673   0.002    1.687  ...    86.0      86.0      60.0   0.99
E_begin   0.263  0.365   0.002    0.931  ...    87.0      88.0      91.0   0.99

[8 rows x 11 columns]