1 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2526.83    38.74
p_loo       29.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      440   89.4%
 (0.5, 0.7]   (ok)         42    8.5%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.054   0.161    0.331  ...     7.0       8.0      38.0   1.22
pu        0.792  0.024   0.753    0.825  ...     5.0       4.0      18.0   1.56
mu        0.129  0.012   0.107    0.146  ...    15.0      13.0      43.0   1.14
mus       0.155  0.031   0.122    0.233  ...     8.0       8.0      15.0   1.20
gamma     0.186  0.031   0.142    0.247  ...    44.0      44.0      58.0   1.04
Is_begin  2.044  1.509   0.110    4.397  ...     4.0       4.0      18.0   1.73
Ia_begin  2.291  1.926   0.038    6.107  ...     7.0       7.0      37.0   1.23
E_begin   1.417  1.093   0.052    3.223  ...     9.0       7.0      62.0   1.24

[8 rows x 11 columns]