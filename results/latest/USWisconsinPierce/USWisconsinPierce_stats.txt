0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1546.10    79.78
p_loo       56.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.047   0.170    0.321  ...   116.0     126.0      34.0   1.02
pu        0.742  0.024   0.704    0.782  ...    47.0      46.0      43.0   1.00
mu        0.122  0.018   0.090    0.151  ...    57.0      55.0      40.0   1.00
mus       0.309  0.057   0.223    0.411  ...   122.0     131.0      60.0   1.01
gamma     0.416  0.088   0.297    0.601  ...    60.0      82.0      40.0   1.03
Is_begin  0.840  0.882   0.013    2.578  ...    99.0      88.0      72.0   1.04
Ia_begin  1.221  1.099   0.035    3.132  ...    64.0      82.0      39.0   1.06
E_begin   0.551  0.554   0.013    1.767  ...    90.0     105.0      95.0   1.01

[8 rows x 11 columns]