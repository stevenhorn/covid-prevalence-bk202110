0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1533.76    41.91
p_loo       45.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      433   88.0%
 (0.5, 0.7]   (ok)         47    9.6%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.048   0.172    0.331  ...    17.0      16.0      65.0   1.09
pu        0.817  0.024   0.775    0.853  ...    42.0      43.0      54.0   1.02
mu        0.159  0.020   0.122    0.197  ...    31.0      28.0      44.0   1.03
mus       0.179  0.029   0.139    0.246  ...    67.0      65.0      44.0   1.02
gamma     0.238  0.040   0.172    0.298  ...    85.0      98.0      60.0   1.02
Is_begin  0.851  0.746   0.007    2.074  ...    84.0      50.0      22.0   1.03
Ia_begin  2.023  1.897   0.125    5.247  ...    86.0      70.0      80.0   1.02
E_begin   0.979  1.176   0.011    2.983  ...    83.0      60.0      60.0   1.04

[8 rows x 11 columns]