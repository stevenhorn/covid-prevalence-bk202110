6 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -3362.94    39.93
p_loo       35.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      445   90.4%
 (0.5, 0.7]   (ok)         39    7.9%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    3    0.6%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.189   0.032   0.153    0.256  ...    90.0      75.0      91.0   1.00
pu         0.716   0.013   0.700    0.742  ...    99.0      94.0      60.0   0.99
mu         0.098   0.015   0.078    0.132  ...    57.0      53.0      60.0   1.05
mus        0.151   0.026   0.107    0.194  ...   152.0     152.0      96.0   0.99
gamma      0.220   0.048   0.144    0.301  ...    65.0      68.0      56.0   1.00
Is_begin   5.790   4.870   0.069   15.163  ...    62.0      55.0      59.0   1.02
Ia_begin  10.727  11.389   0.036   35.516  ...    82.0      43.0      22.0   1.04
E_begin    6.432   6.786   0.146   18.642  ...    53.0      67.0      59.0   1.00

[8 rows x 11 columns]