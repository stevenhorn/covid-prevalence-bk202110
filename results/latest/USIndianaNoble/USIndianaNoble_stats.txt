0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1537.70    36.54
p_loo       37.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.232  0.054   0.155    0.337  ...    65.0     127.0      35.0   1.04
pu        0.735  0.027   0.701    0.784  ...    62.0      63.0      24.0   1.04
mu        0.102  0.018   0.073    0.137  ...    73.0      65.0      37.0   1.10
mus       0.160  0.032   0.104    0.212  ...   114.0     137.0      58.0   0.99
gamma     0.203  0.043   0.144    0.306  ...   129.0     152.0      32.0   1.03
Is_begin  0.539  0.550   0.015    1.656  ...   132.0      80.0      60.0   1.00
Ia_begin  1.042  1.291   0.032    3.776  ...   104.0     152.0      88.0   1.01
E_begin   0.493  0.520   0.017    1.454  ...    97.0     142.0      88.0   1.00

[8 rows x 11 columns]