0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -913.53    88.76
p_loo       73.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      463   94.1%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.201  0.034   0.150    0.269  ...    12.0      20.0      24.0   1.13
pu        0.717  0.015   0.700    0.745  ...    17.0      12.0      40.0   1.14
mu        0.098  0.016   0.073    0.129  ...    24.0      31.0      56.0   1.07
mus       0.261  0.033   0.204    0.320  ...    97.0      95.0      83.0   1.00
gamma     0.421  0.068   0.310    0.564  ...    29.0      32.0      54.0   1.05
Is_begin  0.407  0.408   0.002    1.189  ...    40.0     152.0      74.0   1.11
Ia_begin  0.542  0.973   0.014    2.018  ...    67.0      47.0      60.0   1.03
E_begin   0.234  0.238   0.008    0.668  ...    95.0      72.0      65.0   1.00

[8 rows x 11 columns]