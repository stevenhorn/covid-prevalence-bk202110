0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1567.37    37.22
p_loo       40.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.269  0.050   0.181    0.343  ...    46.0      51.0      60.0   1.00
pu        0.749  0.024   0.711    0.791  ...    36.0      34.0      56.0   1.05
mu        0.115  0.021   0.082    0.160  ...    37.0      36.0      60.0   1.02
mus       0.173  0.033   0.116    0.221  ...   134.0     144.0      59.0   1.06
gamma     0.214  0.042   0.157    0.303  ...    82.0      89.0      93.0   1.00
Is_begin  0.746  0.792   0.007    2.574  ...   107.0      90.0      59.0   1.00
Ia_begin  1.150  1.455   0.011    3.860  ...    98.0     102.0      96.0   0.99
E_begin   0.532  0.677   0.009    1.326  ...    57.0      58.0      53.0   1.00

[8 rows x 11 columns]