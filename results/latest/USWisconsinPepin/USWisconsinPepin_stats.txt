0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -930.87    42.99
p_loo       45.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.7%
 (0.5, 0.7]   (ok)         22    4.5%
   (0.7, 1]   (bad)        13    2.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.049   0.179    0.348  ...    83.0      85.0      39.0   1.03
pu        0.747  0.023   0.706    0.791  ...    13.0      17.0      80.0   1.11
mu        0.151  0.028   0.115    0.215  ...    12.0      15.0      31.0   1.08
mus       0.214  0.041   0.139    0.292  ...   143.0     152.0      47.0   1.03
gamma     0.254  0.047   0.182    0.355  ...    83.0      88.0      88.0   1.02
Is_begin  0.933  0.852   0.044    2.364  ...    76.0      71.0     100.0   1.04
Ia_begin  2.292  2.507   0.055    7.687  ...    49.0      44.0      53.0   1.03
E_begin   1.049  1.457   0.020    4.150  ...    42.0      34.0      57.0   1.07

[8 rows x 11 columns]