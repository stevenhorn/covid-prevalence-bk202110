0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -795.62    36.62
p_loo       36.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      467   94.9%
 (0.5, 0.7]   (ok)         17    3.5%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.063   0.160    0.350  ...    42.0      35.0      40.0   1.08
pu        0.800  0.021   0.764    0.837  ...    17.0      16.0      24.0   1.09
mu        0.123  0.024   0.085    0.164  ...     9.0      12.0      72.0   1.15
mus       0.189  0.037   0.113    0.237  ...    91.0      94.0      83.0   1.00
gamma     0.213  0.042   0.137    0.297  ...   116.0     143.0      59.0   1.00
Is_begin  0.576  0.690   0.010    2.412  ...   109.0      72.0      60.0   1.01
Ia_begin  1.024  1.167   0.007    3.620  ...    74.0      96.0      95.0   1.02
E_begin   0.434  0.483   0.006    1.359  ...    67.0      55.0      58.0   1.01

[8 rows x 11 columns]