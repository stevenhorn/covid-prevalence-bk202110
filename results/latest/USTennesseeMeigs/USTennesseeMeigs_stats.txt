0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1116.20    30.01
p_loo       28.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      458   93.1%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.050   0.177    0.345  ...    32.0      39.0      91.0   1.04
pu        0.781  0.025   0.736    0.827  ...    12.0      13.0      38.0   1.12
mu        0.142  0.024   0.092    0.176  ...    38.0      39.0      54.0   1.00
mus       0.180  0.034   0.122    0.251  ...    69.0      73.0      58.0   0.99
gamma     0.209  0.041   0.147    0.282  ...    44.0      45.0      59.0   1.05
Is_begin  0.646  0.603   0.001    1.850  ...    80.0      98.0      80.0   1.02
Ia_begin  1.464  1.588   0.008    4.626  ...    65.0      55.0      33.0   1.01
E_begin   0.612  0.704   0.012    2.122  ...    60.0      54.0      59.0   1.05

[8 rows x 11 columns]