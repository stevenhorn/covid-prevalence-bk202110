0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2579.81    37.14
p_loo       31.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         34    6.9%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.057   0.160    0.346  ...    62.0      67.0      59.0   1.02
pu        0.790  0.040   0.719    0.848  ...    23.0      24.0      24.0   1.07
mu        0.128  0.023   0.088    0.171  ...    25.0      24.0      34.0   1.03
mus       0.200  0.032   0.139    0.257  ...   121.0     122.0      54.0   1.02
gamma     0.252  0.044   0.179    0.340  ...   100.0     106.0      93.0   0.99
Is_begin  2.460  1.675   0.063    6.192  ...   114.0      63.0      29.0   1.02
Ia_begin  6.773  4.170   1.418   14.336  ...    28.0      21.0      58.0   1.08
E_begin   4.278  3.741   0.115   11.951  ...    15.0      26.0      29.0   1.08

[8 rows x 11 columns]