0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1711.68    42.84
p_loo       34.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      461   93.7%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.047   0.168    0.320  ...    90.0      93.0      60.0   1.13
pu        0.770  0.035   0.712    0.835  ...    11.0      12.0      60.0   1.13
mu        0.141  0.023   0.090    0.174  ...    76.0      77.0      42.0   1.01
mus       0.196  0.031   0.136    0.241  ...    72.0      89.0      97.0   1.11
gamma     0.281  0.045   0.200    0.354  ...   152.0     152.0     100.0   1.00
Is_begin  1.753  1.166   0.040    3.932  ...    18.0      16.0      22.0   1.10
Ia_begin  5.173  2.802   0.640    9.960  ...    51.0      46.0      32.0   1.05
E_begin   4.454  3.205   0.121    9.171  ...    55.0      42.0      51.0   1.04

[8 rows x 11 columns]