0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2618.83    46.06
p_loo       43.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.231  0.048   0.163    0.315  ...   136.0     107.0      16.0   1.07
pu        0.736  0.022   0.706    0.781  ...   152.0     152.0     100.0   1.02
mu        0.107  0.018   0.069    0.135  ...    72.0      72.0      46.0   1.01
mus       0.167  0.038   0.106    0.248  ...    91.0     141.0      46.0   1.02
gamma     0.207  0.035   0.134    0.272  ...    40.0      42.0      81.0   1.06
Is_begin  1.763  1.688   0.000    4.560  ...   122.0     141.0      60.0   1.01
Ia_begin  3.551  2.495   0.269    8.334  ...    66.0      22.0      47.0   1.07
E_begin   1.921  2.481   0.032    5.317  ...    93.0     114.0      62.0   0.98

[8 rows x 11 columns]