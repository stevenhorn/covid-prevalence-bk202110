0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -979.08    36.34
p_loo       40.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.7%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.229  0.047   0.153    0.317  ...    48.0      53.0      59.0   1.05
pu        0.737  0.022   0.702    0.767  ...    25.0      28.0      38.0   1.10
mu        0.148  0.028   0.093    0.193  ...    20.0      21.0      51.0   1.09
mus       0.229  0.037   0.167    0.295  ...    69.0      78.0      58.0   0.98
gamma     0.283  0.052   0.200    0.377  ...   152.0     152.0      77.0   0.99
Is_begin  0.638  0.601   0.017    1.729  ...   105.0      75.0      96.0   1.02
Ia_begin  1.032  1.229   0.014    3.355  ...   115.0      75.0      59.0   1.00
E_begin   0.423  0.495   0.001    1.318  ...   110.0      76.0      42.0   1.01

[8 rows x 11 columns]