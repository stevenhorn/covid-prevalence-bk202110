0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1128.96    32.53
p_loo       29.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      471   95.7%
 (0.5, 0.7]   (ok)         19    3.9%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.052   0.163    0.337  ...    81.0      81.0      59.0   1.00
pu        0.862  0.014   0.844    0.890  ...    17.0      17.0      59.0   1.09
mu        0.126  0.024   0.087    0.173  ...     8.0       8.0      40.0   1.23
mus       0.159  0.030   0.109    0.214  ...   112.0     113.0      88.0   0.98
gamma     0.179  0.036   0.128    0.256  ...    68.0      62.0      59.0   1.00
Is_begin  0.967  0.966   0.032    3.428  ...    74.0      59.0      65.0   1.05
Ia_begin  2.203  2.163   0.031    7.076  ...    59.0      64.0      69.0   1.01
E_begin   1.124  1.377   0.009    3.679  ...    87.0      76.0      54.0   1.01

[8 rows x 11 columns]