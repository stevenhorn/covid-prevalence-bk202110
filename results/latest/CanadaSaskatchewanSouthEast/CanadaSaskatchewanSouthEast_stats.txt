0 Divergences 
Passed validation 
2021-08-08 date of last case count
Computed from 80 by 370 log-likelihood matrix

         Estimate       SE
elpd_loo  -964.01    22.94
p_loo       26.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      343   92.7%
 (0.5, 0.7]   (ok)         23    6.2%
   (0.7, 1]   (bad)         4    1.1%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.060   0.166    0.349  ...    89.0      87.0      59.0   1.08
pu        0.846  0.037   0.784    0.894  ...    24.0      31.0      40.0   1.04
mu        0.167  0.029   0.119    0.217  ...    19.0      18.0      44.0   1.12
mus       0.192  0.028   0.147    0.240  ...    80.0      76.0      60.0   0.99
gamma     0.309  0.045   0.220    0.390  ...   103.0     103.0      93.0   1.00
Is_begin  1.286  1.544   0.000    4.987  ...    82.0      28.0      22.0   1.05
Ia_begin  1.512  1.496   0.006    4.784  ...    19.0      11.0      80.0   1.16
E_begin   0.716  0.808   0.020    2.663  ...    68.0      39.0      60.0   1.00

[8 rows x 11 columns]