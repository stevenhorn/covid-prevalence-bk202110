0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -719.10    32.10
p_loo       36.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.048   0.173    0.334  ...    94.0      98.0      59.0   0.98
pu        0.834  0.016   0.798    0.857  ...    63.0      70.0      60.0   1.00
mu        0.152  0.025   0.104    0.193  ...    31.0      31.0      40.0   1.12
mus       0.210  0.033   0.150    0.260  ...   144.0     137.0      72.0   0.99
gamma     0.263  0.056   0.153    0.357  ...    76.0      97.0      43.0   1.01
Is_begin  0.715  0.656   0.000    1.955  ...   134.0     130.0      67.0   1.00
Ia_begin  1.442  1.444   0.029    4.565  ...   125.0     104.0      72.0   1.00
E_begin   0.548  0.677   0.006    1.643  ...    80.0      80.0      87.0   1.00

[8 rows x 11 columns]