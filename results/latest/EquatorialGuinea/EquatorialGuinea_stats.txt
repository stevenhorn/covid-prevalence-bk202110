0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -2280.72    61.93
p_loo       65.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      473   95.9%
 (0.5, 0.7]   (ok)         10    2.0%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.292  0.042   0.203    0.348  ...    17.0      14.0      39.0   1.12
pu        0.888  0.009   0.870    0.898  ...    65.0      79.0      79.0   1.06
mu        0.262  0.024   0.216    0.305  ...     7.0       7.0      27.0   1.27
mus       0.310  0.047   0.240    0.411  ...    80.0      90.0      57.0   1.00
gamma     0.445  0.077   0.301    0.593  ...    40.0      30.0      59.0   1.06
Is_begin  0.690  0.531   0.002    1.817  ...    72.0      59.0      24.0   1.02
Ia_begin  1.754  1.421   0.052    4.289  ...    60.0      58.0      30.0   1.01
E_begin   1.533  1.907   0.003    5.711  ...    63.0      58.0      91.0   1.03

[8 rows x 11 columns]