0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -3348.81    66.18
p_loo       52.04        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.5%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    2    0.4%

             mean       sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.240    0.056   0.156    0.340  ...    19.0      14.0      14.0   1.11
pu          0.786    0.057   0.700    0.876  ...    10.0      10.0      32.0   1.18
mu          0.136    0.030   0.092    0.189  ...     4.0       4.0      30.0   1.71
mus         0.199    0.037   0.131    0.255  ...    15.0      16.0      59.0   1.13
gamma       0.235    0.048   0.153    0.326  ...    53.0      47.0      18.0   1.02
Is_begin   83.047   59.804   1.788  189.660  ...    11.0       8.0      16.0   1.22
Ia_begin  202.842  108.282  36.262  381.147  ...    71.0      63.0      60.0   1.00
E_begin   210.030  176.919   1.090  586.977  ...    64.0      40.0      22.0   1.03

[8 rows x 11 columns]