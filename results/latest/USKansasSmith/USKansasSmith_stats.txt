0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -692.58    37.61
p_loo       32.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      468   95.1%
 (0.5, 0.7]   (ok)         18    3.7%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.045   0.178    0.324  ...    82.0      86.0      93.0   0.99
pu        0.797  0.017   0.766    0.829  ...    66.0      62.0      86.0   1.01
mu        0.128  0.021   0.095    0.174  ...    32.0      33.0      37.0   1.00
mus       0.173  0.025   0.128    0.222  ...    70.0      72.0      56.0   1.00
gamma     0.191  0.049   0.110    0.278  ...    40.0      44.0      31.0   1.05
Is_begin  0.502  0.467   0.010    1.404  ...    64.0      48.0      80.0   1.05
Ia_begin  0.913  1.226   0.000    3.447  ...    15.0      12.0      54.0   1.15
E_begin   0.372  0.393   0.003    1.074  ...    40.0      17.0      43.0   1.08

[8 rows x 11 columns]