0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1899.43    39.22
p_loo       33.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      451   91.7%
 (0.5, 0.7]   (ok)         36    7.3%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.057   0.151    0.336  ...    61.0      62.0      40.0   0.99
pu        0.764  0.024   0.727    0.805  ...    24.0      26.0      60.0   1.10
mu        0.107  0.021   0.083    0.152  ...    86.0      77.0      80.0   1.00
mus       0.161  0.028   0.122    0.202  ...   152.0     152.0      96.0   1.01
gamma     0.181  0.029   0.130    0.229  ...    83.0      94.0      69.0   0.99
Is_begin  1.028  0.899   0.031    2.957  ...   109.0     103.0      81.0   1.02
Ia_begin  0.689  0.560   0.012    1.700  ...    17.0      16.0      40.0   1.10
E_begin   0.646  0.768   0.022    1.859  ...    51.0      25.0      60.0   1.06

[8 rows x 11 columns]