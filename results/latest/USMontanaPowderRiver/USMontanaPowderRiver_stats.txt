0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -556.42    56.11
p_loo       56.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         23    4.7%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    4    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.043   0.179    0.331  ...    55.0      57.0      80.0   1.04
pu        0.760  0.026   0.708    0.801  ...    53.0      54.0      84.0   1.01
mu        0.133  0.022   0.091    0.170  ...    78.0      80.0      59.0   1.01
mus       0.205  0.038   0.144    0.266  ...   139.0     133.0      92.0   1.01
gamma     0.274  0.052   0.183    0.389  ...   152.0     152.0      49.0   1.03
Is_begin  0.345  0.428   0.001    1.424  ...    59.0      44.0      40.0   1.05
Ia_begin  0.678  0.821   0.003    2.900  ...    61.0      78.0      59.0   1.02
E_begin   0.289  0.344   0.003    0.913  ...    29.0      47.0      57.0   1.05

[8 rows x 11 columns]