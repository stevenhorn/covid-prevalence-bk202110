0 Divergences 
Passed validation 
2021-08-08 date of last case count
Computed from 80 by 513 log-likelihood matrix

         Estimate       SE
elpd_loo -1693.57    51.87
p_loo       53.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      484   94.3%
 (0.5, 0.7]   (ok)         23    4.5%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.269  0.005   0.259    0.275  ...     4.0       5.0      24.0   1.46
pu         0.818  0.003   0.814    0.824  ...     3.0       3.0      14.0   2.33
mu         0.345  0.023   0.318    0.369  ...     3.0       3.0      15.0   1.95
mus        0.318  0.012   0.299    0.333  ...     3.0       3.0      14.0   2.02
gamma      0.204  0.005   0.198    0.213  ...     3.0       3.0      14.0   1.95
Is_begin   9.759  3.624   5.488   16.192  ...     3.0       3.0      18.0   2.23
Ia_begin  13.590  8.972   3.064   25.398  ...     3.0       3.0      21.0   2.15
E_begin    4.964  2.015   2.667    8.101  ...     3.0       3.0      22.0   2.53

[8 rows x 11 columns]