0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1554.91    30.10
p_loo       29.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   93.3%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.211  0.042   0.151    0.287  ...    44.0      38.0      33.0   1.09
pu        0.725  0.014   0.700    0.751  ...    55.0      52.0      39.0   1.02
mu        0.131  0.018   0.109    0.177  ...    14.0      16.0      49.0   1.11
mus       0.178  0.024   0.136    0.222  ...    70.0      71.0      60.0   1.03
gamma     0.257  0.043   0.195    0.346  ...   152.0     152.0      42.0   1.10
Is_begin  0.877  0.731   0.061    2.381  ...   136.0     108.0      57.0   1.01
Ia_begin  1.895  1.849   0.066    4.991  ...   125.0     117.0      46.0   1.00
E_begin   0.742  0.639   0.018    1.921  ...    63.0      20.0      88.0   1.10

[8 rows x 11 columns]