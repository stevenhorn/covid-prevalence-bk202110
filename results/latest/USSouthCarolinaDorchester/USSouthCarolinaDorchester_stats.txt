0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2035.55    39.37
p_loo       41.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      470   95.5%
 (0.5, 0.7]   (ok)         14    2.8%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.198  0.032   0.152    0.252  ...   134.0     152.0      91.0   1.01
pu        0.713  0.013   0.700    0.740  ...   104.0      80.0      60.0   1.02
mu        0.116  0.020   0.081    0.156  ...    46.0      41.0      64.0   1.02
mus       0.170  0.029   0.114    0.225  ...    85.0      74.0      55.0   1.04
gamma     0.239  0.040   0.162    0.302  ...   105.0      98.0      80.0   1.01
Is_begin  1.139  1.144   0.008    3.377  ...    90.0      74.0      29.0   1.00
Ia_begin  1.922  1.433   0.098    4.296  ...    38.0      32.0      29.0   1.06
E_begin   0.942  0.964   0.001    2.942  ...    88.0      55.0      17.0   0.99

[8 rows x 11 columns]