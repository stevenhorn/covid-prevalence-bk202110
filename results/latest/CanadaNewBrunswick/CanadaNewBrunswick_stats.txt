0 Divergences 
Passed validation 
2021-08-07 date of last case count
Computed from 80 by 512 log-likelihood matrix

         Estimate       SE
elpd_loo -1242.67    31.09
p_loo       45.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      479   93.6%
 (0.5, 0.7]   (ok)         27    5.3%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.057   0.166    0.348  ...    45.0      44.0      22.0   1.03
pu        0.810  0.054   0.721    0.896  ...     4.0       5.0      40.0   1.50
mu        0.275  0.025   0.228    0.324  ...    14.0      13.0      31.0   1.14
mus       0.308  0.045   0.217    0.379  ...    49.0      55.0      40.0   1.03
gamma     0.554  0.078   0.383    0.673  ...    98.0      88.0      81.0   1.02
Is_begin  1.726  1.270   0.137    4.048  ...    87.0      71.0      87.0   1.01
Ia_begin  6.242  3.221   0.907   11.787  ...   152.0     114.0      40.0   1.06
E_begin   7.281  5.289   0.500   17.577  ...    36.0      28.0      60.0   1.07

[8 rows x 11 columns]