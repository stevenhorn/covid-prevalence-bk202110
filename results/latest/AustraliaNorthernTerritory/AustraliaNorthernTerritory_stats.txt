0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo  -574.53    40.70
p_loo       52.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      451   91.5%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    4    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.064   0.157    0.342  ...   152.0     152.0      51.0   0.99
pu        0.830  0.045   0.736    0.899  ...    18.0      23.0      32.0   1.05
mu        0.234  0.038   0.179    0.308  ...    10.0       9.0      33.0   1.17
mus       0.320  0.051   0.233    0.418  ...   136.0     124.0      60.0   1.04
gamma     0.476  0.074   0.366    0.620  ...    57.0      75.0      75.0   1.02
Is_begin  1.357  1.041   0.011    3.349  ...   104.0      85.0      91.0   1.01
Ia_begin  0.873  0.712   0.007    2.054  ...   152.0     152.0      53.0   1.06
E_begin   1.101  0.965   0.039    3.162  ...    81.0      42.0      62.0   1.03

[8 rows x 11 columns]