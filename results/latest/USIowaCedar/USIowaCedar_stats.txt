0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1092.86    32.64
p_loo       31.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      469   95.3%
 (0.5, 0.7]   (ok)         19    3.9%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.048   0.171    0.332  ...    91.0      94.0      88.0   1.00
pu        0.825  0.018   0.792    0.857  ...    33.0      34.0      17.0   1.04
mu        0.116  0.022   0.084    0.161  ...    12.0       9.0      18.0   1.18
mus       0.174  0.034   0.106    0.229  ...   152.0     152.0      74.0   1.01
gamma     0.202  0.044   0.135    0.291  ...   152.0     152.0      81.0   0.99
Is_begin  1.212  0.983   0.006    2.914  ...    97.0     115.0      60.0   1.02
Ia_begin  2.343  2.431   0.011    6.516  ...   107.0      68.0      14.0   1.00
E_begin   1.191  1.635   0.009    4.194  ...    84.0      64.0      69.0   1.03

[8 rows x 11 columns]