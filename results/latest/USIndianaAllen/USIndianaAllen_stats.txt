0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2327.72    42.53
p_loo       20.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      463   94.1%
 (0.5, 0.7]   (ok)         23    4.7%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.053   0.162    0.333  ...    62.0      62.0      40.0   1.00
pu        0.829  0.012   0.811    0.853  ...    49.0      44.0      46.0   1.03
mu        0.100  0.016   0.069    0.123  ...   114.0     115.0      93.0   0.99
mus       0.014  0.000   0.013    0.014  ...    60.0      55.0      75.0   1.04
gamma     0.296  0.049   0.232    0.403  ...   116.0     108.0      91.0   0.99
Is_begin  1.119  0.702   0.087    2.383  ...   152.0     131.0      91.0   0.99
Ia_begin  2.108  1.471   0.047    4.558  ...   152.0     152.0      48.0   1.04
E_begin   3.079  3.082   0.126   10.374  ...   138.0      85.0      95.0   1.01

[8 rows x 11 columns]