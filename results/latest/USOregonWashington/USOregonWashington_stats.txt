3 Divergences 
Passed validation 
2021-07-21 date of last case count
Computed from 80 by 494 log-likelihood matrix

         Estimate       SE
elpd_loo -2217.67    34.63
p_loo       31.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      443   89.7%
 (0.5, 0.7]   (ok)         40    8.1%
   (0.7, 1]   (bad)        10    2.0%
   (1, Inf)   (very bad)    1    0.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.250   0.058   0.162    0.345  ...   126.0     116.0      54.0   1.03
pu         0.766   0.041   0.701    0.841  ...    62.0      68.0      69.0   1.03
mu         0.143   0.027   0.102    0.187  ...    15.0      15.0      40.0   1.09
mus        0.183   0.029   0.129    0.242  ...    42.0      47.0      40.0   1.04
gamma      0.236   0.034   0.187    0.315  ...   129.0     116.0     100.0   0.99
Is_begin  10.255   6.064   2.476   20.583  ...    98.0      96.0      38.0   1.00
Ia_begin  22.339  11.560   2.842   41.014  ...   105.0     101.0      74.0   1.00
E_begin   20.702  14.985   0.069   45.058  ...    84.0      64.0      31.0   0.98

[8 rows x 11 columns]