6 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1515.31    37.41
p_loo       33.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.040   0.180    0.313  ...    12.0      19.0      33.0   1.09
pu        0.759  0.020   0.732    0.803  ...     8.0      10.0      31.0   1.18
mu        0.170  0.043   0.116    0.257  ...     4.0       4.0      14.0   1.78
mus       0.189  0.028   0.147    0.235  ...     8.0       8.0      40.0   1.25
gamma     0.214  0.034   0.159    0.280  ...    22.0      24.0      60.0   1.06
Is_begin  0.932  0.693   0.016    2.126  ...    10.0       8.0      20.0   1.21
Ia_begin  1.420  1.887   0.015    4.828  ...    13.0       9.0      20.0   1.19
E_begin   0.568  0.448   0.006    1.307  ...    12.0       8.0      20.0   1.25

[8 rows x 11 columns]