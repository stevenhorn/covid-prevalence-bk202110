0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1272.47    72.83
p_loo       54.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.9%
 (0.5, 0.7]   (ok)         33    6.7%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.047   0.176    0.324  ...    89.0     114.0      84.0   0.99
pu        0.769  0.027   0.719    0.818  ...    18.0      19.0      30.0   1.09
mu        0.143  0.021   0.102    0.175  ...    11.0      13.0      24.0   1.13
mus       0.299  0.049   0.209    0.389  ...    66.0      65.0      59.0   1.00
gamma     0.432  0.077   0.310    0.591  ...   104.0      82.0      87.0   1.17
Is_begin  0.962  0.984   0.000    2.722  ...   113.0      22.0      25.0   1.07
Ia_begin  1.570  1.614   0.091    4.466  ...    89.0      83.0     100.0   0.99
E_begin   0.734  0.849   0.005    2.787  ...    34.0      30.0      47.0   1.11

[8 rows x 11 columns]