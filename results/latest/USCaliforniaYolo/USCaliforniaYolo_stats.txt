0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1852.12    29.92
p_loo       33.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      451   91.7%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.052   0.163    0.338  ...    60.0      50.0      39.0   1.04
pu        0.765  0.042   0.701    0.825  ...    37.0      60.0      28.0   1.03
mu        0.139  0.025   0.081    0.179  ...    18.0      16.0      41.0   1.10
mus       0.177  0.031   0.132    0.250  ...    52.0      50.0      59.0   1.08
gamma     0.255  0.048   0.173    0.350  ...   152.0     146.0      80.0   0.99
Is_begin  0.825  0.602   0.028    2.076  ...   128.0     152.0      57.0   1.00
Ia_begin  1.534  1.096   0.098    3.392  ...   133.0     113.0      53.0   0.99
E_begin   1.107  1.136   0.024    3.333  ...    63.0     100.0      32.0   1.01

[8 rows x 11 columns]