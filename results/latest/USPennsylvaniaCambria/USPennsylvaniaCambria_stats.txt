8 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1764.37    51.59
p_loo       39.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      448   91.1%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)        11    2.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.055   0.162    0.331  ...    46.0      51.0      25.0   1.01
pu        0.760  0.031   0.708    0.806  ...    16.0      18.0      43.0   1.08
mu        0.117  0.018   0.082    0.145  ...    12.0      12.0      60.0   1.14
mus       0.190  0.032   0.131    0.242  ...    64.0      63.0     100.0   1.00
gamma     0.241  0.035   0.184    0.305  ...    81.0      83.0      80.0   1.02
Is_begin  0.598  0.547   0.001    1.725  ...    23.0      13.0      20.0   1.13
Ia_begin  1.555  1.499   0.032    4.382  ...    90.0      67.0      79.0   1.00
E_begin   0.547  0.619   0.016    1.831  ...    38.0      59.0      59.0   1.04

[8 rows x 11 columns]