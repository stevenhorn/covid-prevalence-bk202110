0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1238.58    83.95
p_loo       45.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      475   96.5%
 (0.5, 0.7]   (ok)         12    2.4%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.162  0.011   0.150    0.186  ...   126.0     152.0      43.0   1.03
pu        0.704  0.004   0.700    0.712  ...   126.0      74.0      36.0   1.02
mu        0.119  0.013   0.098    0.144  ...    66.0      64.0      49.0   1.04
mus       0.258  0.034   0.196    0.319  ...   124.0     122.0      48.0   1.18
gamma     0.576  0.089   0.423    0.739  ...   152.0     152.0      80.0   1.07
Is_begin  0.445  0.604   0.000    1.798  ...    56.0      14.0      38.0   1.13
Ia_begin  0.564  0.883   0.001    1.986  ...    51.0      10.0      49.0   1.17
E_begin   0.264  0.406   0.002    0.786  ...    53.0      22.0      88.0   1.11

[8 rows x 11 columns]