0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1274.23    40.73
p_loo       44.21        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      470   95.5%
 (0.5, 0.7]   (ok)         17    3.5%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.054   0.164    0.344  ...    24.0      23.0      40.0   1.08
pu        0.820  0.022   0.771    0.855  ...     6.0       6.0      34.0   1.36
mu        0.147  0.024   0.104    0.186  ...    10.0      10.0      28.0   1.20
mus       0.188  0.033   0.133    0.258  ...   127.0     137.0      86.0   1.00
gamma     0.260  0.045   0.189    0.346  ...    86.0      94.0      72.0   0.99
Is_begin  0.863  0.821   0.010    2.654  ...    78.0      65.0      99.0   1.01
Ia_begin  1.797  1.833   0.007    4.832  ...    87.0      62.0      59.0   1.00
E_begin   0.774  0.819   0.004    2.171  ...    48.0      33.0      69.0   1.04

[8 rows x 11 columns]