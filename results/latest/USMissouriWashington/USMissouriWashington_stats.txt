0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1619.24    93.36
p_loo       50.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      467   94.9%
 (0.5, 0.7]   (ok)         22    4.5%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.015   0.151    0.197  ...    62.0      60.0      57.0   1.03
pu        0.705  0.004   0.700    0.712  ...   150.0      94.0      55.0   1.02
mu        0.103  0.012   0.080    0.126  ...    12.0      12.0      16.0   1.13
mus       0.270  0.027   0.227    0.313  ...    89.0      91.0      59.0   1.00
gamma     0.621  0.083   0.482    0.746  ...   152.0     152.0      97.0   1.01
Is_begin  0.622  0.613   0.010    1.648  ...   106.0      93.0      73.0   0.99
Ia_begin  0.805  0.870   0.027    2.840  ...    70.0      94.0      60.0   1.01
E_begin   0.397  0.331   0.013    1.072  ...    77.0      64.0      55.0   1.02

[8 rows x 11 columns]