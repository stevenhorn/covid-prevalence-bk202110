0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1581.23    32.74
p_loo       37.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      458   93.1%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.056   0.152    0.333  ...    39.0      46.0      24.0   1.02
pu        0.762  0.034   0.707    0.813  ...     4.0       4.0      40.0   1.70
mu        0.122  0.027   0.089    0.181  ...    11.0      12.0      40.0   1.13
mus       0.185  0.029   0.135    0.242  ...    60.0      68.0      59.0   1.01
gamma     0.250  0.034   0.191    0.317  ...   102.0     103.0     100.0   1.01
Is_begin  1.071  0.841   0.021    2.717  ...    65.0      56.0      59.0   1.04
Ia_begin  2.873  2.105   0.098    6.944  ...    71.0      54.0      38.0   1.02
E_begin   1.524  1.336   0.078    4.005  ...    69.0      33.0      43.0   1.07

[8 rows x 11 columns]