0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1173.78    55.11
p_loo       48.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      451   91.7%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.184  0.032   0.150    0.254  ...   119.0      32.0      15.0   1.05
pu        0.714  0.010   0.701    0.731  ...   152.0     146.0     100.0   1.00
mu        0.148  0.025   0.112    0.195  ...    67.0      62.0     100.0   1.01
mus       0.216  0.039   0.146    0.285  ...    52.0      68.0      77.0   1.02
gamma     0.294  0.047   0.220    0.370  ...    49.0      61.0      59.0   1.03
Is_begin  0.308  0.346   0.010    1.084  ...    22.0      19.0      81.0   1.09
Ia_begin  0.498  0.596   0.008    1.734  ...    60.0      92.0      66.0   1.04
E_begin   0.227  0.415   0.002    0.804  ...    90.0      49.0      57.0   1.04

[8 rows x 11 columns]