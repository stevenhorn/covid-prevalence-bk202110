0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -3456.71    42.60
p_loo       29.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      424   86.0%
 (0.5, 0.7]   (ok)         57   11.6%
   (0.7, 1]   (bad)        12    2.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.270  0.054   0.172    0.347  ...    57.0      59.0      42.0   1.03
pu        0.875  0.027   0.822    0.900  ...    27.0      14.0      60.0   1.11
mu        0.128  0.014   0.110    0.159  ...    54.0      57.0      93.0   1.06
mus       0.151  0.028   0.103    0.205  ...    94.0      92.0      60.0   1.03
gamma     0.196  0.032   0.133    0.247  ...   106.0      97.0      57.0   1.03
Is_begin  1.296  1.230   0.028    3.789  ...    88.0      55.0      57.0   1.00
Ia_begin  2.629  2.330   0.051    7.690  ...    61.0      36.0      60.0   1.05
E_begin   1.927  2.298   0.028    6.533  ...    52.0      39.0      57.0   1.02

[8 rows x 11 columns]