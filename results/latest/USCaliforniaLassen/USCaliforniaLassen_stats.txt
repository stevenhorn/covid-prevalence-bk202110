0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1885.25    50.87
p_loo       40.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      458   93.1%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.012   0.151    0.191  ...   152.0     152.0     100.0   1.01
pu        0.705  0.005   0.700    0.714  ...    75.0      98.0      59.0   1.00
mu        0.119  0.015   0.092    0.144  ...   107.0      98.0      93.0   1.00
mus       0.151  0.033   0.102    0.222  ...   129.0     134.0      60.0   1.01
gamma     0.218  0.035   0.164    0.269  ...    87.0      81.0      59.0   1.01
Is_begin  0.085  0.137   0.000    0.289  ...    84.0      63.0      36.0   1.01
Ia_begin  0.223  0.433   0.000    1.282  ...    45.0      47.0      37.0   1.05
E_begin   0.081  0.143   0.000    0.306  ...    47.0      37.0      31.0   1.05

[8 rows x 11 columns]