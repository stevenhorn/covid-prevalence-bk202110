0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1088.12    60.31
p_loo       66.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    4    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.176  0.024   0.150    0.227  ...    54.0      41.0      60.0   1.01
pu        0.710  0.008   0.701    0.726  ...    72.0      84.0      85.0   1.00
mu        0.196  0.024   0.161    0.251  ...    27.0      27.0      58.0   1.03
mus       0.291  0.042   0.235    0.384  ...    19.0      19.0      36.0   1.08
gamma     0.520  0.082   0.405    0.677  ...    64.0      63.0      31.0   1.07
Is_begin  0.701  0.624   0.033    1.819  ...    40.0      22.0      96.0   1.06
Ia_begin  1.213  1.565   0.014    3.334  ...    61.0      42.0      99.0   1.02
E_begin   0.522  0.621   0.014    1.602  ...    47.0      19.0      25.0   1.08

[8 rows x 11 columns]