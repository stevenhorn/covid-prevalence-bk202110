0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1610.48    34.63
p_loo       30.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      458   93.1%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.043   0.191    0.337  ...    37.0      43.0      60.0   1.03
pu        0.755  0.028   0.702    0.795  ...    47.0      49.0      40.0   1.02
mu        0.113  0.023   0.075    0.166  ...    13.0      14.0      23.0   1.11
mus       0.183  0.042   0.102    0.255  ...   109.0     111.0      91.0   0.99
gamma     0.191  0.035   0.136    0.257  ...    44.0      42.0      96.0   1.05
Is_begin  0.730  0.811   0.005    2.175  ...    54.0      40.0      54.0   1.06
Ia_begin  1.582  1.897   0.007    4.719  ...    79.0      56.0      59.0   1.06
E_begin   0.855  1.168   0.003    3.413  ...    74.0      57.0      40.0   1.00

[8 rows x 11 columns]