0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1145.20    43.08
p_loo       36.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.7%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)        11    2.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.053   0.153    0.338  ...   106.0     107.0      59.0   1.02
pu        0.745  0.021   0.704    0.779  ...   100.0      96.0      60.0   1.01
mu        0.113  0.020   0.087    0.153  ...    78.0      76.0      73.0   0.99
mus       0.171  0.030   0.130    0.226  ...   152.0     152.0      45.0   0.99
gamma     0.188  0.036   0.123    0.250  ...    79.0      75.0      83.0   1.03
Is_begin  0.521  0.529   0.019    1.539  ...    28.0      85.0      80.0   1.03
Ia_begin  0.894  0.918   0.023    2.555  ...   114.0     113.0      60.0   0.98
E_begin   0.387  0.403   0.005    1.290  ...    73.0      65.0      66.0   1.01

[8 rows x 11 columns]