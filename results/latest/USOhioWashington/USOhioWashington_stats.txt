0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1457.64    39.03
p_loo       38.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      450   91.5%
 (0.5, 0.7]   (ok)         38    7.7%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.268  0.055   0.168    0.346  ...    46.0      28.0      19.0   1.15
pu        0.802  0.023   0.758    0.839  ...     6.0       6.0      14.0   1.30
mu        0.133  0.020   0.100    0.164  ...     6.0       6.0      58.0   1.33
mus       0.181  0.039   0.112    0.250  ...   152.0     152.0      86.0   0.99
gamma     0.230  0.038   0.166    0.302  ...   122.0     103.0      59.0   1.02
Is_begin  1.319  0.954   0.043    3.465  ...   139.0      93.0      93.0   1.01
Ia_begin  3.118  2.690   0.014    8.074  ...    33.0      16.0      17.0   1.11
E_begin   2.224  2.065   0.016    6.132  ...    58.0      65.0      60.0   1.04

[8 rows x 11 columns]