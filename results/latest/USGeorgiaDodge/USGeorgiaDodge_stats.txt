0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1361.29    81.47
p_loo       47.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.050   0.167    0.347  ...    28.0      30.0      40.0   1.05
pu        0.735  0.025   0.701    0.774  ...    32.0      30.0      40.0   1.07
mu        0.125  0.018   0.090    0.157  ...    10.0      10.0      40.0   1.19
mus       0.357  0.049   0.246    0.425  ...   152.0     152.0      79.0   1.00
gamma     0.565  0.096   0.418    0.817  ...   105.0      88.0      80.0   1.03
Is_begin  0.959  0.953   0.025    3.006  ...   114.0     103.0      55.0   1.03
Ia_begin  1.311  1.549   0.004    4.386  ...    14.0       9.0      15.0   1.19
E_begin   0.629  0.920   0.007    2.151  ...    69.0      41.0      43.0   1.05

[8 rows x 11 columns]