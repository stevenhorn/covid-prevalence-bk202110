0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1274.17    32.90
p_loo       33.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         35    7.1%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.055   0.160    0.334  ...    31.0      32.0      22.0   1.08
pu        0.808  0.026   0.773    0.852  ...    39.0      42.0      60.0   1.02
mu        0.141  0.023   0.106    0.183  ...    11.0       9.0      20.0   1.18
mus       0.206  0.029   0.165    0.257  ...    36.0      45.0      57.0   1.03
gamma     0.342  0.047   0.250    0.419  ...    10.0      10.0      46.0   1.17
Is_begin  0.768  0.716   0.001    2.183  ...    57.0      24.0      16.0   1.08
Ia_begin  1.459  1.301   0.045    3.834  ...    56.0      54.0     100.0   1.03
E_begin   0.752  0.788   0.000    2.144  ...    37.0      24.0      15.0   1.03

[8 rows x 11 columns]