0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1474.09    62.92
p_loo       39.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      449   91.3%
 (0.5, 0.7]   (ok)         33    6.7%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.061   0.159    0.348  ...   152.0     122.0      40.0   1.08
pu        0.795  0.022   0.755    0.835  ...    58.0      60.0      40.0   1.03
mu        0.135  0.020   0.097    0.171  ...    50.0      50.0      88.0   1.00
mus       0.192  0.036   0.124    0.251  ...   103.0      99.0     100.0   0.99
gamma     0.243  0.043   0.159    0.314  ...    70.0      70.0     100.0   1.00
Is_begin  0.826  0.789   0.003    2.552  ...   109.0      98.0      81.0   0.99
Ia_begin  2.154  1.910   0.005    5.222  ...   147.0      86.0      59.0   1.01
E_begin   0.979  1.142   0.024    3.927  ...   117.0     108.0      93.0   1.00

[8 rows x 11 columns]