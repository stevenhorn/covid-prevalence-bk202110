2 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -820.30    43.73
p_loo       38.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      451   91.7%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)        10    2.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.270  0.063   0.168    0.350  ...    14.0      10.0      15.0   1.19
pu        0.778  0.023   0.739    0.816  ...    12.0      12.0      40.0   1.18
mu        0.120  0.021   0.089    0.161  ...    10.0       8.0      47.0   1.23
mus       0.190  0.028   0.152    0.245  ...    64.0      59.0      40.0   0.99
gamma     0.222  0.038   0.135    0.276  ...    64.0      55.0      21.0   1.05
Is_begin  0.361  0.478   0.019    1.483  ...   109.0      71.0      88.0   1.00
Ia_begin  0.676  0.952   0.001    2.144  ...    73.0      38.0      40.0   1.05
E_begin   0.245  0.253   0.002    0.652  ...   130.0     101.0      33.0   1.01

[8 rows x 11 columns]