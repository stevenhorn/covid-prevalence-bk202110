0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1309.37    50.65
p_loo       32.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      466   94.7%
 (0.5, 0.7]   (ok)         18    3.7%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.016   0.151    0.200  ...    80.0      62.0      59.0   1.01
pu        0.707  0.005   0.701    0.716  ...    93.0      88.0      60.0   1.02
mu        0.102  0.017   0.081    0.143  ...    24.0      24.0      32.0   1.07
mus       0.158  0.029   0.089    0.201  ...    36.0      39.0      88.0   1.02
gamma     0.179  0.037   0.118    0.241  ...    63.0      77.0      38.0   1.02
Is_begin  0.254  0.270   0.002    0.702  ...    44.0      31.0      66.0   1.09
Ia_begin  0.356  0.384   0.003    1.097  ...   110.0     149.0      80.0   1.00
E_begin   0.182  0.267   0.001    0.872  ...    71.0      65.0      60.0   1.00

[8 rows x 11 columns]