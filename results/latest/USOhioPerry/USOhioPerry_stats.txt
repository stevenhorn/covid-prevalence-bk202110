0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1160.96    37.37
p_loo       33.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      458   93.1%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.066   0.160    0.347  ...   152.0     152.0      36.0   1.01
pu        0.827  0.020   0.795    0.862  ...    23.0      25.0      58.0   1.06
mu        0.127  0.023   0.093    0.170  ...    50.0      49.0      56.0   1.03
mus       0.168  0.028   0.123    0.223  ...   152.0     152.0      60.0   1.06
gamma     0.206  0.036   0.136    0.277  ...   125.0     152.0      60.0   0.99
Is_begin  0.711  0.576   0.017    1.984  ...    83.0      85.0      59.0   1.02
Ia_begin  1.595  1.519   0.001    4.228  ...   131.0     117.0      43.0   1.01
E_begin   0.683  0.820   0.027    1.895  ...   102.0      94.0      83.0   1.03

[8 rows x 11 columns]