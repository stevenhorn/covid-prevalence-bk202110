7 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2107.35    65.59
p_loo       60.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      467   94.9%
 (0.5, 0.7]   (ok)         15    3.0%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.180  0.025   0.150    0.226  ...    27.0      33.0      24.0   1.08
pu        0.708  0.007   0.700    0.720  ...    38.0      21.0      58.0   1.09
mu        0.128  0.019   0.096    0.165  ...    29.0      29.0      54.0   1.07
mus       0.242  0.036   0.164    0.292  ...    37.0      42.0      22.0   1.04
gamma     0.307  0.056   0.198    0.391  ...    35.0      38.0      40.0   1.03
Is_begin  0.724  0.710   0.031    2.043  ...    39.0      36.0      88.0   1.11
Ia_begin  1.418  1.116   0.013    3.429  ...    39.0      37.0      67.0   1.20
E_begin   0.689  0.638   0.004    1.765  ...    69.0      51.0      60.0   1.00

[8 rows x 11 columns]