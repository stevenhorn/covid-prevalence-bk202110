0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2377.72    26.89
p_loo       37.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      433   88.0%
 (0.5, 0.7]   (ok)         45    9.1%
   (0.7, 1]   (bad)        13    2.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.233  0.049   0.151    0.324  ...    66.0      61.0      59.0   1.03
pu        0.738  0.024   0.703    0.785  ...    13.0      13.0      48.0   1.14
mu        0.104  0.021   0.071    0.138  ...     6.0       6.0      40.0   1.35
mus       0.188  0.031   0.140    0.249  ...    21.0      22.0      58.0   1.07
gamma     0.290  0.042   0.205    0.362  ...    71.0      71.0      80.0   1.01
Is_begin  2.055  1.163   0.082    4.078  ...    68.0      58.0      42.0   1.01
Ia_begin  5.143  2.692   0.478    9.522  ...    49.0      46.0      60.0   1.03
E_begin   6.881  5.495   0.190   18.288  ...    49.0      39.0      24.0   1.02

[8 rows x 11 columns]