0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1753.76    79.89
p_loo       49.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      465   94.5%
 (0.5, 0.7]   (ok)         21    4.3%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.199  0.034   0.151    0.257  ...    70.0      53.0      18.0   1.06
pu        0.715  0.013   0.700    0.743  ...    99.0      88.0      60.0   0.99
mu        0.130  0.018   0.101    0.166  ...    18.0      18.0      58.0   1.08
mus       0.314  0.041   0.231    0.387  ...    61.0      81.0      56.0   1.02
gamma     0.494  0.083   0.340    0.634  ...    70.0      73.0      47.0   1.02
Is_begin  0.732  0.709   0.001    2.072  ...    23.0      12.0      42.0   1.14
Ia_begin  1.265  1.221   0.010    3.705  ...    28.0      32.0      91.0   1.05
E_begin   0.720  0.770   0.005    1.914  ...    50.0      27.0      59.0   1.07

[8 rows x 11 columns]