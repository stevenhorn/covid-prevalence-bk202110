0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1289.18    59.35
p_loo       55.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.048   0.159    0.316  ...    76.0      95.0      60.0   1.06
pu        0.744  0.032   0.700    0.802  ...     9.0       9.0      59.0   1.21
mu        0.169  0.030   0.121    0.222  ...    11.0      11.0      20.0   1.15
mus       0.251  0.048   0.162    0.351  ...    32.0      28.0      39.0   1.09
gamma     0.324  0.062   0.185    0.432  ...    85.0      79.0      61.0   1.02
Is_begin  0.728  0.840   0.001    2.352  ...    88.0      80.0      33.0   0.99
Ia_begin  1.191  1.299   0.001    3.681  ...   101.0      88.0      58.0   0.98
E_begin   0.513  0.699   0.000    1.701  ...    79.0      79.0      17.0   1.01

[8 rows x 11 columns]