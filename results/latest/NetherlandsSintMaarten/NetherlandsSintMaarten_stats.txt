0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -1410.81    27.56
p_loo       37.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.7%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)        11    2.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.051   0.180    0.347  ...   142.0     129.0      49.0   1.02
pu        0.811  0.037   0.751    0.882  ...    35.0      38.0      40.0   1.03
mu        0.167  0.032   0.108    0.220  ...    32.0      34.0      40.0   1.02
mus       0.205  0.032   0.156    0.269  ...    49.0      52.0      40.0   1.02
gamma     0.292  0.051   0.202    0.381  ...   143.0     152.0      60.0   1.04
Is_begin  1.515  0.866   0.101    2.918  ...   152.0     152.0      91.0   0.98
Ia_begin  3.858  2.893   0.229    9.644  ...   106.0      70.0      58.0   1.00
E_begin   2.511  2.234   0.061    5.656  ...    85.0      75.0      91.0   0.98

[8 rows x 11 columns]