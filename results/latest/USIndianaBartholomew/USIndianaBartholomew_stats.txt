0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1537.53    37.73
p_loo       33.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.9%
 (0.5, 0.7]   (ok)         36    7.3%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.045   0.165    0.319  ...    32.0      70.0      59.0   1.03
pu        0.756  0.029   0.704    0.805  ...    19.0      21.0      58.0   1.05
mu        0.143  0.021   0.110    0.180  ...    32.0      35.0      55.0   1.02
mus       0.186  0.031   0.134    0.230  ...    73.0      73.0      93.0   1.04
gamma     0.270  0.043   0.191    0.346  ...    59.0      55.0      96.0   1.03
Is_begin  1.401  1.049   0.093    3.201  ...   142.0     141.0      59.0   1.04
Ia_begin  2.932  2.254   0.097    7.053  ...    40.0      30.0      59.0   1.06
E_begin   2.573  2.699   0.003    7.642  ...    62.0      21.0      31.0   1.06

[8 rows x 11 columns]