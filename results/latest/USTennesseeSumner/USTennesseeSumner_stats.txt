0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2365.79    50.08
p_loo       48.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    4    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.212  0.045   0.150    0.297  ...    77.0      76.0      60.0   1.00
pu        0.721  0.016   0.703    0.753  ...   102.0      95.0      60.0   1.01
mu        0.137  0.024   0.098    0.177  ...    21.0      24.0      40.0   1.11
mus       0.240  0.035   0.188    0.309  ...    70.0      65.0      37.0   1.01
gamma     0.338  0.053   0.252    0.437  ...   111.0     114.0      72.0   1.00
Is_begin  3.187  2.312   0.267    7.740  ...    96.0      87.0      88.0   1.01
Ia_begin  6.033  4.620   0.040   15.418  ...    36.0      28.0      34.0   1.05
E_begin   6.440  5.846   0.049   19.375  ...    76.0      41.0      62.0   1.04

[8 rows x 11 columns]