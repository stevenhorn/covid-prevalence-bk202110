0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1319.87    86.33
p_loo       49.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   93.3%
 (0.5, 0.7]   (ok)         26    5.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.011   0.151    0.181  ...   142.0      83.0      22.0   1.02
pu        0.705  0.005   0.700    0.714  ...   147.0     104.0      34.0   1.01
mu        0.108  0.016   0.078    0.137  ...    41.0      42.0      54.0   1.02
mus       0.255  0.035   0.197    0.321  ...   152.0     152.0      58.0   1.01
gamma     0.539  0.080   0.411    0.717  ...    97.0     124.0      81.0   1.11
Is_begin  0.552  0.492   0.003    1.615  ...    48.0      22.0      39.0   1.11
Ia_begin  0.899  0.967   0.003    2.808  ...    74.0      70.0      57.0   1.00
E_begin   0.377  0.367   0.003    1.125  ...    63.0      51.0      59.0   1.04

[8 rows x 11 columns]