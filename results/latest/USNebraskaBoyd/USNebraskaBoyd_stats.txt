0 Divergences 
Failed validation 
2021-07-21 date of last case count
Computed from 80 by 494 log-likelihood matrix

         Estimate       SE
elpd_loo  -519.88    45.39
p_loo       45.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   92.9%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.054   0.174    0.337  ...    31.0      50.0      77.0   1.03
pu        0.846  0.016   0.816    0.871  ...    35.0      37.0      57.0   1.03
mu        0.114  0.021   0.077    0.150  ...    95.0      91.0      60.0   1.02
mus       0.188  0.042   0.128    0.279  ...    53.0      70.0      60.0   1.01
gamma     0.223  0.043   0.166    0.333  ...   152.0     152.0      84.0   1.09
Is_begin  0.312  0.339   0.001    1.085  ...    60.0      91.0      53.0   1.03
Ia_begin  0.416  0.530   0.001    1.431  ...    72.0      36.0      60.0   1.04
E_begin   0.179  0.193   0.005    0.559  ...    64.0      37.0      61.0   1.07

[8 rows x 11 columns]