0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1173.57    50.95
p_loo       41.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   93.3%
 (0.5, 0.7]   (ok)         26    5.3%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.052   0.162    0.337  ...    50.0      56.0      60.0   1.02
pu        0.797  0.021   0.769    0.835  ...    23.0      33.0      25.0   1.08
mu        0.130  0.024   0.093    0.175  ...    18.0      16.0      59.0   1.10
mus       0.169  0.032   0.117    0.229  ...    74.0     100.0     100.0   1.06
gamma     0.183  0.039   0.120    0.257  ...   101.0     105.0      60.0   0.99
Is_begin  0.573  0.556   0.005    1.744  ...    71.0      55.0      60.0   1.02
Ia_begin  1.007  1.336   0.031    3.550  ...    26.0      55.0      95.0   1.06
E_begin   0.460  0.513   0.002    1.529  ...    62.0      89.0      86.0   1.02

[8 rows x 11 columns]