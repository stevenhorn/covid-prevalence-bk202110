0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1489.11    71.30
p_loo       49.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.9%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.048   0.164    0.323  ...    60.0      88.0      53.0   1.02
pu        0.749  0.027   0.705    0.801  ...    76.0      68.0      30.0   1.02
mu        0.149  0.022   0.109    0.187  ...    39.0      28.0      18.0   1.08
mus       0.302  0.052   0.213    0.403  ...    73.0     114.0      55.0   1.00
gamma     0.473  0.081   0.357    0.621  ...    71.0      67.0      60.0   0.99
Is_begin  0.766  0.760   0.025    2.180  ...    87.0      60.0      60.0   1.05
Ia_begin  1.254  1.200   0.024    3.323  ...   148.0     152.0      40.0   0.99
E_begin   0.576  0.627   0.004    1.811  ...    61.0      23.0      39.0   1.07

[8 rows x 11 columns]