0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1087.26    30.07
p_loo       31.50        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.052   0.155    0.338  ...    52.0      51.0      53.0   1.02
pu        0.771  0.025   0.723    0.811  ...     8.0      10.0      27.0   1.20
mu        0.107  0.019   0.066    0.141  ...     6.0       6.0      40.0   1.31
mus       0.172  0.025   0.125    0.211  ...    59.0      60.0      43.0   1.01
gamma     0.212  0.041   0.149    0.274  ...    97.0     133.0      38.0   1.02
Is_begin  0.291  0.333   0.001    0.847  ...    75.0      46.0      43.0   1.00
Ia_begin  0.466  0.544   0.004    1.579  ...    60.0      77.0      60.0   1.02
E_begin   0.214  0.227   0.001    0.765  ...    79.0      68.0      88.0   1.03

[8 rows x 11 columns]