0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1256.91    42.98
p_loo       36.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   93.3%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.052   0.157    0.335  ...    42.0      45.0      39.0   1.04
pu        0.775  0.019   0.738    0.809  ...    31.0      32.0      43.0   1.09
mu        0.117  0.022   0.072    0.156  ...    41.0      37.0      30.0   1.03
mus       0.171  0.033   0.117    0.229  ...    72.0      82.0      59.0   1.03
gamma     0.201  0.032   0.148    0.253  ...   133.0     114.0      93.0   1.00
Is_begin  0.471  0.531   0.002    1.533  ...    58.0      36.0      38.0   1.05
Ia_begin  0.988  1.270   0.002    3.158  ...    61.0      37.0      58.0   1.07
E_begin   0.411  0.544   0.003    1.592  ...    69.0      38.0      40.0   1.01

[8 rows x 11 columns]