0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2049.31    33.16
p_loo       29.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      461   93.7%
 (0.5, 0.7]   (ok)         23    4.7%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.048   0.183    0.347  ...    39.0      47.0      59.0   1.02
pu        0.781  0.025   0.745    0.830  ...    21.0      23.0      79.0   1.01
mu        0.130  0.019   0.092    0.155  ...    14.0      15.0      22.0   1.06
mus       0.176  0.024   0.137    0.224  ...    94.0      98.0      54.0   1.06
gamma     0.242  0.045   0.179    0.315  ...    72.0      70.0      60.0   0.99
Is_begin  2.237  1.622   0.025    5.244  ...    44.0      30.0      40.0   1.05
Ia_begin  4.144  3.432   0.125    9.741  ...    79.0      60.0      33.0   1.01
E_begin   3.162  2.837   0.103    9.589  ...    61.0      46.0      41.0   1.05

[8 rows x 11 columns]