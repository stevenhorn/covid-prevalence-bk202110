0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1301.53    39.74
p_loo       41.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         35    7.1%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.057   0.151    0.331  ...    89.0      94.0      40.0   1.01
pu        0.747  0.024   0.705    0.782  ...    47.0      33.0      57.0   1.07
mu        0.124  0.024   0.086    0.172  ...    15.0      14.0      40.0   1.12
mus       0.221  0.034   0.162    0.287  ...    63.0      57.0      39.0   1.02
gamma     0.266  0.042   0.205    0.340  ...    74.0      76.0      37.0   1.01
Is_begin  0.825  0.862   0.018    2.437  ...   125.0     152.0      96.0   0.99
Ia_begin  1.371  1.238   0.055    4.072  ...    85.0     110.0     100.0   1.01
E_begin   0.637  0.785   0.009    1.643  ...   101.0      35.0      59.0   1.06

[8 rows x 11 columns]