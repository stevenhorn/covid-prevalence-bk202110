0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1609.22    31.97
p_loo       36.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      435   88.4%
 (0.5, 0.7]   (ok)         51   10.4%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.212  0.042   0.153    0.289  ...   152.0     145.0     100.0   1.00
pu        0.721  0.022   0.701    0.773  ...   102.0      61.0      65.0   0.99
mu        0.144  0.023   0.095    0.177  ...    24.0      23.0      22.0   1.08
mus       0.239  0.036   0.180    0.314  ...    70.0      80.0      43.0   1.03
gamma     0.333  0.052   0.252    0.421  ...   119.0     101.0      84.0   0.99
Is_begin  2.324  1.324   0.404    4.797  ...   125.0     115.0      60.0   1.00
Ia_begin  1.007  0.842   0.003    2.727  ...   152.0      91.0      60.0   1.02
E_begin   2.657  2.766   0.258    5.846  ...    70.0      71.0      83.0   1.00

[8 rows x 11 columns]