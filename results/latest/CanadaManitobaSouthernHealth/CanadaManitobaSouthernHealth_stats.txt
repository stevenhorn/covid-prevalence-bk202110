15 Divergences 
Passed validation 
2021-08-08 date of last case count
Computed from 80 by 513 log-likelihood matrix

         Estimate       SE
elpd_loo -1540.16    37.61
p_loo       38.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      468   91.2%
 (0.5, 0.7]   (ok)         38    7.4%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.292  0.047   0.200    0.349  ...    32.0      34.0      56.0   1.01
pu        0.886  0.014   0.864    0.900  ...    93.0      96.0      59.0   1.04
mu        0.170  0.032   0.118    0.235  ...     5.0       6.0      24.0   1.39
mus       0.217  0.032   0.159    0.281  ...    87.0     106.0      60.0   1.03
gamma     0.306  0.050   0.227    0.401  ...    70.0      90.0      88.0   1.01
Is_begin  3.088  3.490   0.090   10.695  ...    70.0      34.0      55.0   1.03
Ia_begin  6.833  7.958   0.015   21.942  ...    83.0      43.0      43.0   1.02
E_begin   2.933  3.459   0.018    8.448  ...    82.0      40.0      40.0   0.99

[8 rows x 11 columns]