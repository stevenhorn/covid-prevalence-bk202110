0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1250.39    53.81
p_loo       51.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      451   91.7%
 (0.5, 0.7]   (ok)         33    6.7%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.048   0.180    0.339  ...    67.0      64.0      60.0   1.03
pu        0.776  0.024   0.725    0.808  ...    65.0      75.0      99.0   0.99
mu        0.157  0.027   0.111    0.215  ...    10.0      10.0      24.0   1.20
mus       0.218  0.040   0.150    0.285  ...    42.0      45.0      60.0   1.03
gamma     0.269  0.042   0.197    0.352  ...    55.0      55.0      56.0   1.04
Is_begin  0.745  0.743   0.013    2.336  ...   122.0      62.0      17.0   0.99
Ia_begin  1.263  1.325   0.010    4.169  ...    60.0      59.0      81.0   1.02
E_begin   0.646  0.797   0.030    2.187  ...    55.0      94.0      56.0   1.00

[8 rows x 11 columns]