0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo  -690.62    60.33
p_loo       61.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      471   95.5%
 (0.5, 0.7]   (ok)         16    3.2%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    6    1.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.269  0.052   0.178    0.345  ...    96.0     105.0      53.0   1.07
pu        0.878  0.023   0.837    0.900  ...    68.0      64.0      55.0   1.02
mu        0.167  0.025   0.127    0.213  ...    47.0      61.0      96.0   1.04
mus       0.252  0.037   0.190    0.322  ...    76.0      76.0      52.0   1.00
gamma     0.349  0.049   0.286    0.459  ...    98.0      97.0      49.0   1.01
Is_begin  1.571  1.056   0.006    3.481  ...    81.0      74.0      50.0   1.01
Ia_begin  4.174  3.861   0.006   11.857  ...   103.0      82.0      39.0   0.99
E_begin   2.317  3.677   0.001    7.355  ...    65.0      28.0      43.0   1.07

[8 rows x 11 columns]