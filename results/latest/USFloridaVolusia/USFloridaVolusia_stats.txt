0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2605.12    40.40
p_loo       33.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.223  0.050   0.151    0.303  ...    75.0      82.0      56.0   0.99
pu        0.723  0.016   0.700    0.755  ...    61.0      48.0      60.0   1.01
mu        0.115  0.019   0.082    0.137  ...    33.0      28.0      38.0   1.05
mus       0.165  0.031   0.100    0.219  ...    93.0      99.0      42.0   1.01
gamma     0.252  0.052   0.164    0.339  ...   152.0     152.0      19.0   1.02
Is_begin  1.327  1.010   0.003    3.413  ...    57.0      42.0      31.0   1.00
Ia_begin  2.504  2.180   0.044    6.832  ...    14.0       9.0      59.0   1.20
E_begin   1.859  1.898   0.095    5.976  ...   113.0     130.0      69.0   1.01

[8 rows x 11 columns]