0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2294.36    40.45
p_loo       33.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      451   91.7%
 (0.5, 0.7]   (ok)         36    7.3%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.054   0.155    0.331  ...    67.0      63.0      49.0   1.00
pu        0.770  0.030   0.703    0.811  ...    40.0      47.0      38.0   1.01
mu        0.132  0.020   0.097    0.176  ...    21.0      23.0      24.0   1.07
mus       0.199  0.031   0.135    0.248  ...    69.0      64.0      60.0   1.01
gamma     0.256  0.040   0.198    0.331  ...   136.0     119.0      83.0   0.98
Is_begin  1.556  1.605   0.026    4.828  ...    74.0     152.0      55.0   1.05
Ia_begin  3.014  2.714   0.014    7.930  ...   129.0     112.0      40.0   1.02
E_begin   2.084  2.097   0.003    6.855  ...    85.0      75.0      38.0   1.03

[8 rows x 11 columns]