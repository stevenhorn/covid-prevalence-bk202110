0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1378.15    60.46
p_loo       44.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   93.3%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.045   0.185    0.339  ...    75.0      78.0      32.0   1.05
pu        0.784  0.025   0.743    0.826  ...    47.0      47.0      88.0   1.06
mu        0.137  0.026   0.099    0.185  ...    12.0      12.0      20.0   1.15
mus       0.212  0.041   0.137    0.273  ...    42.0      34.0      59.0   1.06
gamma     0.261  0.041   0.179    0.333  ...    57.0      54.0      65.0   1.05
Is_begin  0.799  0.788   0.009    2.049  ...    88.0      33.0      37.0   1.06
Ia_begin  1.504  1.335   0.080    4.374  ...   109.0      88.0      93.0   1.00
E_begin   0.567  0.523   0.021    1.589  ...    48.0      49.0      88.0   1.04

[8 rows x 11 columns]