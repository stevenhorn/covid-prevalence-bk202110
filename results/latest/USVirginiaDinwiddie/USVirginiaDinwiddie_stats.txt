0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1237.83    31.06
p_loo       30.95        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      467   94.9%
 (0.5, 0.7]   (ok)         21    4.3%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.060   0.161    0.347  ...    53.0      50.0      40.0   1.06
pu        0.792  0.047   0.719    0.866  ...     5.0       5.0      29.0   1.34
mu        0.119  0.019   0.083    0.152  ...    41.0      41.0      57.0   1.02
mus       0.163  0.036   0.111    0.218  ...    41.0     107.0      61.0   1.08
gamma     0.172  0.033   0.123    0.227  ...   104.0      98.0      97.0   1.00
Is_begin  0.477  0.549   0.023    1.711  ...    74.0      71.0      58.0   1.01
Ia_begin  0.937  1.098   0.010    2.716  ...    82.0      70.0      84.0   1.00
E_begin   0.389  0.424   0.001    1.104  ...    72.0      52.0      57.0   1.00

[8 rows x 11 columns]