0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1156.10    35.24
p_loo       37.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.7%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)        10    2.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.051   0.161    0.342  ...    55.0      55.0      24.0   1.07
pu        0.747  0.031   0.700    0.804  ...    45.0      39.0      22.0   1.04
mu        0.140  0.022   0.101    0.181  ...    15.0      14.0      74.0   1.14
mus       0.200  0.033   0.144    0.261  ...    97.0      93.0      56.0   1.03
gamma     0.260  0.040   0.189    0.329  ...    52.0      68.0      72.0   1.02
Is_begin  1.196  0.824   0.127    2.872  ...    89.0     102.0      59.0   0.99
Ia_begin  2.105  1.889   0.007    6.835  ...    76.0      36.0      30.0   1.04
E_begin   1.018  1.048   0.019    3.567  ...    90.0      82.0      62.0   1.01

[8 rows x 11 columns]