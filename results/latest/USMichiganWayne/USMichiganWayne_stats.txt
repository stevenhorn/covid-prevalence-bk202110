0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -3169.79    31.09
p_loo       46.47        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         34    6.9%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd   hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.250    0.053    0.170    0.349  ...    25.0      31.0      22.0   1.02
pu          0.786    0.035    0.707    0.828  ...     3.0       4.0      19.0   1.76
mu          0.137    0.016    0.105    0.160  ...     6.0       6.0      30.0   1.32
mus         0.193    0.031    0.154    0.254  ...    35.0      44.0      22.0   1.03
gamma       0.442    0.066    0.320    0.557  ...    31.0      29.0      45.0   1.05
Is_begin   50.339   21.336   16.969   89.768  ...    99.0      91.0      93.0   1.00
Ia_begin  145.198   37.085   91.668  220.672  ...    19.0      16.0      74.0   1.11
E_begin   510.998  149.251  299.606  850.666  ...    24.0      21.0      24.0   1.09

[8 rows x 11 columns]