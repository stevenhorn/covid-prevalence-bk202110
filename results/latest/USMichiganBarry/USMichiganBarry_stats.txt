0 Divergences 
Passed validation 
2021-07-21 date of last case count
Computed from 80 by 494 log-likelihood matrix

         Estimate       SE
elpd_loo -1557.66    36.18
p_loo       35.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      451   91.3%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)        10    2.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.051   0.160    0.325  ...    25.0      25.0      35.0   1.04
pu        0.770  0.022   0.726    0.810  ...    16.0      17.0      21.0   1.08
mu        0.128  0.021   0.098    0.173  ...     4.0       4.0      40.0   1.88
mus       0.186  0.029   0.135    0.234  ...     8.0       8.0      31.0   1.21
gamma     0.259  0.036   0.195    0.308  ...    58.0      57.0      72.0   1.03
Is_begin  0.649  0.589   0.001    1.947  ...    57.0      35.0      16.0   1.01
Ia_begin  0.803  1.227   0.000    3.736  ...     7.0       4.0      22.0   1.71
E_begin   0.499  0.499   0.000    1.385  ...     7.0       8.0      22.0   1.26

[8 rows x 11 columns]