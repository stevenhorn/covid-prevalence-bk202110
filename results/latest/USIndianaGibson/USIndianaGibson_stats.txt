0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1365.32    29.81
p_loo       35.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.193  0.043   0.150    0.280  ...   149.0     122.0      40.0   1.02
pu        0.716  0.015   0.700    0.743  ...    70.0     116.0      56.0   1.11
mu        0.115  0.025   0.074    0.156  ...     7.0       7.0      26.0   1.26
mus       0.193  0.028   0.142    0.242  ...   152.0     152.0      37.0   1.03
gamma     0.259  0.047   0.180    0.344  ...    91.0      99.0      34.0   1.02
Is_begin  0.628  0.615   0.005    1.944  ...   101.0     125.0      86.0   0.98
Ia_begin  0.887  1.204   0.012    3.181  ...    76.0      70.0      83.0   0.99
E_begin   0.337  0.402   0.001    0.945  ...    84.0      75.0      93.0   1.06

[8 rows x 11 columns]