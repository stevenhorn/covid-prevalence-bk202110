0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -975.21    40.32
p_loo       38.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.7%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.046   0.171    0.333  ...    61.0      62.0      59.0   0.98
pu        0.837  0.012   0.818    0.862  ...    56.0      57.0      56.0   1.02
mu        0.116  0.019   0.077    0.155  ...    34.0      34.0      59.0   1.06
mus       0.166  0.031   0.122    0.234  ...   114.0     112.0      60.0   0.98
gamma     0.179  0.036   0.126    0.263  ...    83.0      85.0      60.0   1.01
Is_begin  0.542  0.508   0.002    1.333  ...    70.0      51.0      43.0   0.99
Ia_begin  1.396  1.420   0.026    4.127  ...   127.0     138.0      86.0   1.01
E_begin   0.620  0.756   0.002    2.082  ...   125.0     108.0      43.0   1.00

[8 rows x 11 columns]