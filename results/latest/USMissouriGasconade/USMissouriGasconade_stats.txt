0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1477.41    84.81
p_loo       41.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      478   97.2%
 (0.5, 0.7]   (ok)         10    2.0%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.179  0.025   0.151    0.212  ...   152.0     137.0      59.0   1.00
pu        0.709  0.008   0.700    0.724  ...   147.0     152.0      69.0   0.99
mu        0.067  0.010   0.052    0.089  ...    54.0      55.0      40.0   1.06
mus       0.248  0.030   0.189    0.296  ...   122.0     152.0      38.0   1.06
gamma     0.614  0.078   0.469    0.748  ...   129.0     149.0      59.0   1.02
Is_begin  0.585  0.576   0.001    1.813  ...    85.0     120.0      83.0   1.03
Ia_begin  0.782  1.056   0.004    2.565  ...   100.0     144.0      67.0   0.99
E_begin   0.315  0.372   0.001    1.048  ...    83.0      82.0      59.0   1.00

[8 rows x 11 columns]