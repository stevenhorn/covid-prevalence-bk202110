0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1377.98    52.85
p_loo       47.96        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      466   94.7%
 (0.5, 0.7]   (ok)         20    4.1%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.049   0.169    0.335  ...   152.0     152.0      57.0   1.03
pu        0.772  0.021   0.742    0.808  ...    37.0      37.0      93.0   1.07
mu        0.144  0.024   0.107    0.191  ...    20.0      20.0      31.0   1.05
mus       0.206  0.042   0.147    0.281  ...   124.0     133.0      45.0   1.12
gamma     0.315  0.050   0.225    0.405  ...   152.0     152.0      60.0   1.02
Is_begin  1.848  1.273   0.003    4.119  ...   110.0      83.0      40.0   1.00
Ia_begin  5.571  3.274   0.260   10.107  ...    72.0     144.0      59.0   0.99
E_begin   5.340  3.409   0.138   12.575  ...   116.0      97.0      85.0   1.02

[8 rows x 11 columns]