0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -181.55    46.41
p_loo       53.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      450   91.5%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)        10    2.0%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.057   0.156    0.343  ...    51.0      50.0      49.0   1.01
pu        0.828  0.061   0.737    0.898  ...     3.0       4.0      16.0   1.80
mu        0.168  0.032   0.104    0.219  ...    37.0      36.0      59.0   1.05
mus       0.275  0.047   0.201    0.351  ...    35.0      73.0      69.0   1.03
gamma     0.322  0.045   0.253    0.419  ...   100.0     119.0      76.0   1.06
Is_begin  0.530  0.543   0.004    1.700  ...    13.0      18.0      91.0   1.14
Ia_begin  1.008  1.679   0.013    3.900  ...    42.0      53.0      50.0   1.06
E_begin   0.395  0.563   0.000    1.496  ...    46.0       9.0      25.0   1.19

[8 rows x 11 columns]