0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1393.10    49.80
p_loo       62.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      463   94.1%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.050   0.170    0.342  ...    55.0      61.0      58.0   1.03
pu        0.802  0.022   0.751    0.837  ...    21.0      19.0      59.0   1.12
mu        0.130  0.024   0.095    0.176  ...    17.0      22.0      24.0   1.07
mus       0.178  0.032   0.118    0.240  ...    63.0      60.0      38.0   1.02
gamma     0.196  0.039   0.133    0.270  ...    47.0      44.0      55.0   1.07
Is_begin  0.787  0.780   0.013    2.297  ...    50.0      56.0      54.0   1.06
Ia_begin  1.625  1.347   0.080    4.044  ...    63.0      54.0      60.0   1.03
E_begin   0.697  0.675   0.021    1.938  ...    31.0      17.0      24.0   1.10

[8 rows x 11 columns]