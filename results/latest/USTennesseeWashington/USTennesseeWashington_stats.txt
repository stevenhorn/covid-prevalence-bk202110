0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1946.49    35.93
p_loo       32.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.051   0.150    0.329  ...    51.0      45.0      34.0   1.06
pu        0.749  0.022   0.712    0.788  ...    32.0      35.0      97.0   1.04
mu        0.143  0.024   0.105    0.180  ...    72.0      51.0      59.0   1.03
mus       0.184  0.028   0.133    0.234  ...    59.0      78.0      60.0   1.02
gamma     0.251  0.036   0.194    0.326  ...    84.0      86.0      20.0   1.02
Is_begin  0.851  0.695   0.028    2.147  ...    60.0      53.0      58.0   1.01
Ia_begin  0.669  0.467   0.019    1.622  ...    54.0      76.0      16.0   1.02
E_begin   0.518  0.552   0.001    1.683  ...    98.0      79.0      57.0   0.99

[8 rows x 11 columns]