0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1545.49    38.56
p_loo       43.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      464   94.3%
 (0.5, 0.7]   (ok)         19    3.9%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.050   0.166    0.330  ...    35.0      37.0      45.0   1.03
pu        0.751  0.024   0.707    0.799  ...    36.0      38.0      38.0   1.03
mu        0.136  0.027   0.083    0.186  ...     6.0       6.0      15.0   1.32
mus       0.178  0.037   0.113    0.243  ...    49.0      51.0      59.0   1.01
gamma     0.231  0.044   0.159    0.310  ...    82.0      86.0      59.0   1.01
Is_begin  0.753  0.580   0.017    1.981  ...    65.0      85.0      74.0   1.03
Ia_begin  1.278  1.217   0.009    3.466  ...    88.0      50.0      18.0   1.07
E_begin   0.552  0.572   0.006    1.718  ...    74.0      61.0      59.0   1.01

[8 rows x 11 columns]