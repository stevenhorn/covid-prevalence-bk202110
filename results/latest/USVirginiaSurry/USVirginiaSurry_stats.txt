0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -724.13    29.29
p_loo       25.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      458   93.1%
 (0.5, 0.7]   (ok)         26    5.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.276  0.052   0.187    0.348  ...    99.0     101.0      59.0   1.05
pu        0.884  0.013   0.861    0.900  ...    30.0      35.0      47.0   1.05
mu        0.132  0.021   0.095    0.162  ...    57.0      59.0      93.0   1.02
mus       0.169  0.031   0.108    0.227  ...    80.0      81.0      60.0   1.09
gamma     0.198  0.039   0.128    0.258  ...   152.0     152.0      73.0   1.00
Is_begin  0.795  0.690   0.013    2.183  ...    90.0      92.0      59.0   1.02
Ia_begin  1.642  1.617   0.008    4.663  ...    88.0      47.0      40.0   1.03
E_begin   0.717  0.860   0.001    2.111  ...    43.0      93.0      40.0   0.99

[8 rows x 11 columns]