0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2552.21    49.25
p_loo       56.34        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      454   92.3%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    3    0.6%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.225   0.048   0.153    0.312  ...    56.0      32.0      60.0   1.06
pu         0.717   0.018   0.700    0.761  ...    16.0      19.0      25.0   1.15
mu         0.129   0.024   0.093    0.176  ...     4.0       6.0      24.0   1.37
mus        0.234   0.038   0.179    0.296  ...    66.0      58.0      91.0   1.01
gamma      0.347   0.054   0.264    0.443  ...    89.0      86.0      60.0   1.03
Is_begin   4.417   3.191   0.141    8.990  ...    19.0      19.0      19.0   1.09
Ia_begin   9.288   6.159   0.272   18.637  ...    12.0      10.0      22.0   1.20
E_begin   13.544  10.401   0.403   31.440  ...    16.0      19.0      59.0   1.13

[8 rows x 11 columns]