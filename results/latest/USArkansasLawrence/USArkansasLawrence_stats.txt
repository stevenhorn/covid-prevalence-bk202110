0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1274.07    54.55
p_loo       47.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      464   94.3%
 (0.5, 0.7]   (ok)         18    3.7%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.201  0.033   0.152    0.263  ...   125.0     107.0      91.0   1.01
pu        0.715  0.010   0.702    0.733  ...   147.0     136.0      54.0   1.05
mu        0.136  0.025   0.089    0.180  ...    11.0      10.0      59.0   1.19
mus       0.207  0.031   0.144    0.250  ...   101.0      90.0      95.0   1.00
gamma     0.265  0.042   0.180    0.337  ...    77.0      81.0      47.0   1.04
Is_begin  0.702  0.666   0.001    1.843  ...    89.0      48.0      43.0   0.99
Ia_begin  1.452  1.720   0.010    4.128  ...    96.0      71.0      43.0   1.00
E_begin   0.589  0.695   0.000    1.578  ...    89.0      76.0      42.0   1.00

[8 rows x 11 columns]