0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1553.56    71.57
p_loo       49.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      464   94.3%
 (0.5, 0.7]   (ok)         22    4.5%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.052   0.159    0.333  ...    60.0      67.0      59.0   1.01
pu        0.764  0.022   0.722    0.799  ...    37.0      38.0      54.0   1.07
mu        0.166  0.032   0.111    0.226  ...     4.0       4.0      27.0   1.75
mus       0.322  0.048   0.244    0.407  ...    67.0      71.0      29.0   1.02
gamma     0.438  0.079   0.320    0.564  ...   103.0     118.0      74.0   1.01
Is_begin  1.015  0.891   0.024    2.521  ...   112.0      77.0      43.0   1.04
Ia_begin  1.648  1.623   0.003    4.904  ...    51.0      45.0      58.0   1.03
E_begin   0.720  0.984   0.001    2.881  ...    55.0      37.0      40.0   1.06

[8 rows x 11 columns]