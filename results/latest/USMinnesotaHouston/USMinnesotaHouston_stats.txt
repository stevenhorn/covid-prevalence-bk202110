0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1083.24    40.89
p_loo       42.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      461   93.7%
 (0.5, 0.7]   (ok)         21    4.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    4    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.049   0.171    0.335  ...    64.0      62.0      60.0   1.00
pu        0.764  0.023   0.716    0.800  ...    57.0      60.0      46.0   1.00
mu        0.137  0.020   0.102    0.175  ...    13.0      13.0      57.0   1.12
mus       0.196  0.036   0.134    0.265  ...    68.0      74.0      60.0   1.01
gamma     0.246  0.047   0.166    0.323  ...   143.0     151.0      73.0   0.99
Is_begin  0.579  0.678   0.004    1.705  ...    76.0     152.0      59.0   0.99
Ia_begin  0.981  0.961   0.042    2.845  ...   107.0      88.0     100.0   1.01
E_begin   0.475  0.533   0.019    1.593  ...    91.0      79.0      80.0   1.00

[8 rows x 11 columns]