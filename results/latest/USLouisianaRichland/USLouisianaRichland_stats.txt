0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1521.47    51.96
p_loo       46.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      463   94.1%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.236  0.053   0.152    0.320  ...    38.0      47.0      22.0   1.03
pu        0.729  0.018   0.702    0.760  ...    27.0      23.0      42.0   1.05
mu        0.147  0.020   0.107    0.177  ...    17.0      18.0      49.0   1.08
mus       0.248  0.031   0.196    0.294  ...    55.0      60.0      59.0   1.05
gamma     0.367  0.054   0.270    0.472  ...   152.0     152.0      74.0   0.99
Is_begin  0.745  0.668   0.025    1.930  ...    67.0      63.0      76.0   1.02
Ia_begin  1.273  1.152   0.023    3.593  ...    56.0      42.0      42.0   1.01
E_begin   0.517  0.545   0.023    1.681  ...    70.0      60.0      60.0   1.03

[8 rows x 11 columns]