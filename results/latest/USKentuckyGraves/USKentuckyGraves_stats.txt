0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1479.36    36.36
p_loo       36.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      447   90.9%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)        13    2.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.054   0.166    0.350  ...    44.0      48.0      45.0   1.05
pu        0.736  0.019   0.708    0.769  ...    65.0      65.0      42.0   1.04
mu        0.152  0.025   0.109    0.199  ...    13.0      14.0      40.0   1.11
mus       0.205  0.035   0.142    0.261  ...    44.0      39.0      19.0   1.10
gamma     0.297  0.053   0.223    0.405  ...   125.0     111.0      93.0   1.03
Is_begin  0.692  0.682   0.012    2.411  ...    98.0      83.0      55.0   1.01
Ia_begin  1.708  1.721   0.007    5.315  ...    54.0      43.0      48.0   1.06
E_begin   0.778  0.970   0.002    2.110  ...    20.0       9.0      40.0   1.20

[8 rows x 11 columns]