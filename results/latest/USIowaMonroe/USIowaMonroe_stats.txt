0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -978.09    38.11
p_loo       40.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      445   90.4%
 (0.5, 0.7]   (ok)         41    8.3%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.055   0.158    0.342  ...    65.0      59.0      57.0   1.01
pu        0.755  0.026   0.713    0.803  ...    34.0      36.0      30.0   1.05
mu        0.149  0.026   0.102    0.191  ...    32.0      32.0      59.0   1.04
mus       0.231  0.034   0.180    0.299  ...   152.0     152.0      93.0   1.00
gamma     0.279  0.049   0.194    0.372  ...   126.0     125.0      88.0   0.99
Is_begin  0.394  0.498   0.008    1.185  ...    98.0      66.0      88.0   1.03
Ia_begin  0.547  0.631   0.003    1.663  ...    66.0      68.0      54.0   1.02
E_begin   0.258  0.301   0.001    0.718  ...    50.0      36.0      59.0   1.07

[8 rows x 11 columns]