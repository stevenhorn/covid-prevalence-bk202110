0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1199.22    32.42
p_loo       34.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.9%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.274  0.053   0.169    0.344  ...     6.0       6.0      24.0   1.32
pu        0.795  0.025   0.765    0.842  ...     3.0       4.0      30.0   1.77
mu        0.184  0.043   0.124    0.265  ...     3.0       3.0      19.0   2.25
mus       0.183  0.025   0.150    0.239  ...     5.0       6.0      22.0   1.31
gamma     0.371  0.052   0.271    0.460  ...     9.0       9.0      59.0   1.20
Is_begin  0.595  0.539   0.035    1.714  ...    33.0      17.0      15.0   1.08
Ia_begin  1.216  1.037   0.003    2.814  ...     7.0       6.0      17.0   1.40
E_begin   0.488  0.411   0.027    1.294  ...    18.0       9.0      18.0   1.21

[8 rows x 11 columns]