0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -803.40    47.39
p_loo       46.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      449   91.3%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    7    1.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.054   0.169    0.347  ...    91.0      98.0      34.0   1.05
pu        0.750  0.027   0.704    0.788  ...     9.0       9.0      40.0   1.18
mu        0.128  0.026   0.079    0.167  ...    17.0      19.0      59.0   1.09
mus       0.221  0.051   0.145    0.311  ...    10.0      10.0      59.0   1.17
gamma     0.249  0.044   0.180    0.343  ...    77.0      78.0      43.0   1.11
Is_begin  0.490  0.601   0.005    1.323  ...    77.0      55.0      91.0   1.01
Ia_begin  0.788  1.023   0.001    2.690  ...    15.0      10.0      43.0   1.17
E_begin   0.374  0.506   0.001    1.381  ...    30.0      16.0      15.0   1.11

[8 rows x 11 columns]