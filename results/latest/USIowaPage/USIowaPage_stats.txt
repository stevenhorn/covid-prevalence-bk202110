0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1314.90    42.27
p_loo       48.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      454   92.3%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    5    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.232  0.045   0.157    0.319  ...    47.0      60.0      38.0   1.02
pu        0.725  0.019   0.701    0.761  ...    47.0      42.0      59.0   1.02
mu        0.123  0.020   0.092    0.166  ...    38.0      37.0      80.0   1.04
mus       0.245  0.037   0.188    0.316  ...    74.0      74.0     100.0   1.05
gamma     0.336  0.051   0.273    0.448  ...   103.0      88.0      93.0   1.01
Is_begin  0.732  0.667   0.004    1.795  ...   117.0     109.0      60.0   0.99
Ia_begin  1.230  1.065   0.006    3.430  ...   151.0     142.0      91.0   1.01
E_begin   0.585  0.582   0.005    1.764  ...   145.0     152.0      83.0   1.02

[8 rows x 11 columns]