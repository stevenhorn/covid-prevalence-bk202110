0 Divergences 
Passed validation 
2021-08-08 date of last case count
Computed from 80 by 513 log-likelihood matrix

         Estimate       SE
elpd_loo   194.79    74.03
p_loo       59.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      487   94.9%
 (0.5, 0.7]   (ok)         16    3.1%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    5    1.0%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.298  0.047   0.205    0.349  ...    80.0      78.0      45.0   1.01
pu         0.893  0.007   0.882    0.900  ...   131.0     109.0      24.0   1.01
mu         0.174  0.033   0.122    0.243  ...    62.0      68.0      84.0   1.02
mus        0.231  0.034   0.167    0.291  ...   152.0     152.0      62.0   1.05
gamma      0.249  0.045   0.159    0.315  ...   152.0     152.0      80.0   1.11
Is_begin  12.212  3.622   4.873   18.293  ...   144.0     125.0      64.0   1.03
Ia_begin   6.234  7.455   0.097   25.412  ...    89.0     152.0      69.0   0.99
E_begin    3.239  3.047   0.062    8.839  ...   126.0     104.0      87.0   1.01

[8 rows x 11 columns]