0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1844.35    42.33
p_loo       39.05        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      461   93.7%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.231  0.050   0.155    0.309  ...    89.0      89.0      60.0   1.00
pu        0.730  0.024   0.700    0.774  ...   152.0     149.0      56.0   1.00
mu        0.120  0.019   0.085    0.147  ...    53.0      49.0      59.0   1.08
mus       0.196  0.026   0.149    0.233  ...    86.0      82.0     100.0   0.99
gamma     0.293  0.050   0.227    0.408  ...   128.0     128.0     100.0   1.00
Is_begin  1.107  0.860   0.027    2.879  ...    69.0      57.0      54.0   1.04
Ia_begin  2.063  2.368   0.005    5.849  ...   124.0      80.0      32.0   1.00
E_begin   1.168  1.487   0.005    4.584  ...   104.0      94.0      60.0   1.01

[8 rows x 11 columns]