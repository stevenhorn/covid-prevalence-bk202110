0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1713.54    40.05
p_loo       41.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      461   93.7%
 (0.5, 0.7]   (ok)         19    3.9%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    5    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.174  0.021   0.150    0.215  ...    71.0      76.0      59.0   1.04
pu        0.709  0.009   0.700    0.727  ...   152.0     140.0      59.0   0.99
mu        0.110  0.015   0.081    0.135  ...    13.0      12.0      15.0   1.13
mus       0.172  0.023   0.131    0.215  ...    74.0      74.0      59.0   1.01
gamma     0.271  0.045   0.200    0.368  ...   117.0     111.0      68.0   1.00
Is_begin  0.658  0.555   0.006    1.780  ...    43.0      26.0      22.0   1.06
Ia_begin  1.261  1.192   0.002    3.161  ...    74.0      43.0      58.0   1.01
E_begin   0.671  0.689   0.005    1.831  ...    34.0      23.0      37.0   1.05

[8 rows x 11 columns]