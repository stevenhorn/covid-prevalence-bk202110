0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1614.41    44.00
p_loo       43.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      458   93.1%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.222  0.049   0.152    0.298  ...    45.0      37.0      68.0   1.06
pu        0.727  0.018   0.704    0.769  ...    62.0      35.0      80.0   1.05
mu        0.110  0.019   0.075    0.143  ...    38.0      33.0      40.0   1.04
mus       0.162  0.027   0.122    0.212  ...   124.0     118.0      96.0   1.02
gamma     0.216  0.043   0.150    0.276  ...    64.0      75.0      38.0   1.01
Is_begin  0.582  0.605   0.022    1.729  ...    37.0      25.0     100.0   1.08
Ia_begin  0.837  1.088   0.004    2.571  ...    84.0      52.0      57.0   0.98
E_begin   0.303  0.315   0.017    0.931  ...    51.0      44.0      48.0   1.00

[8 rows x 11 columns]