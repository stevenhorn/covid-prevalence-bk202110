0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -423.87    48.39
p_loo       39.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      466   94.7%
 (0.5, 0.7]   (ok)         16    3.3%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.299  0.033   0.238    0.346  ...    65.0      42.0      38.0   1.04
pu        0.894  0.004   0.886    0.900  ...    96.0      75.0      40.0   0.99
mu        0.138  0.027   0.094    0.189  ...    17.0      18.0      40.0   1.07
mus       0.181  0.032   0.121    0.236  ...    18.0      23.0      77.0   1.07
gamma     0.207  0.041   0.132    0.282  ...   118.0     102.0      95.0   1.04
Is_begin  0.633  0.787   0.014    1.824  ...   108.0      56.0      38.0   1.01
Ia_begin  1.391  1.401   0.004    4.260  ...    50.0      24.0      23.0   1.08
E_begin   0.633  0.708   0.022    1.940  ...    84.0      32.0      38.0   1.06

[8 rows x 11 columns]