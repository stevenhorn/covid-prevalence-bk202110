0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -916.95    36.22
p_loo       36.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.052   0.151    0.330  ...    30.0      29.0      22.0   1.05
pu        0.743  0.020   0.711    0.779  ...    35.0      39.0      38.0   1.03
mu        0.140  0.026   0.091    0.186  ...    69.0      52.0      96.0   1.01
mus       0.198  0.036   0.120    0.256  ...    59.0      59.0      16.0   1.01
gamma     0.249  0.039   0.174    0.315  ...    70.0      72.0     100.0   1.01
Is_begin  0.355  0.479   0.011    1.010  ...    97.0      98.0     100.0   1.04
Ia_begin  0.670  0.701   0.041    2.499  ...    91.0      68.0      91.0   1.00
E_begin   0.298  0.313   0.010    0.904  ...    92.0      70.0      75.0   1.00

[8 rows x 11 columns]