0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2132.38    34.33
p_loo       30.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      447   90.9%
 (0.5, 0.7]   (ok)         37    7.5%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    0    0.0%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.240  0.050   0.152    0.329  ...   124.0     131.0      56.0   1.11
pu         0.762  0.043   0.701    0.827  ...    22.0      18.0      48.0   1.10
mu         0.104  0.021   0.070    0.137  ...    37.0      36.0      49.0   1.02
mus        0.170  0.030   0.121    0.225  ...   111.0     104.0      86.0   0.99
gamma      0.218  0.037   0.152    0.286  ...   152.0     152.0      93.0   1.02
Is_begin   4.584  3.109   0.122    9.285  ...   122.0     103.0      39.0   1.06
Ia_begin  12.694  7.227   0.076   27.033  ...   104.0      83.0      33.0   0.99
E_begin    9.763  7.623   0.211   25.590  ...    57.0      37.0      81.0   1.04

[8 rows x 11 columns]