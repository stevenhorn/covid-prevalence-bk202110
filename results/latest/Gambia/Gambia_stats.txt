0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -1843.61    39.21
p_loo       39.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      465   94.3%
 (0.5, 0.7]   (ok)         20    4.1%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.294  0.038   0.231    0.347  ...   152.0     152.0      60.0   1.00
pu        0.889  0.011   0.870    0.900  ...   110.0     152.0      37.0   1.00
mu        0.179  0.021   0.137    0.214  ...     9.0      10.0      17.0   1.17
mus       0.190  0.031   0.145    0.255  ...    81.0     115.0      86.0   1.08
gamma     0.387  0.056   0.290    0.468  ...    79.0      91.0      20.0   1.05
Is_begin  0.999  0.886   0.000    2.669  ...   101.0      69.0      49.0   1.04
Ia_begin  2.497  2.361   0.034    7.189  ...   147.0     100.0      80.0   1.03
E_begin   0.990  1.133   0.058    3.278  ...   106.0      22.0      59.0   1.08

[8 rows x 11 columns]