0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1342.27    50.06
p_loo       42.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         26    5.3%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.051   0.166    0.347  ...    64.0      70.0      60.0   1.05
pu        0.841  0.015   0.814    0.871  ...    58.0      58.0     100.0   1.02
mu        0.140  0.019   0.110    0.177  ...    43.0      43.0      74.0   1.07
mus       0.221  0.029   0.174    0.274  ...   152.0     152.0      69.0   1.06
gamma     0.324  0.068   0.198    0.429  ...   135.0     152.0      53.0   1.02
Is_begin  0.890  0.848   0.001    2.432  ...    75.0      48.0      39.0   1.01
Ia_begin  2.253  2.130   0.031    6.241  ...    65.0      61.0      59.0   1.02
E_begin   1.148  1.504   0.010    4.713  ...    71.0      42.0      46.0   1.01

[8 rows x 11 columns]