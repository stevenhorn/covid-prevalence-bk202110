0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -923.16    39.67
p_loo       39.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.050   0.154    0.326  ...    31.0      27.0      58.0   1.06
pu        0.791  0.027   0.743    0.836  ...    11.0      10.0      14.0   1.16
mu        0.121  0.024   0.082    0.165  ...    26.0      25.0      95.0   1.07
mus       0.184  0.029   0.140    0.242  ...   109.0     109.0      51.0   1.00
gamma     0.279  0.050   0.168    0.356  ...   152.0     152.0      83.0   1.02
Is_begin  0.697  0.712   0.002    2.492  ...   113.0     152.0      69.0   1.02
Ia_begin  1.504  1.624   0.011    4.597  ...    94.0      59.0      59.0   1.04
E_begin   0.653  0.875   0.018    2.882  ...   101.0      71.0      59.0   1.01

[8 rows x 11 columns]