0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1457.16    34.35
p_loo       37.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      461   93.7%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.219  0.047   0.150    0.297  ...   117.0      99.0      60.0   1.05
pu        0.723  0.019   0.700    0.759  ...    86.0      75.0      38.0   1.03
mu        0.121  0.022   0.084    0.156  ...    30.0      31.0      40.0   1.00
mus       0.190  0.035   0.138    0.250  ...    77.0      99.0      45.0   1.11
gamma     0.264  0.049   0.162    0.343  ...    82.0      97.0      59.0   1.00
Is_begin  0.622  0.574   0.013    1.828  ...   133.0      93.0      71.0   1.00
Ia_begin  1.132  1.346   0.027    3.042  ...    94.0      80.0      88.0   1.00
E_begin   0.544  0.679   0.000    1.352  ...   100.0     152.0      99.0   0.99

[8 rows x 11 columns]