0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1382.02    43.61
p_loo       47.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      448   91.1%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)        11    2.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.034   0.206    0.325  ...     6.0       5.0      14.0   1.38
pu        0.745  0.011   0.730    0.764  ...     4.0       4.0      16.0   1.56
mu        0.173  0.010   0.151    0.192  ...     8.0       7.0      16.0   1.27
mus       0.236  0.032   0.180    0.311  ...     9.0       8.0      14.0   1.22
gamma     0.307  0.052   0.221    0.393  ...     6.0       7.0      35.0   1.25
Is_begin  1.320  0.712   0.114    2.474  ...     8.0       9.0      22.0   1.24
Ia_begin  2.444  2.343   0.131    6.305  ...     3.0       3.0      24.0   2.23
E_begin   0.863  0.757   0.393    2.561  ...     7.0       8.0      38.0   1.24

[8 rows x 11 columns]