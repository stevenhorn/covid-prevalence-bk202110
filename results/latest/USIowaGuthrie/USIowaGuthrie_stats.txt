0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1011.20    29.43
p_loo       35.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      449   91.3%
 (0.5, 0.7]   (ok)         36    7.3%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.052   0.163    0.327  ...    63.0      71.0      58.0   1.02
pu        0.759  0.031   0.703    0.806  ...     6.0       6.0      29.0   1.33
mu        0.133  0.022   0.097    0.170  ...    42.0      37.0      57.0   1.01
mus       0.196  0.036   0.137    0.251  ...    50.0      48.0      55.0   1.04
gamma     0.238  0.050   0.166    0.349  ...   147.0     137.0      77.0   1.03
Is_begin  0.576  0.555   0.004    1.786  ...    60.0      42.0      22.0   1.07
Ia_begin  1.100  1.111   0.005    3.179  ...    70.0      81.0     100.0   1.02
E_begin   0.464  0.634   0.007    1.390  ...    43.0      31.0      97.0   1.05

[8 rows x 11 columns]