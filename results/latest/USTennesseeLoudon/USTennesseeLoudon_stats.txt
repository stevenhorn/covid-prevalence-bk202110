0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1683.24    42.46
p_loo       35.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      466   94.7%
 (0.5, 0.7]   (ok)         20    4.1%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.055   0.159    0.328  ...    38.0      56.0      60.0   1.30
pu        0.783  0.022   0.755    0.827  ...    16.0      16.0      24.0   1.13
mu        0.106  0.016   0.082    0.133  ...    10.0      10.0      33.0   1.15
mus       0.175  0.033   0.102    0.233  ...    36.0      48.0      40.0   1.07
gamma     0.207  0.038   0.146    0.280  ...    67.0      67.0      38.0   1.09
Is_begin  0.376  0.438   0.001    1.267  ...    10.0       6.0      43.0   1.29
Ia_begin  1.234  1.211   0.038    3.511  ...    21.0      16.0      55.0   1.10
E_begin   0.610  0.535   0.006    1.695  ...    22.0      25.0      22.0   1.10

[8 rows x 11 columns]