0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1466.90    41.18
p_loo       41.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      448   91.1%
 (0.5, 0.7]   (ok)         36    7.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.054   0.155    0.331  ...    80.0      73.0      60.0   0.99
pu        0.742  0.025   0.701    0.781  ...    42.0      32.0      60.0   1.05
mu        0.155  0.025   0.113    0.207  ...    67.0      57.0      47.0   1.01
mus       0.219  0.035   0.151    0.269  ...   142.0     152.0      93.0   1.02
gamma     0.284  0.046   0.189    0.358  ...   121.0     115.0      60.0   1.00
Is_begin  1.148  0.912   0.031    2.304  ...   113.0     116.0      60.0   1.00
Ia_begin  2.597  2.886   0.011    8.473  ...   152.0     152.0      81.0   1.03
E_begin   1.585  1.920   0.014    5.655  ...    87.0      59.0      65.0   1.01

[8 rows x 11 columns]