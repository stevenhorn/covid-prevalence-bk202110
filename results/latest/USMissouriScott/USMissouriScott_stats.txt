0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1763.02    93.72
p_loo       45.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)        11    2.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.163  0.010   0.151    0.185  ...   111.0     107.0     100.0   1.06
pu        0.705  0.004   0.700    0.714  ...   152.0     150.0      88.0   0.99
mu        0.100  0.013   0.075    0.117  ...     6.0       7.0      59.0   1.32
mus       0.270  0.028   0.232    0.333  ...    47.0      69.0      48.0   1.01
gamma     0.594  0.072   0.466    0.718  ...   120.0     109.0      58.0   1.00
Is_begin  0.916  0.918   0.004    2.784  ...    99.0      81.0      33.0   1.01
Ia_begin  1.314  1.431   0.011    4.593  ...    95.0      84.0      84.0   1.02
E_begin   0.509  0.610   0.002    2.047  ...    66.0      53.0      60.0   1.07

[8 rows x 11 columns]