0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1154.43    55.82
p_loo       51.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      461   93.7%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.278  0.055   0.176    0.349  ...    71.0      63.0      59.0   1.00
pu        0.878  0.027   0.820    0.899  ...    31.0      37.0      40.0   1.02
mu        0.182  0.024   0.144    0.226  ...    14.0      14.0      18.0   1.12
mus       0.271  0.044   0.194    0.334  ...   106.0     102.0      60.0   1.00
gamma     0.398  0.069   0.281    0.515  ...    52.0      52.0      33.0   1.10
Is_begin  0.996  0.772   0.038    2.634  ...   110.0     121.0      75.0   0.98
Ia_begin  2.069  2.236   0.024    5.955  ...    94.0      63.0      56.0   1.01
E_begin   0.971  1.151   0.073    3.401  ...    68.0      60.0      59.0   1.02

[8 rows x 11 columns]