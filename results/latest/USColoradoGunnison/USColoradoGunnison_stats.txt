0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1220.76    43.30
p_loo       50.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    4    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.052   0.173    0.349  ...    92.0     103.0      59.0   0.99
pu        0.787  0.037   0.714    0.850  ...     9.0      10.0      17.0   1.17
mu        0.190  0.029   0.139    0.235  ...    34.0      35.0      91.0   1.05
mus       0.256  0.037   0.193    0.307  ...    66.0      65.0      55.0   1.04
gamma     0.357  0.061   0.239    0.465  ...   123.0     121.0      60.0   1.00
Is_begin  2.853  2.003   0.115    6.678  ...    60.0      52.0      65.0   1.00
Ia_begin  7.647  4.355   0.666   15.569  ...    88.0      76.0      42.0   0.99
E_begin   7.730  6.076   0.067   18.464  ...    61.0      57.0      14.0   1.02

[8 rows x 11 columns]