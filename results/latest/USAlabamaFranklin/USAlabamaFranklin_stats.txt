0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1614.90    68.00
p_loo       50.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      463   94.1%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.199  0.032   0.151    0.253  ...   105.0      99.0      93.0   1.06
pu        0.718  0.015   0.700    0.749  ...    51.0      73.0      56.0   1.03
mu        0.109  0.014   0.085    0.130  ...    38.0      35.0      40.0   1.04
mus       0.236  0.044   0.169    0.321  ...    19.0      21.0      97.0   1.07
gamma     0.319  0.056   0.220    0.409  ...    57.0      56.0      66.0   1.00
Is_begin  0.405  0.441   0.003    1.299  ...    83.0      72.0      56.0   1.04
Ia_begin  0.620  0.758   0.001    1.810  ...   113.0      70.0      17.0   1.02
E_begin   0.309  0.469   0.000    1.301  ...    82.0      44.0      88.0   1.05

[8 rows x 11 columns]