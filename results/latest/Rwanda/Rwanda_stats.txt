0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -2391.45    46.74
p_loo      108.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      454   92.1%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.281  0.052   0.187    0.350  ...    15.0      20.0      40.0   1.11
pu        0.850  0.060   0.721    0.900  ...     5.0       5.0      40.0   1.41
mu        0.272  0.081   0.161    0.374  ...     3.0       3.0      33.0   1.88
mus       0.267  0.054   0.168    0.354  ...     6.0       6.0      25.0   1.36
gamma     0.352  0.107   0.204    0.536  ...     3.0       4.0      20.0   1.85
Is_begin  2.797  2.183   0.058    6.350  ...   106.0      85.0      59.0   0.99
Ia_begin  6.577  6.257   0.186   17.241  ...    94.0      65.0      61.0   1.02
E_begin   4.253  4.258   0.027   12.501  ...    63.0      69.0      60.0   1.03

[8 rows x 11 columns]