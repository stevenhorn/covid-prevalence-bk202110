0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -664.61    36.51
p_loo       35.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      468   95.1%
 (0.5, 0.7]   (ok)         20    4.1%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.267  0.052   0.182    0.345  ...   152.0     152.0      55.0   0.99
pu        0.870  0.017   0.842    0.897  ...    38.0      42.0      44.0   1.00
mu        0.131  0.029   0.097    0.182  ...    55.0      51.0      56.0   1.00
mus       0.183  0.040   0.118    0.256  ...   152.0     152.0      83.0   1.02
gamma     0.216  0.039   0.157    0.284  ...   152.0     152.0      69.0   1.00
Is_begin  0.744  0.653   0.002    1.944  ...   123.0     146.0      97.0   1.02
Ia_begin  1.420  1.450   0.072    3.745  ...    92.0      78.0      60.0   1.05
E_begin   0.823  1.086   0.045    2.585  ...    59.0      38.0      57.0   1.07

[8 rows x 11 columns]