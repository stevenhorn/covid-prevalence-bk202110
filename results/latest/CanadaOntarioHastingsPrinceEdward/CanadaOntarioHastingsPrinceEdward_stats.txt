0 Divergences 
Passed validation 
2021-08-08 date of last case count
Computed from 80 by 513 log-likelihood matrix

         Estimate       SE
elpd_loo  -970.73    32.93
p_loo       39.19        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      481   93.8%
 (0.5, 0.7]   (ok)         27    5.3%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    2    0.4%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.284   0.049   0.188    0.349  ...   152.0     152.0      68.0   1.21
pu         0.886   0.014   0.859    0.900  ...   152.0     141.0      20.0   1.04
mu         0.182   0.025   0.138    0.224  ...    40.0      43.0      57.0   0.99
mus        0.222   0.036   0.165    0.291  ...   137.0     140.0      60.0   0.99
gamma      0.360   0.058   0.269    0.485  ...    86.0     108.0      59.0   1.04
Is_begin   3.452   2.946   0.152    9.009  ...    97.0     102.0      50.0   1.03
Ia_begin  11.701  12.289   0.020   34.203  ...   108.0      80.0      80.0   1.03
E_begin    4.550   4.513   0.113   13.339  ...   103.0      82.0      93.0   1.01

[8 rows x 11 columns]