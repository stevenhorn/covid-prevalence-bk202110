0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1429.55    27.59
p_loo       32.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.052   0.172    0.345  ...   104.0      97.0      38.0   1.04
pu        0.786  0.029   0.721    0.831  ...    12.0      12.0      30.0   1.13
mu        0.132  0.024   0.093    0.170  ...    31.0      31.0      40.0   1.00
mus       0.168  0.028   0.122    0.211  ...   152.0     152.0      88.0   0.99
gamma     0.198  0.034   0.151    0.266  ...    99.0     104.0      99.0   1.02
Is_begin  0.824  0.885   0.018    2.406  ...   110.0      72.0      40.0   1.02
Ia_begin  1.752  1.633   0.009    4.902  ...   107.0      63.0      62.0   1.03
E_begin   0.840  0.827   0.001    2.378  ...   130.0     127.0      59.0   1.00

[8 rows x 11 columns]