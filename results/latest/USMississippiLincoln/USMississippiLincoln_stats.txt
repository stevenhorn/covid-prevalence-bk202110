0 Divergences 
Passed validation 
2021-07-21 date of last case count
Computed from 80 by 494 log-likelihood matrix

         Estimate       SE
elpd_loo -1572.00    37.83
p_loo       44.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      461   93.3%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.224  0.048   0.152    0.308  ...   119.0     152.0      93.0   1.00
pu        0.735  0.026   0.700    0.785  ...    43.0      48.0      60.0   1.02
mu        0.119  0.030   0.061    0.168  ...     8.0       7.0      22.0   1.24
mus       0.192  0.031   0.140    0.254  ...   105.0     106.0      74.0   0.99
gamma     0.240  0.047   0.167    0.332  ...   148.0     133.0      91.0   1.01
Is_begin  1.035  0.893   0.004    2.880  ...   152.0     152.0      86.0   0.98
Ia_begin  2.205  2.156   0.008    5.877  ...    62.0     106.0      40.0   1.01
E_begin   1.235  1.173   0.044    3.514  ...   101.0      84.0      72.0   1.00

[8 rows x 11 columns]