0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1540.89    35.15
p_loo       36.77        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.056   0.152    0.328  ...    58.0      59.0      96.0   1.01
pu        0.732  0.022   0.701    0.769  ...    12.0      14.0      60.0   1.14
mu        0.131  0.028   0.086    0.179  ...    12.0      11.0      15.0   1.15
mus       0.177  0.035   0.112    0.235  ...    65.0      61.0      60.0   1.02
gamma     0.210  0.043   0.147    0.292  ...   120.0     152.0     100.0   1.00
Is_begin  0.528  0.439   0.035    1.269  ...    89.0      80.0      58.0   1.00
Ia_begin  0.893  0.948   0.007    2.581  ...    82.0      68.0      95.0   0.99
E_begin   0.488  0.497   0.017    1.398  ...    80.0      69.0      37.0   1.00

[8 rows x 11 columns]