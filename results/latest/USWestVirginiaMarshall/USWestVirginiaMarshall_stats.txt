0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1302.84    37.34
p_loo       41.27        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         22    4.5%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.056   0.154    0.335  ...    10.0      11.0      60.0   1.19
pu        0.767  0.023   0.730    0.807  ...    25.0      26.0      33.0   1.07
mu        0.127  0.028   0.067    0.178  ...     7.0       7.0      14.0   1.25
mus       0.221  0.042   0.157    0.308  ...    61.0      58.0      58.0   1.01
gamma     0.262  0.050   0.179    0.351  ...    63.0      68.0      80.0   1.01
Is_begin  0.725  0.657   0.028    1.793  ...    30.0      35.0      32.0   1.04
Ia_begin  0.591  0.454   0.041    1.485  ...    45.0      31.0      60.0   1.04
E_begin   0.377  0.296   0.007    0.911  ...    46.0      37.0      54.0   1.01

[8 rows x 11 columns]