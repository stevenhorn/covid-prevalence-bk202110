0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1224.32    62.15
p_loo       40.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.248  0.051   0.160    0.337  ...    68.0      62.0      21.0   1.04
pu        0.767  0.020   0.732    0.803  ...    55.0      59.0      60.0   1.00
mu        0.128  0.019   0.100    0.168  ...    69.0      77.0      31.0   1.01
mus       0.189  0.035   0.139    0.250  ...    47.0      52.0      59.0   1.03
gamma     0.237  0.039   0.162    0.296  ...    90.0      73.0      96.0   1.02
Is_begin  0.333  0.449   0.001    1.478  ...    67.0      25.0      61.0   1.07
Ia_begin  0.609  0.814   0.009    1.798  ...    69.0      58.0      56.0   1.04
E_begin   0.283  0.386   0.001    0.997  ...    98.0      17.0      19.0   1.10

[8 rows x 11 columns]