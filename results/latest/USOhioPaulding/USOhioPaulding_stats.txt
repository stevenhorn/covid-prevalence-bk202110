0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1042.60    33.67
p_loo       31.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         33    6.7%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.052   0.162    0.330  ...    47.0      44.0      49.0   1.03
pu        0.803  0.024   0.755    0.841  ...    16.0      22.0      38.0   1.08
mu        0.131  0.028   0.085    0.182  ...    59.0      59.0      60.0   1.01
mus       0.172  0.028   0.136    0.232  ...   129.0     117.0     100.0   0.99
gamma     0.195  0.040   0.126    0.271  ...    94.0      97.0      53.0   1.02
Is_begin  0.620  0.523   0.006    1.540  ...   107.0      81.0      76.0   1.08
Ia_begin  1.377  1.516   0.007    4.583  ...   120.0     140.0      32.0   1.03
E_begin   0.582  0.840   0.001    1.984  ...   103.0      72.0      72.0   0.98

[8 rows x 11 columns]