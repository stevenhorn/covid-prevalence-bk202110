0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1048.23    31.94
p_loo       34.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      464   94.3%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.260  0.056   0.169    0.349  ...    80.0      67.0      24.0   1.03
pu        0.786  0.024   0.743    0.825  ...    16.0      14.0      56.0   1.13
mu        0.124  0.024   0.081    0.171  ...    18.0      15.0      56.0   1.09
mus       0.206  0.032   0.153    0.264  ...    38.0     106.0      20.0   1.06
gamma     0.278  0.042   0.201    0.356  ...    90.0      94.0      59.0   0.99
Is_begin  0.462  0.532   0.004    1.639  ...    94.0      98.0      55.0   1.01
Ia_begin  0.695  0.914   0.003    2.668  ...    67.0      73.0      55.0   1.03
E_begin   0.308  0.357   0.002    1.021  ...    93.0      80.0      88.0   1.02

[8 rows x 11 columns]