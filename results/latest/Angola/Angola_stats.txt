0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -2180.76    32.18
p_loo       32.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      448   90.9%
 (0.5, 0.7]   (ok)         36    7.3%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.280  0.049   0.182    0.348  ...    40.0      40.0      40.0   1.01
pu        0.882  0.017   0.843    0.900  ...    52.0      41.0      35.0   1.06
mu        0.134  0.019   0.098    0.167  ...     4.0       4.0      24.0   1.66
mus       0.172  0.037   0.114    0.240  ...    79.0      88.0      69.0   1.02
gamma     0.189  0.031   0.140    0.256  ...    88.0      64.0      72.0   1.05
Is_begin  0.806  0.690   0.034    2.218  ...    69.0      23.0      40.0   1.10
Ia_begin  2.003  1.803   0.022    5.445  ...   119.0      43.0      97.0   1.05
E_begin   0.906  0.843   0.026    2.389  ...   100.0      78.0      59.0   1.01

[8 rows x 11 columns]