0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1278.00    40.77
p_loo       34.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      469   95.3%
 (0.5, 0.7]   (ok)         21    4.3%
   (0.7, 1]   (bad)         1    0.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.044   0.180    0.331  ...    75.0      86.0      60.0   1.03
pu        0.746  0.019   0.716    0.783  ...    41.0      34.0      91.0   1.06
mu        0.137  0.022   0.104    0.180  ...    29.0      22.0      59.0   1.11
mus       0.233  0.039   0.168    0.307  ...    64.0      65.0      87.0   1.04
gamma     0.309  0.052   0.227    0.402  ...    73.0      77.0      41.0   0.99
Is_begin  0.749  0.835   0.019    2.696  ...    61.0      30.0      48.0   1.04
Ia_begin  1.657  1.844   0.005    5.651  ...    17.0      16.0      44.0   1.09
E_begin   0.652  0.827   0.003    2.385  ...    14.0      13.0      38.0   1.11

[8 rows x 11 columns]