0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1775.85    36.79
p_loo       36.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      434   88.2%
 (0.5, 0.7]   (ok)         45    9.1%
   (0.7, 1]   (bad)        11    2.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.058   0.152    0.335  ...    90.0      89.0      59.0   1.02
pu        0.758  0.034   0.705    0.820  ...     8.0       8.0      42.0   1.22
mu        0.113  0.023   0.079    0.160  ...    45.0      43.0      60.0   1.04
mus       0.170  0.032   0.114    0.228  ...    98.0      92.0      59.0   1.10
gamma     0.223  0.036   0.161    0.287  ...    89.0      95.0      83.0   0.99
Is_begin  1.400  1.105   0.015    3.475  ...    15.0      16.0      56.0   1.11
Ia_begin  3.206  2.494   0.185    7.623  ...    72.0      64.0      60.0   1.00
E_begin   2.104  1.829   0.058    5.305  ...    72.0      62.0      91.0   1.04

[8 rows x 11 columns]