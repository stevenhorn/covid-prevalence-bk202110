0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2050.40    33.13
p_loo       37.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      449   91.3%
 (0.5, 0.7]   (ok)         33    6.7%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.233  0.045   0.151    0.296  ...    93.0      97.0      60.0   1.03
pu        0.742  0.029   0.701    0.789  ...    17.0      22.0      99.0   1.13
mu        0.115  0.021   0.081    0.151  ...     8.0       8.0      42.0   1.26
mus       0.189  0.025   0.145    0.230  ...   105.0     107.0      91.0   1.01
gamma     0.287  0.040   0.220    0.359  ...    39.0      46.0      65.0   1.04
Is_begin  0.902  0.605   0.043    2.052  ...    75.0      59.0      43.0   1.02
Ia_begin  3.735  2.001   0.927    7.738  ...   102.0     103.0      60.0   1.00
E_begin   3.966  3.310   0.133   10.301  ...    68.0      51.0      60.0   1.01

[8 rows x 11 columns]