0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2250.41    35.96
p_loo       33.94        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      451   91.7%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.211  0.046   0.153    0.288  ...    53.0      57.0      45.0   1.02
pu        0.727  0.019   0.701    0.764  ...    89.0      75.0      57.0   1.01
mu        0.104  0.017   0.077    0.128  ...    25.0      21.0      99.0   1.07
mus       0.178  0.027   0.130    0.230  ...    75.0      84.0      72.0   1.01
gamma     0.231  0.045   0.148    0.308  ...    84.0      77.0      40.0   1.01
Is_begin  1.331  0.940   0.016    3.007  ...    66.0      57.0      49.0   0.98
Ia_begin  0.722  0.601   0.004    1.782  ...    72.0      50.0      22.0   1.01
E_begin   0.795  0.628   0.011    1.955  ...    65.0      61.0      60.0   1.02

[8 rows x 11 columns]