0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -832.64    36.99
p_loo       40.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.048   0.162    0.317  ...    34.0      30.0      77.0   1.05
pu        0.774  0.028   0.725    0.821  ...     6.0       7.0      38.0   1.26
mu        0.137  0.032   0.090    0.196  ...    36.0      33.0      43.0   1.02
mus       0.231  0.042   0.166    0.333  ...   144.0     128.0      53.0   1.13
gamma     0.274  0.052   0.188    0.386  ...    95.0      95.0      43.0   0.98
Is_begin  0.537  0.587   0.012    1.600  ...    54.0      68.0      57.0   1.05
Ia_begin  0.927  1.053   0.006    2.957  ...    88.0      20.0      38.0   1.07
E_begin   0.440  0.727   0.003    1.423  ...    68.0      28.0      60.0   1.05

[8 rows x 11 columns]