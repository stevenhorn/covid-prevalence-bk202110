0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -991.32    32.03
p_loo       33.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.9%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)        11    2.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.055   0.151    0.339  ...   113.0     100.0      20.0   1.00
pu        0.793  0.022   0.751    0.829  ...    69.0      70.0      39.0   1.00
mu        0.140  0.025   0.088    0.180  ...    62.0      63.0      59.0   1.05
mus       0.200  0.038   0.139    0.268  ...   116.0     133.0      60.0   1.01
gamma     0.251  0.038   0.177    0.317  ...    85.0      85.0      91.0   0.99
Is_begin  0.720  0.627   0.006    1.960  ...    77.0      63.0      60.0   1.03
Ia_begin  1.689  1.771   0.014    5.008  ...   118.0     119.0      79.0   0.99
E_begin   0.718  0.883   0.004    2.263  ...   100.0     146.0      62.0   1.01

[8 rows x 11 columns]