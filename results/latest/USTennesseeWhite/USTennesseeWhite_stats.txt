0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1604.07    60.18
p_loo       45.60        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.199  0.035   0.155    0.263  ...   135.0     144.0      91.0   0.99
pu        0.714  0.012   0.700    0.734  ...    94.0      74.0      93.0   1.02
mu        0.119  0.017   0.093    0.153  ...    21.0      22.0      31.0   1.07
mus       0.194  0.034   0.130    0.252  ...   104.0     145.0      59.0   0.99
gamma     0.252  0.050   0.172    0.340  ...   152.0     152.0      48.0   1.05
Is_begin  0.584  0.559   0.007    1.352  ...   109.0      80.0      60.0   1.03
Ia_begin  1.112  1.430   0.006    3.493  ...   121.0     152.0      60.0   1.00
E_begin   0.463  0.561   0.005    1.355  ...   124.0     152.0      93.0   1.00

[8 rows x 11 columns]