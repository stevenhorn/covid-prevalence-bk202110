0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1046.63    74.18
p_loo       51.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      466   94.7%
 (0.5, 0.7]   (ok)         19    3.9%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.058   0.151    0.334  ...    72.0      65.0      42.0   0.99
pu        0.780  0.036   0.707    0.831  ...    37.0      36.0      38.0   1.03
mu        0.121  0.024   0.083    0.176  ...    31.0      38.0      59.0   1.04
mus       0.297  0.056   0.213    0.396  ...    63.0      76.0      52.0   1.01
gamma     0.437  0.100   0.270    0.637  ...    72.0      76.0      69.0   1.02
Is_begin  0.913  0.757   0.005    2.322  ...    93.0      66.0      30.0   1.01
Ia_begin  1.554  1.554   0.018    3.655  ...   115.0     152.0      59.0   1.00
E_begin   0.671  0.697   0.001    1.959  ...   111.0      87.0     100.0   1.00

[8 rows x 11 columns]