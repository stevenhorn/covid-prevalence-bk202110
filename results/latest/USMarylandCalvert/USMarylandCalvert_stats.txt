0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1313.54    24.48
p_loo       35.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      451   91.7%
 (0.5, 0.7]   (ok)         37    7.5%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.052   0.169    0.343  ...    75.0      84.0      49.0   1.00
pu        0.827  0.046   0.742    0.892  ...    20.0      18.0      38.0   1.10
mu        0.166  0.023   0.127    0.212  ...     7.0       7.0      33.0   1.28
mus       0.226  0.034   0.169    0.286  ...   106.0     122.0      87.0   1.02
gamma     0.327  0.060   0.236    0.431  ...   146.0     152.0     100.0   1.00
Is_begin  1.347  1.113   0.029    3.219  ...    46.0      30.0      22.0   1.08
Ia_begin  3.484  2.544   0.112    7.819  ...    82.0      68.0      91.0   0.99
E_begin   2.171  2.247   0.067    7.212  ...    88.0      56.0      42.0   1.02

[8 rows x 11 columns]