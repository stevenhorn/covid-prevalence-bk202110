75 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -4159.79    32.12
p_loo      150.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      321   65.1%
 (0.5, 0.7]   (ok)         54   11.0%
   (0.7, 1]   (bad)        53   10.8%
   (1, Inf)   (very bad)   65   13.2%

               mean        sd     hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa            0.183     0.035      0.152  ...       4.0      15.0   1.95
pu            0.728     0.026      0.705  ...       4.0      27.0   2.05
mu            0.419     0.052      0.332  ...       4.0      14.0   2.67
mus           0.365     0.130      0.254  ...       3.0      10.0   2.35
gamma         0.173     0.038      0.120  ...       3.0      30.0   2.18
Is_begin   2169.604  1292.369   1473.259  ...       3.0      19.0   2.36
Ia_begin  10105.394  3086.926   7236.258  ...       3.0      19.0   2.40
E_begin   27608.852  5493.471  20303.812  ...       5.0      38.0   2.34

[8 rows x 11 columns]