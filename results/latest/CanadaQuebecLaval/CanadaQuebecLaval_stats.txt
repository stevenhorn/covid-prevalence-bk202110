0 Divergences 
Passed validation 
2021-08-08 date of last case count
Computed from 80 by 513 log-likelihood matrix

         Estimate       SE
elpd_loo -2261.96    56.76
p_loo       43.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      443   86.4%
 (0.5, 0.7]   (ok)         57   11.1%
   (0.7, 1]   (bad)        10    1.9%
   (1, Inf)   (very bad)    3    0.6%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.246   0.057   0.155    0.345  ...   152.0     152.0      30.0   1.11
pu         0.795   0.042   0.715    0.852  ...    13.0      13.0      88.0   1.12
mu         0.126   0.017   0.094    0.152  ...    59.0      62.0      56.0   1.03
mus        0.185   0.028   0.138    0.238  ...    58.0      38.0      54.0   1.04
gamma      0.290   0.045   0.210    0.374  ...   127.0     152.0     100.0   1.00
Is_begin   7.498   5.717   0.176   17.073  ...   103.0     110.0      77.0   0.99
Ia_begin  57.570  19.164  23.642   95.612  ...    83.0      88.0      88.0   1.07
E_begin   42.788  24.786   4.659   76.605  ...    88.0     113.0      88.0   1.05

[8 rows x 11 columns]