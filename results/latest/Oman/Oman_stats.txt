1 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -3553.60    30.19
p_loo       32.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      465   94.3%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.228  0.059   0.151    0.334  ...    11.0      10.0      14.0   1.18
pu        0.854  0.040   0.767    0.900  ...     4.0       4.0      15.0   1.53
mu        0.099  0.023   0.067    0.138  ...    10.0       9.0      38.0   1.18
mus       0.165  0.023   0.127    0.207  ...   118.0     114.0      93.0   0.99
gamma     0.235  0.037   0.169    0.307  ...   105.0     108.0      59.0   1.15
Is_begin  2.877  2.593   0.042    8.996  ...    62.0      65.0      54.0   1.01
Ia_begin  5.164  5.272   0.098   16.212  ...   136.0      86.0      74.0   1.01
E_begin   3.074  3.836   0.069   11.185  ...    61.0      56.0      81.0   1.02

[8 rows x 11 columns]