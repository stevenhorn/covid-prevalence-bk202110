0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2008.34    44.65
p_loo       43.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.046   0.183    0.347  ...    49.0      47.0      40.0   1.05
pu        0.858  0.022   0.817    0.886  ...     5.0       6.0      38.0   1.37
mu        0.151  0.019   0.120    0.185  ...     7.0       6.0      20.0   1.29
mus       0.177  0.034   0.125    0.240  ...    71.0      73.0      93.0   0.99
gamma     0.234  0.044   0.165    0.333  ...    46.0      54.0      54.0   1.02
Is_begin  0.683  0.593   0.051    1.836  ...    92.0      81.0      97.0   1.01
Ia_begin  1.969  2.058   0.035    6.267  ...   134.0     134.0      60.0   1.00
E_begin   0.767  0.886   0.006    2.198  ...    76.0      83.0      40.0   1.01

[8 rows x 11 columns]