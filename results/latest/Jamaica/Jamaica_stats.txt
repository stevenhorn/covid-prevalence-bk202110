2 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -2298.32    31.61
p_loo       33.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      434   88.0%
 (0.5, 0.7]   (ok)         48    9.7%
   (0.7, 1]   (bad)        10    2.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.050   0.182    0.345  ...   120.0     100.0      58.0   0.98
pu        0.844  0.049   0.737    0.900  ...    17.0      27.0      32.0   1.07
mu        0.160  0.032   0.104    0.204  ...     6.0       7.0      32.0   1.22
mus       0.211  0.039   0.151    0.290  ...    85.0      80.0      56.0   1.02
gamma     0.287  0.046   0.221    0.384  ...   152.0     145.0      59.0   1.01
Is_begin  1.137  0.897   0.013    2.987  ...    95.0      69.0      57.0   1.01
Ia_begin  2.356  2.127   0.004    5.957  ...   135.0      57.0      59.0   1.02
E_begin   1.739  1.748   0.013    5.111  ...   114.0     122.0      61.0   1.00

[8 rows x 11 columns]