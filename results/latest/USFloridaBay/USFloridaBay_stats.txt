0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2244.18    48.09
p_loo       42.02        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      465   94.5%
 (0.5, 0.7]   (ok)         19    3.9%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.013   0.153    0.196  ...    76.0     104.0      61.0   1.08
pu        0.704  0.004   0.700    0.712  ...    93.0      58.0      17.0   1.04
mu        0.091  0.012   0.074    0.121  ...    38.0      33.0      60.0   1.02
mus       0.160  0.030   0.120    0.214  ...   130.0     152.0      44.0   0.99
gamma     0.321  0.064   0.228    0.462  ...    93.0      91.0      83.0   1.00
Is_begin  0.683  0.626   0.039    1.672  ...   121.0     114.0      99.0   1.01
Ia_begin  1.204  1.108   0.012    3.828  ...   107.0      92.0      88.0   1.00
E_begin   0.443  0.401   0.005    1.188  ...   152.0     152.0     100.0   0.99

[8 rows x 11 columns]