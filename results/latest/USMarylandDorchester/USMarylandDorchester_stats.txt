0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1380.66    41.94
p_loo       38.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.058   0.166    0.344  ...    66.0      66.0      60.0   1.02
pu        0.777  0.027   0.707    0.809  ...    38.0      38.0      30.0   1.02
mu        0.128  0.022   0.091    0.166  ...    49.0      46.0      39.0   1.03
mus       0.183  0.032   0.132    0.244  ...   151.0     152.0      96.0   1.00
gamma     0.249  0.043   0.185    0.320  ...   109.0     120.0      63.0   1.01
Is_begin  0.744  0.773   0.007    2.207  ...   118.0     152.0      38.0   0.98
Ia_begin  1.372  1.446   0.005    3.684  ...   120.0     152.0     100.0   0.98
E_begin   0.676  0.702   0.009    2.055  ...    39.0      16.0      35.0   1.11

[8 rows x 11 columns]