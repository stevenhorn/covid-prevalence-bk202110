0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2405.10    51.54
p_loo       53.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      443   90.0%
 (0.5, 0.7]   (ok)         40    8.1%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.047   0.155    0.318  ...   139.0     152.0      61.0   1.00
pu        0.799  0.050   0.725    0.893  ...    10.0       9.0      60.0   1.21
mu        0.145  0.024   0.107    0.186  ...     9.0       9.0      36.0   1.20
mus       0.215  0.032   0.162    0.277  ...    56.0      82.0      37.0   1.07
gamma     0.304  0.057   0.221    0.397  ...    84.0      90.0      59.0   1.01
Is_begin  0.801  0.699   0.009    2.131  ...   124.0     113.0      54.0   1.00
Ia_begin  3.133  1.964   0.409    6.820  ...    80.0      75.0      78.0   1.02
E_begin   1.702  1.480   0.027    4.673  ...    57.0      46.0      58.0   1.03

[8 rows x 11 columns]