0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -967.42    32.54
p_loo       32.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      468   95.1%
 (0.5, 0.7]   (ok)         18    3.7%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.051   0.150    0.332  ...    77.0      74.0      43.0   1.01
pu        0.790  0.024   0.746    0.830  ...    16.0      15.0      40.0   1.15
mu        0.113  0.020   0.084    0.146  ...    70.0      66.0      65.0   1.04
mus       0.166  0.035   0.105    0.232  ...   126.0     152.0      59.0   1.01
gamma     0.196  0.032   0.135    0.249  ...   114.0     119.0      60.0   0.99
Is_begin  0.884  0.863   0.007    2.427  ...   125.0      92.0      77.0   1.01
Ia_begin  2.455  2.138   0.042    6.571  ...    64.0     131.0      39.0   1.04
E_begin   1.162  1.339   0.028    3.410  ...    90.0      96.0      81.0   1.01

[8 rows x 11 columns]