0 Divergences 
Passed validation 
2021-07-21 date of last case count
Computed from 80 by 494 log-likelihood matrix

         Estimate       SE
elpd_loo -1815.03    58.47
p_loo       48.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.5%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)        10    2.0%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.201  0.030   0.160    0.258  ...   139.0     130.0      60.0   1.00
pu        0.718  0.014   0.700    0.742  ...    86.0      69.0      51.0   1.03
mu        0.112  0.018   0.085    0.148  ...    31.0      31.0      33.0   0.99
mus       0.217  0.038   0.140    0.270  ...   152.0     152.0      65.0   1.13
gamma     0.296  0.054   0.219    0.407  ...    42.0      43.0      91.0   1.04
Is_begin  0.863  0.882   0.026    2.828  ...   125.0     128.0      55.0   0.99
Ia_begin  0.657  0.543   0.006    1.446  ...   138.0     152.0      48.0   1.05
E_begin   0.457  0.437   0.002    1.233  ...   117.0     125.0      52.0   1.02

[8 rows x 11 columns]