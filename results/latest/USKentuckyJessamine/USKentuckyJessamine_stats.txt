0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1424.61    28.64
p_loo       27.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      466   94.7%
 (0.5, 0.7]   (ok)         22    4.5%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.049   0.158    0.328  ...    79.0      77.0      59.0   1.04
pu        0.763  0.034   0.705    0.819  ...    13.0      16.0      59.0   1.13
mu        0.121  0.022   0.079    0.160  ...    37.0      44.0      58.0   1.02
mus       0.187  0.040   0.109    0.245  ...   152.0     152.0      59.0   1.02
gamma     0.230  0.049   0.146    0.304  ...    86.0      87.0      55.0   1.00
Is_begin  1.032  0.929   0.011    2.630  ...   152.0     142.0      69.0   1.00
Ia_begin  2.544  2.392   0.028    7.278  ...    96.0      60.0      40.0   1.02
E_begin   1.219  1.126   0.007    3.463  ...    97.0      83.0      91.0   1.03

[8 rows x 11 columns]