0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1668.10    27.63
p_loo       27.03        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      444   90.2%
 (0.5, 0.7]   (ok)         36    7.3%
   (0.7, 1]   (bad)        10    2.0%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.178  0.024   0.152    0.231  ...   147.0     151.0      59.0   1.05
pu        0.709  0.007   0.700    0.724  ...    85.0      83.0      56.0   1.01
mu        0.093  0.018   0.066    0.123  ...    55.0      51.0      39.0   1.00
mus       0.141  0.021   0.103    0.179  ...   109.0      96.0      99.0   0.99
gamma     0.209  0.039   0.150    0.286  ...   152.0     152.0     102.0   0.99
Is_begin  0.587  0.650   0.000    1.831  ...   119.0      85.0      22.0   0.99
Ia_begin  0.916  0.882   0.004    2.556  ...    89.0      77.0      40.0   1.00
E_begin   0.371  0.327   0.006    0.845  ...    81.0      78.0      57.0   0.99

[8 rows x 11 columns]