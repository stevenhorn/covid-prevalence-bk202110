0 Divergences 
Passed validation 
2021-08-06 date of last case count
Computed from 80 by 511 log-likelihood matrix

         Estimate       SE
elpd_loo -2339.71    46.92
p_loo       41.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      461   90.2%
 (0.5, 0.7]   (ok)         42    8.2%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.242   0.057   0.155    0.331  ...   152.0     129.0      60.0   1.04
pu         0.779   0.045   0.717    0.866  ...    39.0      45.0      52.0   1.01
mu         0.154   0.019   0.119    0.188  ...    11.0      12.0      59.0   1.14
mus        0.185   0.024   0.140    0.228  ...   152.0     152.0      83.0   0.99
gamma      0.305   0.044   0.224    0.387  ...    74.0      87.0      54.0   1.01
Is_begin   7.279   5.327   0.245   16.557  ...    57.0      37.0      24.0   1.04
Ia_begin  64.797  29.986  17.327  114.596  ...    88.0      69.0      59.0   1.02
E_begin   50.310  37.549   5.699  112.898  ...   106.0      67.0      60.0   1.03

[8 rows x 11 columns]