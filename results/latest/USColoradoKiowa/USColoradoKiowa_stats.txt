0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -202.46    44.18
p_loo       41.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.9%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)        12    2.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.273  0.049   0.194    0.345  ...    26.0      29.0      43.0   1.05
pu        0.884  0.012   0.866    0.900  ...    75.0      99.0      57.0   1.00
mu        0.122  0.023   0.091    0.177  ...    66.0      78.0      80.0   1.02
mus       0.223  0.033   0.169    0.283  ...   152.0     152.0      70.0   0.98
gamma     0.267  0.038   0.207    0.340  ...    88.0      91.0      59.0   1.01
Is_begin  0.440  0.559   0.004    1.359  ...   111.0      56.0      88.0   1.00
Ia_begin  0.676  0.942   0.002    1.651  ...    66.0      73.0      53.0   0.98
E_begin   0.343  0.415   0.011    1.264  ...   119.0      98.0      80.0   1.00

[8 rows x 11 columns]