0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1113.78    30.66
p_loo       32.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         26    5.3%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.059   0.157    0.344  ...   108.0      90.0      60.0   0.99
pu        0.832  0.019   0.787    0.856  ...    65.0      64.0      60.0   1.03
mu        0.151  0.023   0.100    0.184  ...    61.0      62.0      60.0   1.00
mus       0.188  0.027   0.131    0.237  ...   152.0     152.0      70.0   1.02
gamma     0.234  0.040   0.178    0.320  ...   134.0     132.0      93.0   1.04
Is_begin  0.581  0.637   0.003    1.727  ...    67.0     122.0      60.0   1.00
Ia_begin  1.050  0.999   0.028    3.252  ...    78.0      63.0      96.0   1.10
E_begin   0.468  0.469   0.011    1.294  ...   101.0     122.0      93.0   1.01

[8 rows x 11 columns]