0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -978.29    30.52
p_loo       39.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   93.3%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.061   0.160    0.346  ...    94.0     118.0      59.0   1.01
pu        0.795  0.028   0.742    0.840  ...    50.0      55.0      57.0   1.03
mu        0.153  0.027   0.107    0.200  ...    44.0      40.0      88.0   1.01
mus       0.217  0.034   0.161    0.279  ...    69.0      86.0      72.0   1.02
gamma     0.287  0.047   0.208    0.367  ...   146.0     138.0      60.0   1.00
Is_begin  0.762  0.815   0.003    2.458  ...   106.0      34.0      31.0   1.06
Ia_begin  1.715  1.367   0.033    4.249  ...    36.0      39.0      33.0   1.05
E_begin   0.671  0.609   0.011    1.720  ...   119.0      61.0      40.0   1.01

[8 rows x 11 columns]