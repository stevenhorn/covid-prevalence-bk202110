0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2880.17    34.65
p_loo       28.93        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      437   88.8%
 (0.5, 0.7]   (ok)         47    9.6%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.241  0.068   0.151    0.339  ...    59.0      48.0      75.0   1.07
pu        0.739  0.022   0.702    0.781  ...    73.0      76.0      35.0   1.00
mu        0.136  0.025   0.101    0.190  ...    49.0      63.0      60.0   1.00
mus       0.170  0.032   0.125    0.234  ...   105.0      90.0      59.0   1.00
gamma     0.204  0.039   0.133    0.279  ...   126.0     103.0      43.0   1.00
Is_begin  0.940  1.018   0.004    3.398  ...    55.0      44.0      72.0   1.03
Ia_begin  1.718  1.401   0.052    4.092  ...   110.0      93.0      80.0   1.00
E_begin   0.903  0.837   0.013    2.583  ...   117.0     100.0      60.0   0.98

[8 rows x 11 columns]