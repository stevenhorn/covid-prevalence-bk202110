0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1410.88    32.22
p_loo       39.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      433   88.0%
 (0.5, 0.7]   (ok)         44    8.9%
   (0.7, 1]   (bad)        14    2.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.220  0.047   0.153    0.303  ...    46.0      53.0      42.0   1.08
pu        0.720  0.015   0.701    0.746  ...    48.0      25.0      26.0   1.09
mu        0.138  0.024   0.099    0.170  ...    12.0      11.0      57.0   1.14
mus       0.194  0.037   0.135    0.247  ...    79.0      64.0      40.0   1.07
gamma     0.257  0.043   0.185    0.337  ...   152.0     152.0      91.0   1.02
Is_begin  0.496  0.571   0.001    1.883  ...   128.0      66.0      60.0   1.00
Ia_begin  0.893  1.186   0.023    3.636  ...    59.0      48.0      55.0   1.05
E_begin   0.517  0.851   0.001    1.786  ...    79.0      92.0      55.0   1.03

[8 rows x 11 columns]