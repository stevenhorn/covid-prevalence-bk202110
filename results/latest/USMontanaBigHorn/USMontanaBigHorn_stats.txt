0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1553.32    59.37
p_loo       50.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      465   94.5%
 (0.5, 0.7]   (ok)         20    4.1%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.014   0.150    0.190  ...   152.0     152.0      37.0   1.01
pu        0.705  0.004   0.700    0.712  ...    96.0      73.0      55.0   1.00
mu        0.104  0.018   0.072    0.139  ...    35.0      25.0      60.0   1.09
mus       0.185  0.021   0.155    0.231  ...    62.0      64.0      57.0   1.04
gamma     0.282  0.057   0.189    0.380  ...   136.0     152.0      59.0   1.07
Is_begin  0.403  0.438   0.001    1.303  ...    82.0      64.0      59.0   1.04
Ia_begin  0.619  0.813   0.001    2.475  ...    94.0      52.0      57.0   1.04
E_begin   0.260  0.296   0.006    0.825  ...    74.0      65.0      60.0   1.04

[8 rows x 11 columns]