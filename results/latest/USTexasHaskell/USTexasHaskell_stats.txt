0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1029.09    71.46
p_loo       75.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         23    4.7%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    4    0.8%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.053   0.164    0.333  ...    44.0      46.0      57.0   1.03
pu        0.762  0.027   0.709    0.814  ...     5.0       5.0      41.0   1.46
mu        0.134  0.027   0.086    0.179  ...     8.0       7.0      57.0   1.26
mus       0.225  0.043   0.158    0.311  ...    93.0      88.0      66.0   1.00
gamma     0.269  0.046   0.182    0.340  ...    68.0      67.0      59.0   1.05
Is_begin  0.446  0.487   0.009    1.440  ...    90.0      44.0      60.0   1.02
Ia_begin  0.710  0.931   0.002    2.530  ...    73.0      54.0      99.0   1.01
E_begin   0.315  0.440   0.003    1.179  ...   102.0      98.0     100.0   0.99

[8 rows x 11 columns]