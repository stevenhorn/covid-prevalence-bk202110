0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1651.75    32.53
p_loo       33.40        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      461   93.7%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.052   0.162    0.335  ...    56.0      57.0      60.0   1.09
pu        0.823  0.026   0.782    0.874  ...    20.0      19.0      38.0   1.09
mu        0.152  0.026   0.096    0.192  ...     7.0       7.0      17.0   1.26
mus       0.194  0.031   0.150    0.266  ...    60.0      68.0      21.0   1.06
gamma     0.284  0.047   0.217    0.389  ...    66.0      62.0      86.0   1.01
Is_begin  0.771  0.808   0.011    2.835  ...    86.0      51.0      40.0   1.02
Ia_begin  0.707  0.519   0.036    1.738  ...    69.0      53.0      60.0   1.02
E_begin   0.533  0.587   0.003    1.684  ...    47.0      36.0      48.0   1.06

[8 rows x 11 columns]