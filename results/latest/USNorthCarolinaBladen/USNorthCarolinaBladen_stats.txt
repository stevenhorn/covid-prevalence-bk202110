0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1516.67    35.24
p_loo       36.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.9%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.054   0.162    0.324  ...    87.0      90.0      60.0   1.03
pu        0.780  0.023   0.738    0.819  ...   103.0     127.0      60.0   0.99
mu        0.116  0.020   0.081    0.154  ...    25.0      28.0      86.0   1.06
mus       0.152  0.025   0.110    0.205  ...   123.0     121.0      88.0   1.05
gamma     0.160  0.028   0.100    0.208  ...    83.0      75.0      95.0   1.03
Is_begin  0.575  0.586   0.017    1.810  ...    83.0     109.0      60.0   1.00
Ia_begin  0.888  0.825   0.045    2.596  ...   108.0     108.0      72.0   1.01
E_begin   0.561  0.652   0.006    1.449  ...   104.0      87.0      72.0   1.00

[8 rows x 11 columns]