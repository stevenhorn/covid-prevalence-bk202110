0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1503.22    52.30
p_loo       58.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   92.1%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.211  0.042   0.154    0.286  ...   136.0     131.0      93.0   0.99
pu        0.716  0.011   0.701    0.736  ...   125.0     133.0      71.0   1.02
mu        0.152  0.031   0.101    0.221  ...    50.0      50.0      72.0   1.00
mus       0.220  0.040   0.155    0.294  ...    48.0      42.0      56.0   1.04
gamma     0.296  0.055   0.184    0.385  ...    59.0      55.0     100.0   1.02
Is_begin  0.643  0.693   0.013    2.101  ...    94.0      98.0      60.0   1.00
Ia_begin  1.144  1.116   0.002    3.727  ...    96.0      93.0      80.0   1.01
E_begin   0.554  0.596   0.011    1.830  ...    75.0      76.0      72.0   1.06

[8 rows x 11 columns]