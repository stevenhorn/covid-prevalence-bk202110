0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1272.67    29.45
p_loo       27.63        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.259  0.056   0.156    0.337  ...    62.0      68.0      59.0   1.00
pu        0.786  0.025   0.729    0.823  ...    21.0      25.0      24.0   1.08
mu        0.113  0.020   0.068    0.143  ...    46.0      47.0      60.0   1.04
mus       0.155  0.035   0.098    0.218  ...   115.0     136.0      80.0   1.01
gamma     0.167  0.035   0.104    0.227  ...   129.0     142.0      72.0   1.00
Is_begin  0.466  0.419   0.018    1.210  ...   106.0      83.0      60.0   1.00
Ia_begin  0.872  0.828   0.019    2.025  ...    94.0      98.0      60.0   1.01
E_begin   0.434  0.438   0.005    1.405  ...    82.0      67.0      93.0   1.03

[8 rows x 11 columns]