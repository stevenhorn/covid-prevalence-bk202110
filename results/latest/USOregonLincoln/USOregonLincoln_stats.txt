0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1290.62    64.42
p_loo       52.82        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      449   91.3%
 (0.5, 0.7]   (ok)         39    7.9%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.297  0.041   0.216    0.348  ...    61.0     104.0      97.0   1.05
pu        0.894  0.006   0.885    0.900  ...    89.0      82.0      69.0   0.98
mu        0.190  0.019   0.142    0.215  ...    21.0      22.0      38.0   1.08
mus       0.238  0.037   0.180    0.305  ...   111.0     104.0      36.0   1.00
gamma     0.443  0.081   0.327    0.597  ...   152.0     152.0      99.0   0.98
Is_begin  0.880  0.873   0.030    2.757  ...   102.0      97.0      59.0   1.02
Ia_begin  1.834  1.704   0.001    5.445  ...    47.0      21.0      30.0   1.04
E_begin   0.824  0.748   0.002    2.091  ...    49.0      45.0      24.0   1.01

[8 rows x 11 columns]