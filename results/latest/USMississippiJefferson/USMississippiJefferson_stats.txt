0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -917.33    41.07
p_loo       37.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         23    4.7%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.246  0.054   0.158    0.341  ...    43.0      39.0      38.0   1.02
pu        0.831  0.019   0.805    0.860  ...    11.0      11.0      30.0   1.15
mu        0.113  0.023   0.073    0.158  ...     8.0       7.0      17.0   1.27
mus       0.181  0.037   0.130    0.254  ...   106.0     126.0      59.0   1.00
gamma     0.232  0.051   0.148    0.322  ...   125.0     149.0      99.0   0.98
Is_begin  0.664  0.674   0.011    1.964  ...   116.0     129.0     100.0   1.01
Ia_begin  1.061  1.155   0.000    3.145  ...   126.0      71.0      22.0   1.00
E_begin   0.539  0.564   0.008    1.564  ...    56.0      30.0      76.0   1.05

[8 rows x 11 columns]