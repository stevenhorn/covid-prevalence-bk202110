0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -855.37    26.80
p_loo       29.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.9%
 (0.5, 0.7]   (ok)         33    6.7%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.046   0.157    0.315  ...    13.0      12.0      17.0   1.12
pu        0.876  0.018   0.839    0.899  ...    40.0      42.0      93.0   1.04
mu        0.154  0.034   0.081    0.207  ...    21.0      23.0      40.0   1.07
mus       0.183  0.033   0.125    0.249  ...    67.0      97.0      56.0   1.02
gamma     0.211  0.042   0.146    0.290  ...    49.0      44.0      40.0   1.06
Is_begin  0.970  0.758   0.017    2.193  ...   120.0      92.0      59.0   1.01
Ia_begin  2.140  2.091   0.018    6.143  ...    51.0      34.0      54.0   1.06
E_begin   0.961  1.069   0.005    3.533  ...    22.0      17.0      21.0   1.09

[8 rows x 11 columns]