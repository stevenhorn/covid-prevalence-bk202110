0 Divergences 
Passed validation 
2021-08-06 date of last case count
Computed from 80 by 511 log-likelihood matrix

         Estimate       SE
elpd_loo -1409.95    41.46
p_loo       35.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      453   88.6%
 (0.5, 0.7]   (ok)         42    8.2%
   (0.7, 1]   (bad)        15    2.9%
   (1, Inf)   (very bad)    1    0.2%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.217  0.024   0.174    0.264  ...    17.0      18.0      31.0   1.11
pu         0.890  0.004   0.883    0.895  ...     4.0       4.0      22.0   1.87
mu         0.293  0.027   0.256    0.324  ...     3.0       3.0      17.0   2.23
mus        0.233  0.016   0.204    0.255  ...     4.0       4.0      14.0   1.62
gamma      0.320  0.029   0.267    0.362  ...     4.0       4.0      14.0   1.64
Is_begin   3.953  1.939   0.998    7.132  ...     5.0       5.0      14.0   1.36
Ia_begin  23.646  8.261   7.401   37.646  ...     4.0       4.0      43.0   1.61
E_begin    7.262  5.426   0.608   17.138  ...     3.0       4.0      15.0   1.87

[8 rows x 11 columns]