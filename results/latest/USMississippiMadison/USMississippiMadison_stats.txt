0 Divergences 
Passed validation 
2021-07-21 date of last case count
Computed from 80 by 494 log-likelihood matrix

         Estimate       SE
elpd_loo -1935.20    33.76
p_loo       42.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      454   91.9%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.057   0.151    0.321  ...   126.0     148.0      66.0   1.09
pu        0.727  0.022   0.701    0.767  ...   120.0      85.0      56.0   1.02
mu        0.098  0.017   0.073    0.124  ...    50.0      51.0     100.0   1.02
mus       0.164  0.032   0.111    0.218  ...   152.0     152.0      77.0   1.04
gamma     0.219  0.040   0.161    0.298  ...    90.0     101.0      95.0   1.06
Is_begin  1.120  1.176   0.001    3.048  ...   115.0      75.0      40.0   1.00
Ia_begin  0.692  0.604   0.004    1.571  ...    54.0      15.0      16.0   1.09
E_begin   0.633  0.780   0.002    1.833  ...    81.0      60.0      60.0   1.01

[8 rows x 11 columns]