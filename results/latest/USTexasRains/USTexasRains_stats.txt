0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1147.21    62.04
p_loo       43.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.061   0.165    0.350  ...    46.0      38.0      39.0   1.07
pu        0.866  0.018   0.833    0.892  ...    25.0      24.0      40.0   1.08
mu        0.134  0.022   0.094    0.175  ...    42.0      56.0      41.0   1.04
mus       0.194  0.034   0.141    0.258  ...    90.0     108.0      91.0   1.04
gamma     0.265  0.041   0.190    0.337  ...    76.0      85.0      80.0   0.99
Is_begin  0.790  0.702   0.051    2.278  ...   102.0      89.0      60.0   1.00
Ia_begin  1.606  1.653   0.012    5.004  ...    90.0      73.0      60.0   0.99
E_begin   0.746  0.805   0.001    2.163  ...    94.0      77.0      57.0   1.00

[8 rows x 11 columns]