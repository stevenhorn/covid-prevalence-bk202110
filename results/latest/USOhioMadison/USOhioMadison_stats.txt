0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1429.09    47.86
p_loo       45.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.233  0.049   0.156    0.322  ...    46.0      34.0      38.0   1.04
pu        0.755  0.023   0.717    0.791  ...    22.0      19.0      35.0   1.09
mu        0.149  0.028   0.105    0.200  ...     4.0       4.0      17.0   1.56
mus       0.285  0.045   0.211    0.359  ...    93.0      93.0      77.0   0.99
gamma     0.363  0.052   0.269    0.454  ...   152.0     152.0     100.0   1.00
Is_begin  0.974  0.862   0.037    2.638  ...    47.0      39.0      54.0   1.05
Ia_begin  1.442  1.237   0.009    3.543  ...    61.0      68.0      43.0   1.03
E_begin   0.688  0.587   0.051    2.053  ...   116.0     112.0      83.0   1.01

[8 rows x 11 columns]