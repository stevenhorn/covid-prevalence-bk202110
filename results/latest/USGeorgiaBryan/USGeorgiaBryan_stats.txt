0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1460.70    44.15
p_loo       42.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      447   90.9%
 (0.5, 0.7]   (ok)         36    7.3%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.235  0.054   0.154    0.325  ...   138.0     124.0      80.0   1.07
pu        0.737  0.025   0.702    0.779  ...    43.0      50.0      24.0   1.11
mu        0.114  0.024   0.073    0.159  ...     8.0      10.0      15.0   1.16
mus       0.172  0.031   0.117    0.225  ...    94.0      99.0      59.0   1.02
gamma     0.210  0.033   0.161    0.277  ...    43.0      44.0      57.0   1.03
Is_begin  0.904  0.837   0.008    2.310  ...    90.0      85.0      60.0   1.02
Ia_begin  1.662  1.532   0.044    4.431  ...   115.0     142.0      52.0   1.00
E_begin   0.872  0.961   0.001    3.205  ...   125.0      90.0      60.0   1.01

[8 rows x 11 columns]