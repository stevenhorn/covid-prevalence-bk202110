0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1061.11    36.13
p_loo       35.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.055   0.152    0.337  ...    87.0      84.0      42.0   1.03
pu        0.771  0.034   0.702    0.815  ...     7.0       9.0      22.0   1.24
mu        0.124  0.023   0.085    0.166  ...    19.0      13.0      58.0   1.15
mus       0.202  0.029   0.154    0.250  ...    61.0      67.0      29.0   1.00
gamma     0.232  0.043   0.165    0.317  ...    90.0     112.0      81.0   1.01
Is_begin  0.548  0.673   0.002    1.671  ...   108.0     116.0      95.0   1.02
Ia_begin  0.964  1.009   0.019    3.404  ...    63.0      43.0      93.0   1.06
E_begin   0.539  0.717   0.003    1.990  ...    96.0      88.0      93.0   0.99

[8 rows x 11 columns]