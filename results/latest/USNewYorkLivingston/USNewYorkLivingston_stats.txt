0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1235.86    32.94
p_loo       37.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      451   91.7%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.053   0.173    0.349  ...    62.0      65.0      49.0   1.05
pu        0.840  0.017   0.805    0.866  ...    19.0      21.0      96.0   1.13
mu        0.140  0.022   0.102    0.176  ...    10.0      10.0      39.0   1.20
mus       0.191  0.030   0.145    0.257  ...   152.0     152.0      91.0   1.00
gamma     0.271  0.051   0.171    0.352  ...   101.0     100.0      54.0   1.00
Is_begin  1.475  1.075   0.003    3.731  ...    41.0      31.0      15.0   1.04
Ia_begin  0.794  0.671   0.002    1.739  ...    97.0      62.0      38.0   1.01
E_begin   1.104  0.852   0.049    2.396  ...    55.0      53.0      58.0   0.98

[8 rows x 11 columns]