0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1079.40    30.52
p_loo       30.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.053   0.159    0.335  ...    14.0      15.0      56.0   1.14
pu        0.835  0.020   0.799    0.871  ...     7.0       7.0      40.0   1.27
mu        0.139  0.028   0.090    0.192  ...     7.0       7.0      18.0   1.24
mus       0.185  0.034   0.132    0.249  ...    67.0      85.0      37.0   1.01
gamma     0.239  0.041   0.156    0.309  ...   116.0     122.0      27.0   1.27
Is_begin  0.656  0.627   0.006    1.844  ...    73.0      85.0      49.0   1.02
Ia_begin  1.328  1.502   0.020    4.316  ...    53.0      32.0      72.0   1.07
E_begin   0.687  0.819   0.005    2.166  ...    70.0      65.0      57.0   1.03

[8 rows x 11 columns]