0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -837.14    40.78
p_loo       32.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   93.3%
 (0.5, 0.7]   (ok)         26    5.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.053   0.159    0.337  ...    84.0      85.0      32.0   1.03
pu        0.803  0.018   0.764    0.831  ...    69.0      67.0      83.0   1.02
mu        0.119  0.018   0.091    0.153  ...   106.0     104.0      39.0   0.98
mus       0.183  0.033   0.130    0.246  ...    93.0      91.0      95.0   1.03
gamma     0.203  0.041   0.130    0.276  ...   152.0     152.0      54.0   1.01
Is_begin  0.674  0.730   0.006    2.045  ...    81.0      62.0      59.0   1.03
Ia_begin  1.333  1.558   0.003    4.325  ...   118.0      97.0      59.0   1.03
E_begin   0.572  0.786   0.005    2.219  ...    55.0      81.0      60.0   1.00

[8 rows x 11 columns]