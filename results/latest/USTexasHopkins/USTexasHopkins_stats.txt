0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1692.20    69.33
p_loo       50.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      463   94.1%
 (0.5, 0.7]   (ok)         22    4.5%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.242  0.052   0.161    0.324  ...   106.0     104.0      57.0   1.04
pu        0.744  0.024   0.701    0.787  ...    72.0      64.0      57.0   1.06
mu        0.118  0.015   0.085    0.145  ...    64.0      59.0      53.0   1.02
mus       0.277  0.047   0.214    0.381  ...    75.0      78.0      22.0   1.02
gamma     0.394  0.064   0.286    0.516  ...    96.0     107.0      96.0   1.01
Is_begin  0.831  0.708   0.014    2.059  ...   125.0     122.0      49.0   1.01
Ia_begin  1.244  1.149   0.044    3.389  ...   110.0     109.0      68.0   1.02
E_begin   0.495  0.474   0.009    1.401  ...   101.0     107.0      91.0   1.02

[8 rows x 11 columns]