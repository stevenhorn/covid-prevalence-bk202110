5 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -2173.44    33.80
p_loo       36.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.5%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.288  0.039   0.207    0.344  ...    99.0      89.0      60.0   1.06
pu        0.886  0.015   0.854    0.900  ...    16.0      24.0      35.0   1.07
mu        0.146  0.022   0.114    0.195  ...    17.0      17.0      49.0   1.11
mus       0.162  0.029   0.102    0.205  ...   123.0     114.0      49.0   0.99
gamma     0.221  0.036   0.165    0.280  ...   152.0     152.0      99.0   0.99
Is_begin  1.086  0.970   0.012    2.967  ...    79.0      87.0      33.0   1.04
Ia_begin  0.674  0.536   0.020    1.635  ...    84.0      36.0      59.0   1.05
E_begin   0.590  0.626   0.000    1.771  ...    77.0      42.0      14.0   1.02

[8 rows x 11 columns]