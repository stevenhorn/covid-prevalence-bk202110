0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1278.72    38.17
p_loo       36.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      467   94.9%
 (0.5, 0.7]   (ok)         20    4.1%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.052   0.151    0.326  ...    17.0      15.0      31.0   1.10
pu        0.836  0.035   0.777    0.878  ...     8.0       8.0      96.0   1.24
mu        0.135  0.023   0.091    0.176  ...    21.0      21.0      57.0   1.07
mus       0.187  0.030   0.131    0.235  ...    82.0      93.0      95.0   1.11
gamma     0.256  0.043   0.163    0.334  ...    82.0      70.0      56.0   1.02
Is_begin  1.351  1.002   0.075    3.421  ...   152.0     118.0      40.0   1.00
Ia_begin  3.121  2.369   0.147    7.933  ...    72.0      56.0      88.0   1.00
E_begin   2.026  1.443   0.174    4.879  ...    72.0      81.0      79.0   0.99

[8 rows x 11 columns]