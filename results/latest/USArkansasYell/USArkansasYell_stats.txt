0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1381.76    39.30
p_loo       39.28        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         23    4.7%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.201  0.028   0.153    0.252  ...    63.0      66.0      60.0   0.99
pu        0.718  0.012   0.701    0.742  ...    73.0      68.0      88.0   1.01
mu        0.115  0.017   0.088    0.146  ...    45.0      42.0      35.0   1.00
mus       0.172  0.030   0.111    0.223  ...    84.0      84.0      60.0   1.00
gamma     0.206  0.035   0.154    0.278  ...   152.0     148.0      42.0   1.00
Is_begin  0.337  0.345   0.013    0.952  ...   103.0     100.0      59.0   1.02
Ia_begin  0.491  0.603   0.008    1.763  ...    67.0      83.0      60.0   1.00
E_begin   0.257  0.259   0.002    0.676  ...    73.0      61.0      60.0   1.00

[8 rows x 11 columns]