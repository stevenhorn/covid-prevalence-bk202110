0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -589.92    34.17
p_loo       33.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.286  0.042   0.210    0.347  ...    77.0     109.0      59.0   1.00
pu        0.890  0.008   0.876    0.900  ...    28.0      36.0      34.0   1.05
mu        0.132  0.022   0.089    0.167  ...    76.0      74.0      96.0   1.01
mus       0.160  0.024   0.126    0.212  ...   143.0     152.0      67.0   1.01
gamma     0.200  0.031   0.142    0.254  ...   137.0     147.0      75.0   1.00
Is_begin  0.705  0.508   0.086    1.704  ...    90.0      89.0     100.0   0.99
Ia_begin  1.710  1.764   0.025    4.850  ...   110.0     125.0      73.0   1.06
E_begin   0.711  0.628   0.013    2.000  ...    90.0      81.0      62.0   1.04

[8 rows x 11 columns]