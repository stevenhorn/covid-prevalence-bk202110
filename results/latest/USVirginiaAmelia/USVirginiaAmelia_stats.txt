0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1011.44    46.46
p_loo       37.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      466   94.7%
 (0.5, 0.7]   (ok)         21    4.3%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.061   0.174    0.350  ...    32.0      31.0      22.0   1.05
pu        0.846  0.020   0.814    0.879  ...    43.0      45.0      60.0   1.00
mu        0.137  0.025   0.099    0.186  ...    28.0      27.0      43.0   1.02
mus       0.200  0.032   0.150    0.277  ...    93.0      86.0      80.0   1.04
gamma     0.246  0.040   0.167    0.306  ...   105.0     101.0      96.0   1.02
Is_begin  0.926  0.905   0.007    2.536  ...    71.0      26.0      40.0   1.06
Ia_begin  2.026  1.838   0.078    5.389  ...    72.0      65.0      59.0   1.05
E_begin   0.911  0.905   0.010    2.917  ...    74.0      90.0      40.0   0.99

[8 rows x 11 columns]