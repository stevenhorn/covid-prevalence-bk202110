0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1008.67    38.68
p_loo       36.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.7%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.261  0.055   0.169    0.345  ...    64.0      62.0      43.0   0.99
pu        0.799  0.019   0.766    0.831  ...    49.0      51.0     100.0   0.99
mu        0.129  0.025   0.087    0.179  ...    23.0      20.0      31.0   1.01
mus       0.217  0.038   0.165    0.314  ...    47.0      82.0      14.0   1.03
gamma     0.238  0.040   0.157    0.306  ...    74.0     102.0      37.0   1.00
Is_begin  0.386  0.420   0.008    1.122  ...    67.0      74.0      77.0   1.00
Ia_begin  0.579  0.714   0.005    1.785  ...    59.0      57.0     100.0   1.02
E_begin   0.305  0.373   0.004    1.072  ...    47.0      46.0      59.0   1.04

[8 rows x 11 columns]