0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1222.71    55.03
p_loo       45.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      469   95.3%
 (0.5, 0.7]   (ok)         18    3.7%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.263  0.051   0.180    0.344  ...    48.0      51.0      58.0   1.01
pu        0.846  0.016   0.814    0.873  ...    47.0      45.0      59.0   1.01
mu        0.144  0.029   0.104    0.208  ...     8.0       8.0      57.0   1.20
mus       0.196  0.033   0.143    0.248  ...    64.0      66.0      60.0   1.04
gamma     0.225  0.042   0.153    0.295  ...   121.0     117.0      88.0   1.01
Is_begin  0.893  0.765   0.023    2.279  ...    90.0      96.0      74.0   0.98
Ia_begin  2.195  2.297   0.036    7.383  ...    35.0      70.0      19.0   1.01
E_begin   0.917  0.960   0.019    3.189  ...    51.0      20.0      60.0   1.08

[8 rows x 11 columns]