49 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -3562.58    41.74
p_loo       30.31        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      421   85.6%
 (0.5, 0.7]   (ok)         51   10.4%
   (0.7, 1]   (bad)        15    3.0%
   (1, Inf)   (very bad)    5    1.0%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.184   0.032   0.157    0.249  ...     4.0       3.0      14.0   2.07
pu         0.749   0.025   0.711    0.776  ...     3.0       3.0      87.0   2.12
mu         0.104   0.019   0.089    0.139  ...     5.0       5.0      15.0   1.58
mus        0.147   0.046   0.105    0.235  ...     3.0       3.0      22.0   2.03
gamma      0.209   0.057   0.154    0.306  ...     3.0       3.0      26.0   1.91
Is_begin  14.628   5.950   1.701   21.572  ...     6.0       6.0      40.0   1.48
Ia_begin  16.538  10.201   0.761   37.855  ...    93.0      56.0      14.0   1.59
E_begin   12.290  13.249   1.524   41.256  ...     6.0       7.0      20.0   1.81

[8 rows x 11 columns]