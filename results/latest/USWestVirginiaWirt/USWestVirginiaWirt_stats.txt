0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -772.36    38.20
p_loo       41.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      469   95.3%
 (0.5, 0.7]   (ok)         17    3.5%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.056   0.175    0.345  ...    52.0      59.0      96.0   1.01
pu        0.851  0.018   0.824    0.880  ...    20.0      17.0      34.0   1.11
mu        0.156  0.027   0.111    0.210  ...    17.0      14.0      17.0   1.12
mus       0.194  0.033   0.136    0.245  ...    42.0      39.0      59.0   1.05
gamma     0.257  0.045   0.188    0.340  ...    53.0      83.0      47.0   1.07
Is_begin  0.714  0.497   0.048    1.581  ...   108.0      87.0      88.0   1.02
Ia_begin  1.350  1.216   0.004    4.124  ...    38.0      31.0      60.0   1.05
E_begin   0.622  0.577   0.019    1.622  ...    74.0      59.0      59.0   1.01

[8 rows x 11 columns]