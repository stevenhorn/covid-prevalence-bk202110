0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2412.50    44.63
p_loo       42.58        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      435   88.4%
 (0.5, 0.7]   (ok)         48    9.8%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    1    0.2%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.243   0.055   0.173    0.350  ...    41.0      49.0      22.0   1.11
pu         0.751   0.029   0.710    0.804  ...    12.0      14.0      49.0   1.12
mu         0.117   0.017   0.088    0.146  ...    13.0      12.0      39.0   1.16
mus        0.209   0.044   0.143    0.285  ...    21.0      29.0      59.0   1.07
gamma      0.306   0.046   0.252    0.404  ...   118.0     137.0      88.0   0.99
Is_begin  19.246  10.878   1.906   37.471  ...     9.0       7.0      59.0   1.26
Ia_begin  54.839  20.881  13.909   94.863  ...   111.0     106.0      60.0   1.00
E_begin   80.457  43.614   8.125  150.264  ...    65.0      59.0      65.0   1.02

[8 rows x 11 columns]