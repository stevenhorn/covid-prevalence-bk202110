0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -905.76    42.45
p_loo       42.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      450   91.5%
 (0.5, 0.7]   (ok)         35    7.1%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.258  0.053   0.152    0.330  ...    78.0      78.0      81.0   1.01
pu        0.778  0.020   0.752    0.820  ...   119.0      99.0      48.0   1.00
mu        0.130  0.026   0.077    0.173  ...    25.0      25.0      93.0   1.07
mus       0.192  0.037   0.136    0.264  ...    89.0      83.0      43.0   1.01
gamma     0.221  0.046   0.144    0.301  ...   102.0      96.0      49.0   0.98
Is_begin  0.735  0.746   0.008    2.279  ...    89.0      74.0      57.0   1.00
Ia_begin  1.219  1.211   0.028    3.224  ...   107.0      96.0      88.0   0.98
E_begin   0.528  0.673   0.008    1.872  ...    94.0      80.0      59.0   1.02

[8 rows x 11 columns]