0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -763.30    29.54
p_loo       27.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      464   94.3%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         1    0.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.050   0.164    0.335  ...    76.0      78.0      68.0   1.01
pu        0.809  0.018   0.781    0.839  ...    39.0      36.0      95.0   1.06
mu        0.131  0.025   0.085    0.173  ...    16.0      14.0      75.0   1.12
mus       0.174  0.031   0.131    0.239  ...    44.0      43.0      59.0   1.05
gamma     0.211  0.040   0.158    0.312  ...    83.0     114.0      54.0   1.01
Is_begin  0.863  0.814   0.017    2.311  ...   124.0     110.0      40.0   1.00
Ia_begin  1.882  1.731   0.013    4.513  ...    54.0      33.0      60.0   1.05
E_begin   0.774  0.907   0.012    2.732  ...    67.0      51.0      43.0   1.05

[8 rows x 11 columns]