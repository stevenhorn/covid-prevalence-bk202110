2 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2773.56    52.66
p_loo       38.70        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      446   90.7%
 (0.5, 0.7]   (ok)         37    7.5%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    4    0.8%

             mean       sd   hdi_3%  ...  ess_bulk  ess_tail  r_hat
pa          0.197    0.037    0.150  ...      71.0      59.0   1.04
pu          0.716    0.012    0.700  ...      76.0      54.0   1.03
mu          0.107    0.016    0.081  ...      14.0      59.0   1.11
mus         0.183    0.029    0.147  ...      65.0      43.0   1.01
gamma       0.325    0.047    0.248  ...      54.0      60.0   1.06
Is_begin  169.864  120.097    6.576  ...      42.0      14.0   1.05
Ia_begin  572.652  230.173  122.442  ...      14.0      52.0   1.11
E_begin   659.068  386.477  118.721  ...      46.0      65.0   1.05

[8 rows x 11 columns]