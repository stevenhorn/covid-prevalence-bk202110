0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1778.71    27.84
p_loo       31.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      445   90.4%
 (0.5, 0.7]   (ok)         39    7.9%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.057   0.166    0.346  ...    65.0      62.0      40.0   1.03
pu        0.775  0.045   0.700    0.832  ...    30.0      30.0      30.0   1.07
mu        0.137  0.021   0.098    0.175  ...    38.0      36.0      59.0   1.00
mus       0.180  0.030   0.138    0.248  ...   152.0     152.0      76.0   1.07
gamma     0.269  0.040   0.194    0.340  ...    92.0      95.0      61.0   1.01
Is_begin  0.917  0.596   0.144    2.190  ...    89.0      91.0      91.0   0.99
Ia_begin  1.621  1.221   0.008    4.028  ...    90.0      67.0      54.0   1.00
E_begin   2.038  1.802   0.040    5.560  ...    90.0      98.0      91.0   1.02

[8 rows x 11 columns]