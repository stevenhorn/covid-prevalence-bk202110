0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1261.29    84.98
p_loo       51.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      463   94.1%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.167  0.017   0.151    0.188  ...   111.0     105.0      60.0   1.08
pu        0.706  0.006   0.700    0.719  ...   124.0     132.0      59.0   1.02
mu        0.118  0.014   0.093    0.144  ...    38.0      28.0      59.0   1.10
mus       0.238  0.031   0.178    0.293  ...    84.0      96.0      40.0   1.01
gamma     0.565  0.093   0.382    0.691  ...   152.0     152.0      79.0   0.99
Is_begin  0.561  0.492   0.006    1.347  ...   126.0     115.0      93.0   0.99
Ia_begin  0.874  0.881   0.002    2.441  ...    71.0     152.0      42.0   1.04
E_begin   0.365  0.358   0.001    1.067  ...    76.0      42.0      37.0   1.01

[8 rows x 11 columns]