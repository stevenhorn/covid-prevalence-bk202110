0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -458.51    51.64
p_loo       42.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      472   95.9%
 (0.5, 0.7]   (ok)         14    2.8%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.051   0.160    0.324  ...    66.0      55.0      60.0   1.00
pu        0.819  0.016   0.788    0.847  ...    41.0      36.0      96.0   1.05
mu        0.125  0.023   0.078    0.166  ...    62.0      52.0      69.0   1.05
mus       0.173  0.034   0.105    0.222  ...    91.0      96.0      91.0   1.00
gamma     0.196  0.042   0.132    0.255  ...    38.0      52.0      33.0   1.02
Is_begin  0.307  0.390   0.001    1.158  ...    45.0      38.0      51.0   1.02
Ia_begin  0.552  0.834   0.011    2.251  ...    76.0      16.0      49.0   1.11
E_begin   0.287  0.512   0.000    0.923  ...    81.0      47.0      96.0   1.04

[8 rows x 11 columns]