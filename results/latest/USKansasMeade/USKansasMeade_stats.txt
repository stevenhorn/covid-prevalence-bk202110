0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -958.24    40.41
p_loo       42.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      454   92.3%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.202  0.038   0.152    0.263  ...    28.0      45.0      88.0   1.06
pu        0.720  0.014   0.700    0.743  ...    53.0      84.0      57.0   1.07
mu        0.118  0.031   0.071    0.178  ...     9.0       8.0      43.0   1.22
mus       0.208  0.036   0.157    0.270  ...    52.0      56.0      91.0   1.01
gamma     0.261  0.041   0.183    0.333  ...    65.0      67.0      33.0   1.02
Is_begin  0.482  0.505   0.004    1.331  ...    17.0      19.0      18.0   1.13
Ia_begin  0.865  0.951   0.025    2.445  ...    68.0      83.0      86.0   1.00
E_begin   0.343  0.318   0.008    0.919  ...    50.0      56.0      58.0   1.01

[8 rows x 11 columns]