0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -1522.18    35.43
p_loo       38.43        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.7%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.275  0.051   0.181    0.348  ...   138.0     123.0      49.0   1.03
pu        0.877  0.025   0.846    0.899  ...   101.0     109.0      61.0   1.00
mu        0.181  0.015   0.151    0.205  ...     8.0       8.0      44.0   1.23
mus       0.214  0.029   0.174    0.267  ...   152.0     152.0      70.0   1.00
gamma     0.324  0.050   0.242    0.419  ...    29.0      41.0      91.0   1.05
Is_begin  0.919  0.763   0.024    2.295  ...    92.0      68.0      60.0   1.00
Ia_begin  1.724  2.029   0.016    6.095  ...   134.0      55.0      22.0   1.00
E_begin   0.993  1.080   0.001    3.119  ...    74.0      72.0      22.0   1.03

[8 rows x 11 columns]