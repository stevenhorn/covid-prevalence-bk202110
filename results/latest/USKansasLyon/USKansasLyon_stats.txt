1 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1781.24    40.54
p_loo       39.56        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   93.3%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.181  0.026   0.151    0.229  ...   152.0     107.0      43.0   1.00
pu        0.711  0.010   0.700    0.730  ...   126.0     127.0      44.0   1.00
mu        0.115  0.019   0.085    0.146  ...    44.0      42.0      59.0   1.05
mus       0.183  0.036   0.125    0.253  ...    10.0      10.0      85.0   1.19
gamma     0.246  0.046   0.166    0.322  ...   109.0      99.0      60.0   0.98
Is_begin  0.987  0.863   0.029    2.775  ...    83.0      83.0      59.0   0.98
Ia_begin  1.814  2.289   0.025    5.832  ...    86.0      89.0      60.0   1.00
E_begin   1.147  1.349   0.002    3.454  ...   102.0      51.0      59.0   1.02

[8 rows x 11 columns]