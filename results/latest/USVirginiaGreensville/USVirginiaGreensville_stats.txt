0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1291.75    39.29
p_loo       47.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.200  0.039   0.152    0.273  ...    72.0      60.0      60.0   1.04
pu        0.722  0.014   0.702    0.746  ...    93.0     106.0      91.0   1.01
mu        0.170  0.017   0.150    0.215  ...    58.0      63.0      59.0   1.01
mus       0.267  0.036   0.194    0.328  ...   113.0     122.0      51.0   0.99
gamma     0.459  0.075   0.331    0.579  ...    59.0      76.0      60.0   1.02
Is_begin  0.653  0.575   0.033    1.931  ...   125.0     111.0      76.0   0.99
Ia_begin  1.378  1.477   0.000    4.871  ...    90.0      86.0      40.0   1.03
E_begin   0.581  0.688   0.020    1.898  ...   101.0     123.0      38.0   1.03

[8 rows x 11 columns]