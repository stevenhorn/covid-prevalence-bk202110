0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2486.87    53.40
p_loo       43.74        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      472   95.9%
 (0.5, 0.7]   (ok)         17    3.5%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.206  0.036   0.152    0.267  ...   152.0     152.0      17.0   1.02
pu        0.717  0.012   0.701    0.739  ...   126.0     120.0      80.0   1.01
mu        0.123  0.024   0.082    0.162  ...    41.0      42.0      60.0   1.03
mus       0.166  0.038   0.113    0.246  ...    76.0      77.0     100.0   1.00
gamma     0.202  0.040   0.130    0.269  ...    95.0      88.0      97.0   1.01
Is_begin  0.803  0.784   0.017    2.038  ...    96.0      95.0      74.0   0.99
Ia_begin  0.626  0.473   0.017    1.423  ...    68.0      42.0      58.0   1.03
E_begin   0.493  0.539   0.002    1.298  ...    90.0      54.0      59.0   1.03

[8 rows x 11 columns]