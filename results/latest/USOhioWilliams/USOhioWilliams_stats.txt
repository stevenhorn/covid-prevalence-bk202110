0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1345.55    69.74
p_loo       47.10        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      450   91.5%
 (0.5, 0.7]   (ok)         36    7.3%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.240  0.043   0.162    0.327  ...    37.0      38.0      36.0   1.05
pu        0.741  0.023   0.702    0.779  ...    67.0      66.0      59.0   1.00
mu        0.137  0.021   0.106    0.182  ...    25.0      24.0      59.0   1.08
mus       0.264  0.049   0.170    0.353  ...    37.0      41.0      60.0   1.05
gamma     0.348  0.048   0.249    0.411  ...   100.0      95.0     100.0   1.02
Is_begin  0.745  0.661   0.005    2.225  ...   127.0      89.0      86.0   1.01
Ia_begin  1.256  1.517   0.016    3.638  ...    89.0      73.0      40.0   1.00
E_begin   0.539  0.559   0.004    1.657  ...    31.0      61.0      46.0   1.01

[8 rows x 11 columns]