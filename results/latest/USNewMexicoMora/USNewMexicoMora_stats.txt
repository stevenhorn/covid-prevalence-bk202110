0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -497.37    59.10
p_loo       49.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      470   95.5%
 (0.5, 0.7]   (ok)         18    3.7%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.271  0.050   0.189    0.348  ...    53.0      49.0      58.0   1.01
pu        0.880  0.013   0.855    0.900  ...     9.0      10.0      38.0   1.17
mu        0.144  0.028   0.098    0.186  ...    11.0      13.0      56.0   1.13
mus       0.197  0.032   0.128    0.247  ...    30.0      30.0      23.0   1.05
gamma     0.228  0.042   0.154    0.302  ...    44.0      44.0      60.0   1.02
Is_begin  0.616  0.552   0.019    1.572  ...    78.0      54.0      60.0   0.99
Ia_begin  0.971  1.020   0.003    3.118  ...    52.0      51.0      91.0   1.01
E_begin   0.515  0.520   0.013    1.713  ...    64.0      71.0      60.0   1.00

[8 rows x 11 columns]