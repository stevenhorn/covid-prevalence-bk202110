0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2224.37    33.13
p_loo       30.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      445   90.4%
 (0.5, 0.7]   (ok)         37    7.5%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.049   0.153    0.323  ...   152.0     152.0      69.0   1.03
pu        0.742  0.025   0.702    0.777  ...   124.0     110.0      81.0   1.01
mu        0.124  0.022   0.088    0.161  ...    38.0      37.0      56.0   1.05
mus       0.173  0.031   0.120    0.223  ...   152.0     152.0      72.0   1.12
gamma     0.206  0.040   0.144    0.287  ...    84.0      98.0      91.0   0.99
Is_begin  0.776  0.716   0.000    2.016  ...   107.0      73.0      24.0   1.04
Ia_begin  0.620  0.541   0.033    1.633  ...    95.0      58.0      22.0   1.01
E_begin   0.448  0.434   0.008    1.355  ...   107.0      68.0      80.0   1.04

[8 rows x 11 columns]