0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1352.11    35.50
p_loo       36.42        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      437   88.8%
 (0.5, 0.7]   (ok)         42    8.5%
   (0.7, 1]   (bad)        11    2.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.200  0.032   0.151    0.260  ...    80.0      88.0      60.0   1.01
pu        0.715  0.011   0.700    0.737  ...   124.0     118.0      66.0   1.02
mu        0.133  0.017   0.107    0.168  ...    24.0      29.0      22.0   1.07
mus       0.200  0.034   0.150    0.273  ...    76.0      82.0      93.0   1.01
gamma     0.302  0.045   0.209    0.374  ...    56.0      59.0      59.0   1.01
Is_begin  0.482  0.537   0.007    1.832  ...    71.0      80.0      61.0   1.04
Ia_begin  0.786  0.835   0.012    2.119  ...   117.0     103.0      59.0   0.99
E_begin   0.249  0.288   0.001    0.748  ...    84.0      64.0      88.0   1.05

[8 rows x 11 columns]