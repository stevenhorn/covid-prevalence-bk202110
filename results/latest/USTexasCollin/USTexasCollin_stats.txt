0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2933.89    59.60
p_loo       54.12        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         26    5.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    5    1.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.048   0.164    0.327  ...    39.0      40.0      56.0   1.04
pu        0.765  0.032   0.703    0.809  ...    58.0      66.0      97.0   0.99
mu        0.116  0.022   0.081    0.156  ...    25.0      23.0      40.0   1.06
mus       0.201  0.033   0.151    0.257  ...   122.0     134.0     100.0   0.98
gamma     0.240  0.044   0.170    0.317  ...    83.0      64.0      59.0   1.00
Is_begin  3.555  2.759   0.356    8.771  ...    68.0      55.0      93.0   1.03
Ia_begin  7.199  5.607   0.308   18.018  ...    70.0      55.0      53.0   1.02
E_begin   3.539  3.378   0.008   10.095  ...    74.0      26.0      14.0   1.03

[8 rows x 11 columns]