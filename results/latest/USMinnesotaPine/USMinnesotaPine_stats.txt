0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1412.80    48.02
p_loo       48.79        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      454   92.3%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.235  0.051   0.153    0.328  ...    95.0      94.0      59.0   1.03
pu        0.746  0.022   0.702    0.780  ...    53.0      52.0      14.0   1.00
mu        0.143  0.023   0.098    0.183  ...    68.0      62.0     100.0   1.02
mus       0.194  0.030   0.147    0.254  ...   152.0     152.0     100.0   1.02
gamma     0.287  0.041   0.207    0.358  ...   152.0     152.0     100.0   0.99
Is_begin  0.575  0.596   0.013    1.840  ...    87.0      36.0      24.0   1.05
Ia_begin  1.363  1.137   0.112    3.825  ...    76.0      66.0      59.0   0.99
E_begin   0.526  0.565   0.006    1.429  ...    79.0      59.0      32.0   1.00

[8 rows x 11 columns]