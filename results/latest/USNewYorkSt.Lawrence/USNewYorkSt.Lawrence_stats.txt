0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1371.39    36.29
p_loo       37.83        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      446   90.7%
 (0.5, 0.7]   (ok)         41    8.3%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.251  0.055   0.167    0.343  ...    62.0      62.0      43.0   1.06
pu        0.851  0.027   0.786    0.886  ...    29.0      21.0      43.0   1.06
mu        0.154  0.029   0.103    0.204  ...    42.0      39.0      32.0   1.05
mus       0.228  0.042   0.156    0.311  ...   127.0     152.0      38.0   1.02
gamma     0.327  0.053   0.251    0.428  ...   115.0     135.0      47.0   1.01
Is_begin  1.664  1.413   0.031    4.619  ...    85.0      26.0      40.0   1.07
Ia_begin  5.822  3.366   0.083   11.470  ...    38.0      38.0      56.0   1.05
E_begin   5.550  4.333   0.026   14.300  ...   133.0     107.0      57.0   1.00

[8 rows x 11 columns]