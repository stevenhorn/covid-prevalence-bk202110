0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -617.59    46.83
p_loo       41.09        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      461   93.7%
 (0.5, 0.7]   (ok)         23    4.7%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.243  0.055   0.156    0.334  ...    68.0      73.0      60.0   0.99
pu        0.799  0.017   0.771    0.834  ...    98.0      96.0      74.0   1.02
mu        0.113  0.020   0.084    0.152  ...    75.0      97.0      49.0   0.99
mus       0.180  0.036   0.120    0.238  ...   152.0     152.0      75.0   1.00
gamma     0.211  0.042   0.144    0.290  ...   149.0     152.0      58.0   0.99
Is_begin  0.276  0.323   0.006    0.815  ...    95.0     118.0      67.0   1.02
Ia_begin  0.389  0.582   0.000    1.399  ...    97.0     152.0      72.0   1.02
E_begin   0.207  0.244   0.005    0.596  ...    99.0     152.0      52.0   1.01

[8 rows x 11 columns]