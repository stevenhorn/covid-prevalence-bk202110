0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -554.74    37.32
p_loo       29.23        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      470   95.5%
 (0.5, 0.7]   (ok)         17    3.5%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.045   0.182    0.330  ...    67.0      67.0      43.0   1.00
pu        0.812  0.018   0.782    0.842  ...    19.0      25.0      59.0   1.07
mu        0.120  0.022   0.087    0.173  ...    48.0      57.0      60.0   1.00
mus       0.175  0.026   0.136    0.233  ...    87.0      82.0      92.0   1.03
gamma     0.208  0.038   0.141    0.275  ...    80.0      87.0      57.0   1.01
Is_begin  0.429  0.473   0.001    1.287  ...   104.0      90.0      43.0   0.98
Ia_begin  0.769  0.876   0.001    2.407  ...    62.0      26.0      58.0   1.06
E_begin   0.330  0.375   0.001    1.166  ...    29.0      23.0      46.0   1.09

[8 rows x 11 columns]