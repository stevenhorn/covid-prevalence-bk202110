0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1256.86    34.23
p_loo       37.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.7%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.060   0.150    0.337  ...    29.0      33.0      34.0   1.04
pu        0.738  0.020   0.702    0.769  ...    37.0      41.0      59.0   1.04
mu        0.136  0.019   0.108    0.173  ...    35.0      35.0      60.0   1.02
mus       0.195  0.028   0.144    0.240  ...    62.0      74.0      60.0   1.05
gamma     0.260  0.040   0.198    0.328  ...   138.0     131.0     100.0   1.00
Is_begin  0.495  0.503   0.000    1.209  ...    53.0      55.0      59.0   1.02
Ia_begin  0.908  0.985   0.028    2.339  ...    93.0     114.0      65.0   1.05
E_begin   0.428  0.491   0.007    1.499  ...    58.0      54.0      57.0   0.99

[8 rows x 11 columns]