0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -799.10    31.17
p_loo       27.69        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   93.3%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.049   0.161    0.331  ...    55.0      50.0      30.0   1.03
pu        0.779  0.019   0.749    0.815  ...    14.0      14.0      59.0   1.11
mu        0.129  0.026   0.082    0.174  ...    13.0      12.0      61.0   1.14
mus       0.168  0.030   0.111    0.219  ...   130.0     152.0      77.0   1.00
gamma     0.189  0.038   0.119    0.248  ...   152.0     152.0      80.0   1.01
Is_begin  0.660  0.829   0.002    1.604  ...   101.0      90.0      42.0   1.00
Ia_begin  1.113  1.366   0.008    2.902  ...    68.0      40.0      40.0   1.05
E_begin   0.512  0.608   0.000    1.649  ...    94.0      73.0      60.0   0.99

[8 rows x 11 columns]