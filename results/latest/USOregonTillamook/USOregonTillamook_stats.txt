0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -831.48    29.37
p_loo       31.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      463   94.1%
 (0.5, 0.7]   (ok)         23    4.7%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.266  0.047   0.192    0.341  ...   132.0     129.0      30.0   1.00
pu        0.873  0.019   0.836    0.899  ...   109.0     142.0      80.0   1.01
mu        0.168  0.024   0.127    0.212  ...    43.0      44.0      39.0   1.03
mus       0.182  0.037   0.117    0.252  ...   131.0     152.0      72.0   0.99
gamma     0.243  0.046   0.156    0.316  ...   152.0     152.0      25.0   1.06
Is_begin  1.001  0.853   0.043    2.727  ...   152.0     152.0      97.0   1.02
Ia_begin  2.068  1.562   0.053    5.336  ...    93.0      71.0      43.0   1.00
E_begin   1.083  0.904   0.000    2.741  ...    61.0      47.0      22.0   1.03

[8 rows x 11 columns]