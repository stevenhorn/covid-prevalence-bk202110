0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -847.53    32.14
p_loo       42.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      450   91.5%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.050   0.165    0.339  ...    58.0      62.0      60.0   1.01
pu        0.769  0.036   0.707    0.827  ...    11.0      12.0      59.0   1.13
mu        0.162  0.026   0.114    0.213  ...    29.0      25.0      15.0   1.01
mus       0.220  0.035   0.162    0.293  ...    77.0      74.0      49.0   1.01
gamma     0.282  0.049   0.177    0.370  ...    96.0      92.0      96.0   0.99
Is_begin  0.383  0.397   0.003    0.928  ...    89.0      40.0      65.0   1.07
Ia_begin  0.614  0.755   0.000    2.157  ...    79.0      37.0      17.0   1.00
E_begin   0.321  0.458   0.005    1.097  ...    91.0      43.0      91.0   1.02

[8 rows x 11 columns]