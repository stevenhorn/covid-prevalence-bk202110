0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1869.91    82.99
p_loo       60.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      450   91.5%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)        10    2.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.174  0.018   0.152    0.211  ...   118.0      99.0      60.0   1.00
pu        0.709  0.007   0.701    0.723  ...   152.0     152.0      60.0   1.01
mu        0.136  0.015   0.110    0.162  ...    29.0      28.0      40.0   1.14
mus       0.290  0.033   0.234    0.342  ...    72.0      71.0      77.0   0.99
gamma     0.565  0.079   0.428    0.721  ...   114.0     111.0      76.0   1.01
Is_begin  0.825  0.683   0.006    2.050  ...   102.0      88.0      91.0   1.01
Ia_begin  1.405  1.658   0.016    4.438  ...    41.0      26.0      43.0   1.06
E_begin   0.647  0.825   0.030    1.969  ...    56.0      98.0      77.0   0.99

[8 rows x 11 columns]