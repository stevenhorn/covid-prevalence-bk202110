0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1839.86    58.26
p_loo       53.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      471   95.7%
 (0.5, 0.7]   (ok)         17    3.5%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.207  0.037   0.152    0.265  ...   152.0     152.0      81.0   1.00
pu        0.719  0.012   0.700    0.737  ...    86.0      98.0      88.0   1.02
mu        0.130  0.024   0.082    0.162  ...   112.0     106.0      79.0   1.00
mus       0.199  0.036   0.137    0.270  ...   152.0     136.0      91.0   1.03
gamma     0.262  0.049   0.172    0.346  ...   152.0     152.0      70.0   0.98
Is_begin  0.559  0.556   0.001    1.326  ...   101.0      69.0      40.0   1.04
Ia_begin  0.916  0.887   0.031    3.144  ...    85.0      91.0      56.0   1.02
E_begin   0.481  0.547   0.002    1.344  ...    89.0     108.0      59.0   1.00

[8 rows x 11 columns]