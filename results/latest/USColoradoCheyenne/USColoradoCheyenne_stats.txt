0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -405.55    47.15
p_loo       45.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      446   90.7%
 (0.5, 0.7]   (ok)         35    7.1%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.262  0.058   0.154    0.336  ...   127.0     123.0      56.0   1.00
pu        0.838  0.019   0.811    0.870  ...    85.0      89.0      88.0   1.02
mu        0.160  0.031   0.108    0.214  ...    49.0      59.0      56.0   1.02
mus       0.227  0.036   0.176    0.306  ...   152.0     152.0      47.0   1.03
gamma     0.316  0.058   0.210    0.424  ...   152.0     152.0      60.0   1.04
Is_begin  0.703  0.698   0.014    2.218  ...    84.0     152.0      59.0   0.99
Ia_begin  0.928  0.757   0.026    2.374  ...    46.0      39.0      57.0   1.09
E_begin   0.573  0.587   0.026    2.048  ...    73.0      59.0      83.0   0.99

[8 rows x 11 columns]