0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1482.53    61.83
p_loo       55.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      451   91.7%
 (0.5, 0.7]   (ok)         33    6.7%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.237  0.056   0.150    0.331  ...    52.0      43.0      33.0   1.04
pu        0.721  0.021   0.700    0.757  ...    19.0      24.0      60.0   1.08
mu        0.140  0.018   0.109    0.175  ...     8.0       8.0      15.0   1.21
mus       0.276  0.044   0.188    0.347  ...    80.0      85.0      30.0   0.99
gamma     0.390  0.073   0.258    0.514  ...    90.0     121.0      34.0   1.00
Is_begin  1.012  0.798   0.035    2.284  ...   116.0     152.0      86.0   0.98
Ia_begin  1.606  1.457   0.039    4.610  ...   108.0      88.0      58.0   1.00
E_begin   0.678  0.677   0.005    1.873  ...   102.0      76.0      57.0   1.00

[8 rows x 11 columns]