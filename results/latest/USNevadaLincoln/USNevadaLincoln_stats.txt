0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -945.37    52.47
p_loo       46.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      466   94.7%
 (0.5, 0.7]   (ok)         20    4.1%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.059   0.168    0.350  ...    46.0      57.0      93.0   1.01
pu        0.758  0.031   0.711    0.805  ...    21.0      22.0      60.0   1.10
mu        0.135  0.026   0.086    0.178  ...    23.0      23.0      20.0   1.07
mus       0.227  0.033   0.173    0.278  ...    90.0      95.0      74.0   1.04
gamma     0.312  0.063   0.214    0.441  ...   120.0     152.0      56.0   1.12
Is_begin  0.579  0.592   0.036    1.581  ...   105.0      39.0     100.0   1.06
Ia_begin  1.012  0.932   0.042    3.268  ...    76.0      62.0      93.0   1.00
E_begin   0.469  0.401   0.001    1.196  ...    44.0      15.0      57.0   1.10

[8 rows x 11 columns]