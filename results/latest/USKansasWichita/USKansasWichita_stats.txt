0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -668.54    47.44
p_loo       48.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.053   0.172    0.347  ...   106.0     109.0      60.0   1.09
pu        0.805  0.022   0.764    0.843  ...    97.0     111.0      24.0   1.10
mu        0.125  0.021   0.085    0.162  ...    24.0      29.0      60.0   1.05
mus       0.217  0.037   0.155    0.285  ...    76.0      96.0      60.0   1.00
gamma     0.254  0.039   0.162    0.314  ...   137.0     142.0      69.0   1.01
Is_begin  0.365  0.500   0.000    1.260  ...    74.0      42.0      24.0   1.03
Ia_begin  0.649  0.728   0.001    2.339  ...    76.0      60.0      74.0   1.02
E_begin   0.260  0.354   0.000    1.011  ...    62.0      60.0      60.0   0.98

[8 rows x 11 columns]