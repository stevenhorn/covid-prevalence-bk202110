0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1608.04    95.88
p_loo       65.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      473   96.1%
 (0.5, 0.7]   (ok)         13    2.6%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.161  0.012   0.150    0.179  ...   152.0     152.0      55.0   1.03
pu        0.704  0.004   0.700    0.711  ...    47.0      76.0      59.0   1.05
mu        0.111  0.016   0.079    0.138  ...    30.0      30.0      43.0   1.04
mus       0.297  0.045   0.202    0.368  ...    47.0      77.0      24.0   1.06
gamma     0.652  0.098   0.480    0.812  ...   133.0     117.0      60.0   1.00
Is_begin  0.803  0.561   0.014    1.762  ...    90.0      70.0      56.0   1.04
Ia_begin  1.294  1.380   0.042    3.816  ...   111.0     125.0      57.0   1.00
E_begin   0.587  0.607   0.008    1.540  ...    90.0      55.0      58.0   0.99

[8 rows x 11 columns]