0 Divergences 
Failed validation 
2021-07-21 date of last case count
Computed from 80 by 494 log-likelihood matrix

         Estimate       SE
elpd_loo -1158.36    90.86
p_loo       61.65        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      465   94.1%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.015   0.151    0.198  ...    80.0      70.0      74.0   1.03
pu        0.706  0.007   0.700    0.720  ...    83.0      67.0      60.0   1.03
mu        0.116  0.015   0.088    0.142  ...     7.0       7.0      18.0   1.24
mus       0.281  0.035   0.221    0.338  ...    66.0      63.0      38.0   1.04
gamma     0.659  0.088   0.489    0.823  ...    59.0      66.0      59.0   1.09
Is_begin  0.715  0.646   0.013    2.203  ...    73.0      71.0      59.0   1.05
Ia_begin  0.940  0.939   0.021    2.799  ...    74.0      79.0      96.0   1.01
E_begin   0.371  0.383   0.002    1.122  ...    30.0      41.0      55.0   1.03

[8 rows x 11 columns]