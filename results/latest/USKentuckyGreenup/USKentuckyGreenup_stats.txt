0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1449.05    56.11
p_loo       43.66        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      464   94.3%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.225  0.050   0.153    0.312  ...    40.0      41.0      59.0   1.07
pu        0.731  0.019   0.705    0.771  ...   152.0     152.0      97.0   1.02
mu        0.111  0.019   0.078    0.150  ...    37.0      26.0      43.0   1.06
mus       0.180  0.030   0.130    0.236  ...    99.0     102.0     100.0   1.03
gamma     0.237  0.048   0.161    0.321  ...   152.0     152.0      72.0   1.01
Is_begin  0.619  0.592   0.009    1.747  ...   132.0      72.0      59.0   1.02
Ia_begin  1.247  1.673   0.003    4.145  ...    99.0     101.0     100.0   0.99
E_begin   0.424  0.522   0.000    1.204  ...    82.0      38.0      39.0   1.01

[8 rows x 11 columns]