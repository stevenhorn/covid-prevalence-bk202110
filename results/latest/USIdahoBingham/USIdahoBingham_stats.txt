0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1523.14    40.35
p_loo       34.17        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.053   0.151    0.325  ...    62.0      60.0      57.0   1.03
pu        0.730  0.020   0.701    0.772  ...    26.0      22.0      57.0   1.10
mu        0.114  0.021   0.079    0.149  ...    20.0      19.0      59.0   1.08
mus       0.180  0.037   0.121    0.244  ...    45.0      55.0      60.0   1.02
gamma     0.226  0.047   0.146    0.303  ...    55.0      56.0     100.0   1.03
Is_begin  0.751  0.776   0.017    2.072  ...   102.0      19.0      19.0   1.09
Ia_begin  0.550  0.465   0.002    1.589  ...    29.0      20.0      39.0   1.08
E_begin   0.458  0.545   0.003    1.531  ...    47.0      27.0      30.0   1.06

[8 rows x 11 columns]