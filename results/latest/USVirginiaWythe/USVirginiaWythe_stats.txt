0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1346.05    42.48
p_loo       41.57        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   93.3%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.054   0.151    0.335  ...    37.0      32.0      22.0   1.06
pu        0.841  0.016   0.813    0.870  ...     7.0       7.0      30.0   1.30
mu        0.111  0.019   0.079    0.146  ...    14.0      12.0      30.0   1.12
mus       0.176  0.036   0.107    0.234  ...    73.0      81.0      95.0   1.02
gamma     0.205  0.034   0.161    0.267  ...    48.0      57.0      81.0   1.05
Is_begin  0.720  0.534   0.019    1.794  ...    36.0      35.0      36.0   1.07
Ia_begin  1.746  1.713   0.007    5.013  ...    26.0      26.0      69.0   1.05
E_begin   0.729  0.623   0.006    1.913  ...    45.0      38.0      32.0   1.01

[8 rows x 11 columns]