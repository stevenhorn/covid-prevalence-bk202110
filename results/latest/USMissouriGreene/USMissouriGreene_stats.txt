0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2556.52    92.30
p_loo       69.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   93.3%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.014   0.150    0.195  ...    25.0      19.0      54.0   1.09
pu        0.705  0.005   0.700    0.715  ...    17.0      18.0      59.0   1.11
mu        0.103  0.013   0.080    0.128  ...     5.0       5.0      22.0   1.48
mus       0.289  0.043   0.227    0.372  ...    13.0      13.0      48.0   1.13
gamma     0.580  0.095   0.437    0.768  ...    16.0      16.0      95.0   1.11
Is_begin  0.849  0.826   0.013    2.644  ...    82.0      61.0      59.0   1.03
Ia_begin  1.335  1.271   0.022    4.171  ...    80.0      83.0      75.0   1.02
E_begin   0.653  0.690   0.009    1.897  ...    63.0      49.0      59.0   1.03

[8 rows x 11 columns]