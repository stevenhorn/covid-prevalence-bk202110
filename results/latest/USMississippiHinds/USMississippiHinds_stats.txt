0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2225.11    32.48
p_loo       39.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      468   95.1%
 (0.5, 0.7]   (ok)         18    3.7%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.201  0.036   0.153    0.273  ...   152.0     152.0      96.0   0.98
pu        0.716  0.014   0.700    0.742  ...    94.0      91.0      59.0   1.04
mu        0.095  0.014   0.070    0.120  ...    29.0      31.0      42.0   1.00
mus       0.167  0.026   0.129    0.221  ...    67.0      86.0      91.0   1.07
gamma     0.260  0.038   0.196    0.326  ...    79.0     152.0      56.0   1.01
Is_begin  1.595  0.948   0.277    3.413  ...    99.0      83.0      59.0   1.01
Ia_begin  2.565  2.319   0.006    6.775  ...     8.0       5.0      22.0   1.38
E_begin   2.078  2.024   0.068    6.120  ...    11.0       6.0      34.0   1.32

[8 rows x 11 columns]