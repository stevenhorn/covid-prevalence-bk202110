0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1394.10    55.91
p_loo       39.80        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      447   90.9%
 (0.5, 0.7]   (ok)         38    7.7%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.051   0.174    0.349  ...    61.0      59.0      43.0   1.01
pu        0.851  0.014   0.826    0.874  ...    61.0      58.0      59.0   1.04
mu        0.122  0.017   0.088    0.149  ...    53.0      53.0      60.0   1.04
mus       0.185  0.036   0.136    0.283  ...   141.0     152.0      60.0   1.05
gamma     0.202  0.041   0.143    0.299  ...   152.0     152.0      57.0   1.01
Is_begin  1.009  0.803   0.036    2.457  ...   144.0     152.0      96.0   1.01
Ia_begin  1.774  1.768   0.025    5.143  ...   106.0     123.0      86.0   0.98
E_begin   0.858  1.001   0.023    2.758  ...   104.0      25.0      59.0   1.07

[8 rows x 11 columns]