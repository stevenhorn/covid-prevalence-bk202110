0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1645.69    59.59
p_loo       52.73        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      466   94.7%
 (0.5, 0.7]   (ok)         22    4.5%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.178  0.022   0.151    0.227  ...   127.0     122.0      59.0   1.00
pu        0.709  0.009   0.700    0.726  ...   132.0      65.0      31.0   1.00
mu        0.130  0.018   0.100    0.161  ...    17.0      16.0      74.0   1.11
mus       0.201  0.037   0.132    0.258  ...    24.0      30.0      60.0   1.11
gamma     0.263  0.042   0.174    0.321  ...   115.0      73.0      72.0   1.02
Is_begin  0.579  0.627   0.016    1.682  ...    45.0     111.0      33.0   1.01
Ia_begin  0.935  0.841   0.010    2.382  ...   100.0      72.0      59.0   1.04
E_begin   0.500  0.478   0.007    1.499  ...    80.0      65.0     100.0   1.01

[8 rows x 11 columns]