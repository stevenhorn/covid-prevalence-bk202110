0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1184.67    34.40
p_loo       41.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      459   93.3%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.048   0.171    0.333  ...    66.0      72.0      87.0   1.01
pu        0.883  0.013   0.860    0.900  ...     8.0       9.0      26.0   1.21
mu        0.149  0.021   0.116    0.192  ...    19.0      21.0      56.0   1.09
mus       0.210  0.034   0.147    0.267  ...   138.0     152.0      96.0   1.00
gamma     0.288  0.051   0.211    0.391  ...   152.0     152.0      59.0   1.06
Is_begin  1.094  0.893   0.047    2.730  ...    99.0     152.0      73.0   1.03
Ia_begin  2.690  2.748   0.007    8.386  ...   128.0     152.0      60.0   0.98
E_begin   1.498  1.544   0.023    4.482  ...    75.0      48.0      50.0   1.02

[8 rows x 11 columns]