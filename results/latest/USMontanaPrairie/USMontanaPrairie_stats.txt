0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -502.62    58.99
p_loo       53.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      458   93.1%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.048   0.156    0.316  ...    93.0      88.0      93.0   0.99
pu        0.757  0.022   0.721    0.794  ...    59.0      61.0      77.0   1.06
mu        0.145  0.034   0.090    0.211  ...     7.0       8.0      22.0   1.25
mus       0.268  0.043   0.204    0.359  ...    68.0      79.0      61.0   1.05
gamma     0.353  0.053   0.274    0.466  ...    57.0      57.0      60.0   1.01
Is_begin  0.303  0.492   0.000    1.252  ...    76.0      45.0      26.0   1.00
Ia_begin  0.390  0.594   0.000    1.614  ...    59.0      44.0      93.0   1.00
E_begin   0.233  0.399   0.000    0.756  ...    71.0      55.0      60.0   0.99

[8 rows x 11 columns]