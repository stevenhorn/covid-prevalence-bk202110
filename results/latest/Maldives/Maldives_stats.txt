0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -2374.69    41.40
p_loo       49.85        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      434   88.0%
 (0.5, 0.7]   (ok)         46    9.3%
   (0.7, 1]   (bad)        11    2.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.053   0.150    0.317  ...    92.0      88.0      57.0   1.08
pu        0.747  0.024   0.704    0.784  ...    35.0      34.0      38.0   1.04
mu        0.126  0.021   0.085    0.158  ...    28.0      29.0      14.0   1.05
mus       0.251  0.036   0.195    0.316  ...    76.0      89.0     100.0   1.02
gamma     0.321  0.048   0.242    0.414  ...    78.0     102.0      60.0   1.00
Is_begin  0.509  0.432   0.035    1.408  ...    94.0      67.0      30.0   1.01
Ia_begin  0.812  0.790   0.016    2.259  ...    81.0      55.0      79.0   1.03
E_begin   0.401  0.399   0.002    1.223  ...    97.0      59.0      95.0   1.02

[8 rows x 11 columns]