0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1126.63    33.53
p_loo       27.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      449   91.3%
 (0.5, 0.7]   (ok)         34    6.9%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.056   0.154    0.332  ...    66.0      53.0      18.0   1.08
pu        0.810  0.020   0.773    0.850  ...    21.0      24.0      80.0   1.07
mu        0.128  0.026   0.097    0.189  ...    39.0      34.0      58.0   1.06
mus       0.162  0.030   0.106    0.218  ...   118.0     146.0      96.0   1.06
gamma     0.174  0.040   0.114    0.248  ...    60.0      82.0      42.0   1.01
Is_begin  0.692  0.584   0.018    1.937  ...    90.0      55.0      42.0   1.02
Ia_begin  1.942  1.875   0.018    4.360  ...    72.0     147.0      80.0   1.04
E_begin   0.728  0.848   0.024    2.185  ...    77.0      85.0      60.0   0.98

[8 rows x 11 columns]