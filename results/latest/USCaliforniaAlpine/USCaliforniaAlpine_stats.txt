0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -453.05    70.14
p_loo       64.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      463   94.1%
 (0.5, 0.7]   (ok)         20    4.1%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    6    1.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.271  0.043   0.204    0.345  ...    47.0      48.0      24.0   1.02
pu        0.842  0.016   0.814    0.872  ...    48.0      49.0      59.0   1.04
mu        0.149  0.029   0.103    0.205  ...    25.0      27.0      35.0   1.06
mus       0.196  0.042   0.124    0.273  ...    31.0      27.0      30.0   1.03
gamma     0.220  0.048   0.129    0.289  ...    31.0      30.0      60.0   1.07
Is_begin  0.725  0.696   0.006    2.201  ...    45.0      16.0      48.0   1.10
Ia_begin  1.361  1.531   0.010    3.564  ...    72.0      46.0      60.0   1.01
E_begin   0.674  0.902   0.003    2.609  ...    52.0      76.0      21.0   1.01

[8 rows x 11 columns]