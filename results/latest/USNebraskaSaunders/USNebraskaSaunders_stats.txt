0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1231.26    46.31
p_loo       43.81        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.7%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.231  0.041   0.172    0.316  ...    59.0      60.0      99.0   0.98
pu        0.729  0.021   0.701    0.763  ...    78.0      52.0      36.0   1.00
mu        0.128  0.024   0.090    0.177  ...    26.0      26.0      32.0   1.05
mus       0.202  0.033   0.158    0.271  ...    81.0      75.0      59.0   1.02
gamma     0.258  0.044   0.190    0.338  ...    90.0     106.0      93.0   1.00
Is_begin  0.629  0.608   0.036    1.549  ...   113.0      91.0      69.0   1.00
Ia_begin  1.287  1.200   0.060    3.995  ...   130.0     152.0      88.0   1.02
E_begin   0.479  0.465   0.003    1.271  ...   109.0      83.0      88.0   1.01

[8 rows x 11 columns]