0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1698.30    33.66
p_loo       31.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.9%
 (0.5, 0.7]   (ok)         33    6.7%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.055   0.151    0.342  ...    98.0     124.0      59.0   1.06
pu        0.765  0.025   0.710    0.806  ...    71.0      74.0      57.0   0.99
mu        0.138  0.022   0.108    0.182  ...     7.0       7.0      23.0   1.23
mus       0.190  0.033   0.150    0.263  ...    75.0     110.0      59.0   1.02
gamma     0.304  0.049   0.223    0.378  ...    50.0      42.0      73.0   1.00
Is_begin  1.535  1.275   0.104    3.826  ...    76.0      49.0      49.0   1.01
Ia_begin  4.950  3.309   0.568   10.645  ...    81.0      70.0      60.0   1.00
E_begin   3.225  3.308   0.097   10.057  ...    61.0      54.0      83.0   1.03

[8 rows x 11 columns]