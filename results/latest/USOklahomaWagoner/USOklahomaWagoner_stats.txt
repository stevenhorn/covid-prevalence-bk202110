0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1851.28    36.80
p_loo       39.22        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      462   93.9%
 (0.5, 0.7]   (ok)         21    4.3%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.231  0.051   0.150    0.309  ...   127.0     117.0      39.0   1.05
pu        0.733  0.022   0.702    0.772  ...   109.0     101.0      43.0   1.00
mu        0.137  0.019   0.106    0.171  ...    30.0      22.0      48.0   1.09
mus       0.180  0.028   0.138    0.231  ...   138.0     105.0      88.0   1.05
gamma     0.241  0.040   0.174    0.313  ...   108.0     104.0      79.0   0.98
Is_begin  1.306  0.925   0.075    3.171  ...    92.0      83.0      59.0   1.03
Ia_begin  2.710  2.409   0.030    7.216  ...   118.0     102.0      88.0   1.01
E_begin   1.562  1.510   0.040    4.034  ...    96.0      88.0      60.0   1.03

[8 rows x 11 columns]