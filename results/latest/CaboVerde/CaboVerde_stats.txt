0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -2216.51    38.48
p_loo       35.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      458   92.9%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.265  0.049   0.188    0.345  ...    83.0      78.0      79.0   0.99
pu        0.856  0.033   0.789    0.899  ...     8.0      10.0      33.0   1.18
mu        0.115  0.021   0.082    0.154  ...     7.0       9.0      24.0   1.19
mus       0.164  0.032   0.105    0.211  ...   115.0     133.0      52.0   1.00
gamma     0.200  0.030   0.137    0.240  ...    79.0      68.0      77.0   1.00
Is_begin  0.979  0.886   0.002    2.827  ...   124.0     104.0      44.0   1.00
Ia_begin  2.646  2.484   0.030    7.613  ...    86.0      48.0      55.0   1.00
E_begin   1.629  1.872   0.004    4.748  ...    88.0      66.0      91.0   1.01

[8 rows x 11 columns]