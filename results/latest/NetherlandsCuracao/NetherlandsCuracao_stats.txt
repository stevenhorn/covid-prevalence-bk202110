0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 493 log-likelihood matrix

         Estimate       SE
elpd_loo -1543.05    53.30
p_loo       46.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      452   91.7%
 (0.5, 0.7]   (ok)         28    5.7%
   (0.7, 1]   (bad)        11    2.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.168  0.015   0.150    0.196  ...   152.0     152.0      96.0   0.99
pu        0.706  0.005   0.700    0.716  ...   137.0     110.0      60.0   1.01
mu        0.148  0.013   0.125    0.171  ...    65.0      59.0      59.0   1.02
mus       0.209  0.019   0.175    0.242  ...   152.0     152.0      70.0   1.00
gamma     0.557  0.070   0.432    0.678  ...   152.0     152.0      37.0   0.99
Is_begin  0.837  0.758   0.003    2.395  ...   128.0     152.0      76.0   0.98
Ia_begin  0.631  0.555   0.001    1.613  ...   152.0     100.0      24.0   0.99
E_begin   0.418  0.493   0.002    1.378  ...   100.0      72.0      40.0   1.01

[8 rows x 11 columns]