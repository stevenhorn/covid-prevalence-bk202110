1 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -899.35    28.96
p_loo       27.54        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      458   93.1%
 (0.5, 0.7]   (ok)         26    5.3%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.299  0.037   0.229    0.349  ...    95.0     106.0      80.0   1.02
pu        0.893  0.007   0.881    0.899  ...   138.0     125.0      60.0   1.00
mu        0.162  0.026   0.122    0.211  ...    16.0      17.0      14.0   1.05
mus       0.177  0.027   0.129    0.223  ...   152.0     152.0      58.0   1.01
gamma     0.227  0.038   0.165    0.296  ...   102.0     123.0      83.0   0.98
Is_begin  0.675  0.549   0.007    1.716  ...   127.0     112.0      96.0   0.99
Ia_begin  1.247  1.123   0.066    3.574  ...   137.0      88.0      81.0   1.05
E_begin   0.685  0.747   0.004    2.407  ...    99.0      43.0      39.0   1.04

[8 rows x 11 columns]