0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1935.06    31.70
p_loo       35.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      425   86.4%
 (0.5, 0.7]   (ok)         49   10.0%
   (0.7, 1]   (bad)        17    3.5%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.244  0.049   0.170    0.340  ...    53.0      56.0      37.0   1.05
pu        0.759  0.027   0.712    0.801  ...    22.0      21.0      35.0   1.04
mu        0.111  0.025   0.072    0.152  ...     9.0       8.0      59.0   1.21
mus       0.177  0.034   0.123    0.243  ...   112.0     142.0      86.0   1.00
gamma     0.229  0.037   0.179    0.290  ...   152.0     152.0      97.0   0.99
Is_begin  0.987  1.101   0.000    2.626  ...   115.0      44.0      22.0   1.04
Ia_begin  2.911  2.182   0.350    8.307  ...   100.0      75.0      70.0   0.99
E_begin   2.022  2.222   0.012    6.101  ...   119.0      77.0      65.0   1.02

[8 rows x 11 columns]