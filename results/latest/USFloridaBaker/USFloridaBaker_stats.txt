0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1571.31    46.61
p_loo       43.62        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      445   90.4%
 (0.5, 0.7]   (ok)         37    7.5%
   (0.7, 1]   (bad)         8    1.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.166  0.016   0.150    0.199  ...    66.0      51.0      22.0   1.04
pu        0.705  0.006   0.700    0.713  ...    70.0      95.0      54.0   1.01
mu        0.093  0.011   0.077    0.115  ...    31.0      32.0      59.0   1.02
mus       0.243  0.038   0.175    0.318  ...    76.0      77.0      54.0   1.00
gamma     0.393  0.053   0.282    0.481  ...   152.0     152.0      96.0   1.00
Is_begin  0.892  0.659   0.095    2.227  ...   130.0     101.0      93.0   1.01
Ia_begin  1.189  1.073   0.042    2.914  ...    63.0      49.0      39.0   1.00
E_begin   0.574  0.550   0.048    1.810  ...    57.0      62.0      54.0   1.03

[8 rows x 11 columns]