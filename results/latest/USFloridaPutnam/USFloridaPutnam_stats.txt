0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1746.70    41.93
p_loo       39.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      466   94.7%
 (0.5, 0.7]   (ok)         22    4.5%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.192  0.035   0.151    0.264  ...   152.0     152.0      39.0   1.00
pu        0.712  0.008   0.701    0.727  ...   141.0     152.0      61.0   1.00
mu        0.117  0.016   0.088    0.141  ...    49.0      47.0      57.0   1.02
mus       0.167  0.027   0.123    0.212  ...    91.0      92.0      59.0   1.01
gamma     0.275  0.052   0.180    0.373  ...    99.0     109.0      83.0   0.99
Is_begin  0.990  0.900   0.014    2.569  ...   112.0     100.0      95.0   1.00
Ia_begin  2.037  2.094   0.003    6.500  ...    65.0      36.0      60.0   1.05
E_begin   0.977  0.926   0.031    2.566  ...   134.0     101.0      60.0   0.99

[8 rows x 11 columns]