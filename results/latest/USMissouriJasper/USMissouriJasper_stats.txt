0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2367.32    97.15
p_loo       50.33        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.159  0.007   0.151    0.173  ...   124.0     152.0      81.0   1.02
pu        0.703  0.003   0.700    0.709  ...    41.0     128.0      32.0   1.10
mu        0.094  0.014   0.074    0.118  ...    13.0      13.0      40.0   1.16
mus       0.275  0.031   0.216    0.325  ...   103.0     116.0      86.0   1.04
gamma     0.626  0.079   0.506    0.780  ...    92.0      89.0      58.0   1.01
Is_begin  0.949  0.803   0.015    2.654  ...   117.0     120.0      80.0   1.04
Ia_begin  1.364  1.441   0.020    3.867  ...   126.0     152.0      93.0   1.01
E_begin   0.593  0.505   0.008    1.435  ...   115.0      89.0      97.0   1.00

[8 rows x 11 columns]