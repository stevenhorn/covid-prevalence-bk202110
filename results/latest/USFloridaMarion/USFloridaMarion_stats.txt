0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2315.04    45.12
p_loo       35.76        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      456   92.7%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.203  0.042   0.153    0.277  ...    75.0      75.0      40.0   1.01
pu        0.717  0.014   0.700    0.739  ...   152.0     126.0      93.0   1.01
mu        0.123  0.018   0.096    0.161  ...    51.0      48.0      76.0   1.00
mus       0.167  0.025   0.121    0.205  ...    43.0      48.0      74.0   1.04
gamma     0.271  0.041   0.209    0.359  ...   151.0     152.0      80.0   0.98
Is_begin  0.964  0.753   0.040    2.258  ...   101.0     109.0      93.0   0.99
Ia_begin  0.710  0.591   0.001    1.887  ...   146.0      97.0      21.0   1.05
E_begin   0.543  0.424   0.001    1.171  ...    84.0      73.0      59.0   1.05

[8 rows x 11 columns]