0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2786.77    36.68
p_loo       35.06        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         27    5.5%
   (0.7, 1]   (bad)         9    1.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.226  0.041   0.157    0.290  ...   116.0     136.0      96.0   1.06
pu        0.729  0.017   0.703    0.765  ...    72.0      67.0      60.0   1.02
mu        0.125  0.017   0.099    0.159  ...    51.0      52.0      38.0   1.07
mus       0.198  0.030   0.139    0.247  ...   108.0     112.0      96.0   1.00
gamma     0.281  0.045   0.211    0.368  ...    91.0      78.0      69.0   1.03
Is_begin  0.865  0.543   0.094    1.852  ...    73.0      67.0      60.0   1.02
Ia_begin  1.484  1.099   0.005    3.279  ...    36.0      16.0      20.0   1.10
E_begin   1.184  1.104   0.006    3.430  ...    13.0       6.0      22.0   1.32

[8 rows x 11 columns]