0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -839.46    30.90
p_loo       27.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      455   92.5%
 (0.5, 0.7]   (ok)         32    6.5%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.253  0.052   0.175    0.331  ...    34.0      35.0      59.0   1.07
pu        0.823  0.016   0.797    0.852  ...    30.0      30.0      30.0   1.04
mu        0.126  0.023   0.095    0.172  ...    46.0      45.0      54.0   1.04
mus       0.170  0.032   0.116    0.227  ...    29.0      57.0      21.0   1.08
gamma     0.209  0.045   0.139    0.297  ...   152.0     152.0      77.0   1.01
Is_begin  0.889  0.791   0.030    2.599  ...    82.0      93.0      60.0   1.01
Ia_begin  1.744  1.668   0.017    5.362  ...    86.0      70.0      84.0   0.99
E_begin   0.751  0.774   0.031    2.739  ...    92.0      61.0      31.0   1.01

[8 rows x 11 columns]