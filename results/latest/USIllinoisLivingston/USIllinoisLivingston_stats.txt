0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1289.87    32.32
p_loo       37.36        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      449   91.3%
 (0.5, 0.7]   (ok)         37    7.5%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.209  0.037   0.151    0.277  ...    41.0      39.0      66.0   1.05
pu        0.716  0.012   0.702    0.742  ...    73.0      68.0      59.0   0.98
mu        0.117  0.017   0.088    0.150  ...    14.0      14.0      56.0   1.13
mus       0.250  0.038   0.183    0.317  ...    69.0      64.0      91.0   1.00
gamma     0.322  0.047   0.235    0.396  ...   141.0     145.0      53.0   1.00
Is_begin  0.807  0.731   0.001    2.093  ...   117.0      93.0      87.0   0.99
Ia_begin  1.262  1.078   0.072    3.377  ...   127.0     130.0      65.0   0.99
E_begin   0.549  0.437   0.014    1.472  ...   137.0     135.0      96.0   0.99

[8 rows x 11 columns]