0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1408.13    41.72
p_loo       36.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      458   93.1%
 (0.5, 0.7]   (ok)         29    5.9%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.254  0.050   0.152    0.321  ...    76.0      59.0      63.0   1.05
pu        0.753  0.026   0.710    0.793  ...    21.0      33.0      60.0   1.06
mu        0.114  0.015   0.090    0.139  ...    43.0      44.0      57.0   1.00
mus       0.163  0.030   0.110    0.221  ...    63.0      63.0      60.0   1.03
gamma     0.195  0.038   0.132    0.265  ...   144.0     141.0      81.0   0.98
Is_begin  0.447  0.407   0.000    1.320  ...    72.0      62.0      42.0   1.00
Ia_begin  1.040  1.076   0.017    3.145  ...    68.0      60.0      42.0   1.00
E_begin   0.463  0.503   0.002    1.309  ...    59.0      51.0      40.0   1.06

[8 rows x 11 columns]