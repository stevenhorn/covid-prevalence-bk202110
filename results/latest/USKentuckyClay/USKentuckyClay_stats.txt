0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1433.32    52.85
p_loo       43.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      458   93.1%
 (0.5, 0.7]   (ok)         26    5.3%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.203  0.033   0.155    0.265  ...   102.0      99.0      93.0   1.02
pu        0.722  0.014   0.701    0.748  ...    32.0      26.0      67.0   1.07
mu        0.113  0.018   0.083    0.146  ...    16.0      13.0      31.0   1.13
mus       0.195  0.038   0.119    0.268  ...    63.0      74.0      59.0   1.06
gamma     0.223  0.037   0.155    0.280  ...    90.0      88.0      59.0   1.01
Is_begin  0.412  0.523   0.003    1.479  ...   110.0      80.0      60.0   1.01
Ia_begin  0.647  0.795   0.004    2.660  ...   107.0      81.0      93.0   1.01
E_begin   0.232  0.316   0.002    0.691  ...    99.0      36.0      59.0   1.05

[8 rows x 11 columns]