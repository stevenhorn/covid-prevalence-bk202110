0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1262.62    47.72
p_loo       43.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      470   95.5%
 (0.5, 0.7]   (ok)         19    3.9%
   (0.7, 1]   (bad)         1    0.2%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.247  0.053   0.165    0.347  ...    53.0      54.0      40.0   1.04
pu        0.760  0.021   0.723    0.795  ...    19.0      18.0      59.0   1.12
mu        0.145  0.022   0.106    0.185  ...    42.0      43.0      72.0   1.00
mus       0.223  0.031   0.161    0.265  ...    69.0      74.0      22.0   1.04
gamma     0.294  0.054   0.209    0.408  ...    62.0      63.0      72.0   1.00
Is_begin  0.749  0.677   0.030    2.063  ...    74.0      84.0      59.0   1.05
Ia_begin  1.135  1.271   0.005    3.844  ...    53.0      37.0      38.0   1.02
E_begin   0.451  0.365   0.013    1.087  ...    57.0      42.0      86.0   1.02

[8 rows x 11 columns]