0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1512.02    41.00
p_loo       34.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      445   90.4%
 (0.5, 0.7]   (ok)         36    7.3%
   (0.7, 1]   (bad)        10    2.0%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.245  0.055   0.150    0.327  ...    75.0      74.0      17.0   1.03
pu        0.794  0.045   0.713    0.850  ...     4.0       5.0      59.0   1.42
mu        0.128  0.023   0.083    0.165  ...    34.0      34.0      74.0   1.05
mus       0.171  0.027   0.118    0.217  ...    37.0      37.0      65.0   1.06
gamma     0.222  0.043   0.162    0.319  ...   152.0     152.0      49.0   0.99
Is_begin  1.380  1.205   0.002    3.660  ...    75.0      46.0      40.0   1.03
Ia_begin  3.755  2.515   0.207    8.421  ...   123.0     116.0      35.0   1.04
E_begin   2.096  2.080   0.033    6.383  ...    59.0      51.0     100.0   1.01

[8 rows x 11 columns]