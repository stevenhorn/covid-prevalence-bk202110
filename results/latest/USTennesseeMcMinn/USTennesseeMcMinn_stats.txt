0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1767.07    50.73
p_loo       48.14        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      466   94.7%
 (0.5, 0.7]   (ok)         20    4.1%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.249  0.047   0.152    0.322  ...    69.0      68.0      59.0   1.02
pu        0.760  0.028   0.706    0.800  ...    24.0      30.0      39.0   1.05
mu        0.112  0.022   0.069    0.148  ...    37.0      40.0      40.0   1.04
mus       0.177  0.035   0.125    0.249  ...    69.0      82.0      61.0   1.03
gamma     0.217  0.043   0.140    0.295  ...    74.0      80.0      91.0   0.98
Is_begin  0.731  0.685   0.008    1.944  ...   138.0     152.0     100.0   0.99
Ia_begin  1.160  1.035   0.021    3.175  ...    63.0      52.0      19.0   1.05
E_begin   0.748  0.798   0.006    2.689  ...    66.0     120.0      20.0   1.00

[8 rows x 11 columns]