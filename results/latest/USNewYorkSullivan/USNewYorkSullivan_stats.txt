0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1480.84    32.91
p_loo       30.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      462   93.9%
 (0.5, 0.7]   (ok)         25    5.1%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    0    0.0%

            mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.230  0.055   0.150    0.329  ...   152.0     123.0      55.0   1.01
pu         0.739  0.027   0.701    0.785  ...    50.0      47.0      39.0   1.02
mu         0.123  0.018   0.097    0.154  ...    16.0      15.0      91.0   1.11
mus        0.167  0.030   0.122    0.225  ...    58.0      52.0      65.0   1.03
gamma      0.279  0.038   0.218    0.339  ...   152.0     152.0      88.0   1.00
Is_begin   3.133  2.121   0.238    6.595  ...    53.0      24.0      40.0   1.06
Ia_begin   9.748  4.639   2.436   19.195  ...   102.0     117.0      58.0   1.03
E_begin   13.225  7.023   0.934   25.386  ...    25.0      38.0      69.0   1.06

[8 rows x 11 columns]