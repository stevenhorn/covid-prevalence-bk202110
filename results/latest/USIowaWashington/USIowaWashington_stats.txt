0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1278.60    36.15
p_loo       36.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      470   95.5%
 (0.5, 0.7]   (ok)         20    4.1%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.233  0.055   0.161    0.334  ...   109.0     106.0      93.0   1.00
pu        0.740  0.028   0.701    0.790  ...    12.0      10.0      56.0   1.16
mu        0.134  0.027   0.096    0.192  ...    18.0      14.0      15.0   1.13
mus       0.200  0.032   0.143    0.255  ...    99.0      99.0      91.0   1.00
gamma     0.269  0.049   0.191    0.334  ...   151.0     152.0      88.0   1.02
Is_begin  1.593  1.209   0.065    4.047  ...   102.0      90.0      40.0   0.99
Ia_begin  0.816  0.618   0.036    2.091  ...   141.0     121.0      79.0   1.00
E_begin   1.289  1.352   0.026    3.948  ...   106.0     125.0      93.0   0.99

[8 rows x 11 columns]