0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2159.01    41.96
p_loo       38.52        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      437   88.8%
 (0.5, 0.7]   (ok)         48    9.8%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.252  0.056   0.160    0.332  ...    36.0      33.0      53.0   1.07
pu        0.766  0.024   0.720    0.806  ...    17.0      17.0      22.0   1.14
mu        0.160  0.026   0.121    0.216  ...    12.0      12.0      30.0   1.16
mus       0.215  0.031   0.168    0.285  ...    42.0      60.0      60.0   1.02
gamma     0.276  0.041   0.217    0.359  ...   136.0     127.0     100.0   1.00
Is_begin  0.824  0.771   0.005    2.135  ...    95.0     130.0      49.0   1.01
Ia_begin  0.655  0.586   0.001    1.975  ...   134.0     128.0      40.0   1.02
E_begin   0.572  0.752   0.009    1.913  ...   106.0     152.0      53.0   1.02

[8 rows x 11 columns]