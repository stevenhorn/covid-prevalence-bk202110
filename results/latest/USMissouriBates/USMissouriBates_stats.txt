0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1267.33    76.81
p_loo       47.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      460   93.5%
 (0.5, 0.7]   (ok)         24    4.9%
   (0.7, 1]   (bad)         7    1.4%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.181  0.024   0.151    0.227  ...   119.0     107.0      59.0   1.00
pu        0.710  0.008   0.700    0.725  ...    71.0      45.0      22.0   1.08
mu        0.125  0.015   0.102    0.152  ...    32.0      27.0      59.0   1.09
mus       0.296  0.027   0.247    0.345  ...    87.0      94.0      88.0   1.01
gamma     0.567  0.094   0.409    0.725  ...   126.0     152.0      58.0   0.99
Is_begin  0.873  0.811   0.003    2.488  ...    76.0      41.0      14.0   1.00
Ia_begin  1.233  1.206   0.015    3.726  ...    86.0      96.0      60.0   1.00
E_begin   0.598  0.732   0.001    1.809  ...   101.0      63.0      37.0   1.03

[8 rows x 11 columns]