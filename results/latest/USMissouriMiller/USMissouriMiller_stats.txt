0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1449.44    76.85
p_loo       48.68        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      447   90.9%
 (0.5, 0.7]   (ok)         39    7.9%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.173  0.018   0.152    0.201  ...   114.0     113.0      69.0   1.03
pu        0.707  0.005   0.700    0.716  ...    55.0      30.0      40.0   1.06
mu        0.118  0.014   0.093    0.144  ...    13.0      12.0      38.0   1.13
mus       0.242  0.034   0.169    0.291  ...    35.0      37.0      14.0   1.08
gamma     0.444  0.071   0.325    0.554  ...    13.0      12.0      59.0   1.17
Is_begin  0.390  0.443   0.001    1.090  ...    49.0      36.0      20.0   1.08
Ia_begin  0.827  0.889   0.041    2.764  ...    57.0      73.0      53.0   1.00
E_begin   0.349  0.442   0.007    1.046  ...    53.0      58.0      25.0   1.04

[8 rows x 11 columns]