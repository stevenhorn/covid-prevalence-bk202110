0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1444.88    70.87
p_loo       62.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      467   94.9%
 (0.5, 0.7]   (ok)         18    3.7%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.164  0.013   0.150    0.190  ...   152.0     152.0      43.0   1.00
pu        0.705  0.006   0.700    0.721  ...   152.0     152.0      65.0   1.00
mu        0.132  0.021   0.098    0.167  ...    24.0      23.0      60.0   1.11
mus       0.255  0.038   0.199    0.318  ...    65.0      60.0      42.0   1.03
gamma     0.506  0.086   0.385    0.674  ...    50.0      65.0      56.0   0.99
Is_begin  0.736  0.629   0.026    2.007  ...   128.0     152.0      81.0   1.02
Ia_begin  0.937  1.063   0.000    3.302  ...   102.0      31.0      14.0   1.06
E_begin   0.424  0.466   0.004    1.384  ...    53.0      51.0      60.0   1.03

[8 rows x 11 columns]