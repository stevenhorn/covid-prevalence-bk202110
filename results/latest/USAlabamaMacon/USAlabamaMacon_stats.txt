0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1176.60    55.72
p_loo       39.84        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      458   93.1%
 (0.5, 0.7]   (ok)         30    6.1%
   (0.7, 1]   (bad)         2    0.4%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.274  0.048   0.196    0.336  ...    33.0      32.0      42.0   1.04
pu        0.799  0.019   0.761    0.832  ...    15.0      14.0      60.0   1.11
mu        0.110  0.020   0.076    0.142  ...     9.0       9.0      81.0   1.19
mus       0.180  0.037   0.119    0.255  ...    73.0      71.0      63.0   1.03
gamma     0.201  0.041   0.134    0.274  ...    57.0      60.0      74.0   1.03
Is_begin  0.717  0.754   0.004    2.548  ...    18.0      10.0      36.0   1.15
Ia_begin  1.277  1.390   0.001    3.505  ...    28.0      11.0      15.0   1.15
E_begin   0.615  0.611   0.024    2.011  ...    62.0      56.0      60.0   1.02

[8 rows x 11 columns]