0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -898.51    29.14
p_loo       27.13        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      463   94.1%
 (0.5, 0.7]   (ok)         26    5.3%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    0    0.0%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.250  0.052   0.156    0.338  ...    33.0      36.0      59.0   1.06
pu        0.780  0.019   0.740    0.807  ...    57.0      56.0     100.0   1.05
mu        0.133  0.021   0.091    0.168  ...    27.0      51.0      41.0   1.04
mus       0.179  0.037   0.109    0.228  ...    81.0      87.0      80.0   0.99
gamma     0.199  0.041   0.131    0.251  ...   152.0     152.0      81.0   1.04
Is_begin  0.563  0.560   0.002    1.751  ...    81.0      55.0      40.0   1.06
Ia_begin  0.985  1.224   0.014    3.623  ...   100.0     102.0      91.0   0.98
E_begin   0.438  0.524   0.001    1.298  ...   111.0     111.0      59.0   1.02

[8 rows x 11 columns]