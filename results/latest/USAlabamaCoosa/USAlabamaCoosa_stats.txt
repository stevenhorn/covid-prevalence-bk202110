0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1131.84    49.91
p_loo       41.18        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      464   94.3%
 (0.5, 0.7]   (ok)         23    4.7%
   (0.7, 1]   (bad)         4    0.8%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.255  0.049   0.177    0.346  ...   152.0     152.0      88.0   1.00
pu        0.754  0.023   0.717    0.793  ...    34.0      36.0      38.0   1.04
mu        0.108  0.021   0.075    0.140  ...    26.0      27.0      57.0   1.04
mus       0.182  0.032   0.124    0.241  ...   152.0     152.0      60.0   1.00
gamma     0.215  0.036   0.152    0.287  ...    78.0     139.0      60.0   1.01
Is_begin  0.788  0.873   0.006    2.196  ...   109.0      47.0      40.0   1.03
Ia_begin  1.477  1.396   0.008    3.899  ...    99.0      89.0      59.0   1.01
E_begin   0.628  0.661   0.001    1.629  ...    80.0      49.0      43.0   1.02

[8 rows x 11 columns]