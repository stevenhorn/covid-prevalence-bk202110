0 Divergences 
Passed validation 
2021-08-08 date of last case count
Computed from 80 by 513 log-likelihood matrix

         Estimate       SE
elpd_loo  -892.77    42.43
p_loo       43.45        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      469   91.4%
 (0.5, 0.7]   (ok)         40    7.8%
   (0.7, 1]   (bad)         1    0.2%
   (1, Inf)   (very bad)    3    0.6%

            mean      sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.257   0.048   0.174    0.337  ...   152.0     152.0      44.0   1.05
pu         0.840   0.054   0.724    0.899  ...     4.0       5.0      17.0   1.46
mu         0.265   0.035   0.213    0.334  ...     6.0       6.0      60.0   1.38
mus        0.298   0.055   0.204    0.388  ...    12.0      13.0      40.0   1.13
gamma      0.442   0.080   0.328    0.596  ...    49.0      44.0      43.0   1.04
Is_begin   3.899   3.773   0.060   13.547  ...   147.0      92.0      59.0   1.00
Ia_begin  14.302  13.625   0.202   37.198  ...    64.0      52.0      57.0   1.07
E_begin    5.188   5.281   0.101   14.383  ...    51.0      19.0      87.0   1.09

[8 rows x 11 columns]