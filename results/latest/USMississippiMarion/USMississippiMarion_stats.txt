0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1386.09    52.15
p_loo       50.72        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      457   92.9%
 (0.5, 0.7]   (ok)         31    6.3%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    1    0.2%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.256  0.056   0.177    0.350  ...    27.0      29.0      14.0   1.05
pu        0.739  0.023   0.701    0.781  ...    28.0      28.0      42.0   1.03
mu        0.145  0.025   0.105    0.195  ...     7.0       7.0      57.0   1.29
mus       0.230  0.035   0.170    0.299  ...    93.0      88.0      86.0   1.04
gamma     0.280  0.057   0.190    0.394  ...    91.0     107.0      69.0   1.07
Is_begin  0.754  0.900   0.001    2.532  ...   119.0      66.0      60.0   1.01
Ia_begin  1.522  1.833   0.010    5.058  ...    93.0      55.0     100.0   1.01
E_begin   0.574  0.678   0.003    1.827  ...    45.0      47.0      40.0   1.03

[8 rows x 11 columns]