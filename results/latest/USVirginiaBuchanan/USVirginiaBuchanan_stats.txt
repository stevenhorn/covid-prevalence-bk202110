0 Divergences 
Passed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -1236.49    39.65
p_loo       36.41        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      471   95.7%
 (0.5, 0.7]   (ok)         16    3.3%
   (0.7, 1]   (bad)         3    0.6%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.257  0.051   0.174    0.336  ...   125.0     116.0      96.0   1.02
pu        0.812  0.031   0.747    0.853  ...     7.0       8.0      38.0   1.25
mu        0.141  0.024   0.101    0.186  ...    39.0      39.0      49.0   1.05
mus       0.213  0.050   0.135    0.298  ...   142.0     115.0      46.0   1.05
gamma     0.242  0.047   0.170    0.332  ...   124.0     114.0      59.0   1.01
Is_begin  0.934  0.841   0.011    2.666  ...   122.0     152.0      36.0   0.99
Ia_begin  1.666  1.942   0.030    6.401  ...   114.0     152.0      95.0   1.00
E_begin   0.812  1.017   0.015    2.149  ...    97.0     102.0      47.0   1.01

[8 rows x 11 columns]