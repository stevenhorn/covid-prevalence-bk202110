0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo  -637.98    63.46
p_loo       59.78        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      470   95.5%
 (0.5, 0.7]   (ok)         13    2.6%
   (0.7, 1]   (bad)         6    1.2%
   (1, Inf)   (very bad)    3    0.6%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.234  0.048   0.161    0.316  ...   131.0     152.0      59.0   0.99
pu        0.741  0.021   0.703    0.778  ...   122.0     111.0      57.0   1.03
mu        0.162  0.027   0.124    0.215  ...    91.0      88.0      80.0   1.00
mus       0.218  0.034   0.169    0.281  ...   107.0     115.0      83.0   1.00
gamma     0.277  0.049   0.189    0.366  ...   124.0     118.0      57.0   1.11
Is_begin  0.522  0.562   0.001    1.321  ...    98.0      76.0      60.0   0.99
Ia_begin  0.918  1.121   0.004    2.977  ...    50.0      82.0      42.0   1.03
E_begin   0.492  0.681   0.015    1.722  ...    60.0      75.0      58.0   1.09

[8 rows x 11 columns]