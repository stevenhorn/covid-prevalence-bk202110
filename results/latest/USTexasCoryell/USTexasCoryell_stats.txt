0 Divergences 
Failed validation 
2021-07-19 date of last case count
Computed from 80 by 492 log-likelihood matrix

         Estimate       SE
elpd_loo -2159.20    66.47
p_loo       44.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      464   94.3%
 (0.5, 0.7]   (ok)         21    4.3%
   (0.7, 1]   (bad)         5    1.0%
   (1, Inf)   (very bad)    2    0.4%

           mean     sd  hdi_3%  hdi_97%  ...  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.209  0.042   0.151    0.289  ...    67.0      50.0      47.0   1.05
pu        0.715  0.014   0.700    0.743  ...    96.0     126.0      88.0   1.03
mu        0.130  0.023   0.102    0.173  ...    10.0      11.0      78.0   1.18
mus       0.263  0.031   0.194    0.305  ...   140.0     142.0      60.0   1.01
gamma     0.493  0.064   0.372    0.609  ...   152.0     152.0      96.0   1.00
Is_begin  0.854  0.862   0.002    2.649  ...   111.0      82.0      58.0   1.00
Ia_begin  1.244  1.176   0.025    3.506  ...   119.0      73.0      39.0   0.99
E_begin   0.681  0.725   0.014    2.190  ...   134.0     147.0      84.0   1.00

[8 rows x 11 columns]